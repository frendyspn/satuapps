@extends('layout.template')

@section('title', $title)

@section('content')
    <!-- App Capsule -->
    <div id="appCapsule">

    <div class="section mt-2 text-center">
            <h1>{{__('bahasa.Daftar')}}</h1>
            <h4>{{__('bahasa.Lengkapi_Form_Dibawah')}}</h4>
        </div>

        <div class="section mt-2 mb-2 p-2">

            <ul class="listview flush transparent simple-listview no-space mt-3">
                <li>
                    <strong>{{__('bahasa.Nama')}}</strong>
                    <span>{{Session::get('nama_lengkap')}}</span>
                </li>
                <li>
                    <strong>{{__('bahasa.Email')}}</strong>
                    <span>{{Session::get('email')}}</span>
                </li>
                <li>
                    <strong>{{__('bahasa.Nomor_HP')}}</strong>
                    <span>{{Session::get('no_hp')}}</span>
                </li>
            </ul>


        </div>

        <div class="section mb-5 p-2">

            <form method="post" action="{{route('registrasi_proses_konfirmasi')}}" enctype="multipart/form-data">
                @csrf
                <div class="card">
                    <div class="card-body pb-1">
                        <div class="form-group basic">
                            <div class="input-wrapper">
                                <select class="form-control" name="jenis_kendaraan" id="jenis_kendaraan">
                                    <option value="">-- {{__('bahasa.Pilih_Jenis_Kendaraan')}} --</option>
                                    @foreach($jenis_kendaraan as $row)
                                    <option value="{{$row->id_jenis_kendaraan}}">{{$row->jenis_kendaraan}}</option>
                                    @endforeach
                                </select>
                                
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <div class="form-group basic">
                            <div class="input-wrapper">
                                <input type="text" class="form-control" id="merek" name="merek" placeholder="{{__('bahasa.placeholder_merk_kendaraan')}}">
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <div class="form-group basic">
                            <div class="input-wrapper">
                                <input type="text" class="form-control" id="plat_nomor" name="plat_nomor" placeholder="{{__('bahasa.Plat_Nomor_Kendaraan')}}">
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <div class="form-group basic">
                            <div class="input-wrapper">
                                <label for="">{{__('bahasa.upload_lampiran')}}</label>
                                <span class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#ModalFotoLampiran" onclick="start_take_capture()" >{{__('bahasa.Ambil_Foto_Lampiran')}}</span>
                                <input type="hidden" class="form-control" id="lampiran" name="lampiran" >
                                <img src="" alt="" width="100%" id="preview_lampiran">
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>
                    </div>
                </div>


                
                <div class="form-button-group  transparent">
                    <button type="submit" class="btn btn-primary btn-block btn-lg" onclick="loaderShow()">{{__('bahasa.Lanjutkan')}}</button>
                </div>

            </form>
        </div>

    </div>

    
        <div class="modal fade modalbox" id="ModalFotoLampiran" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{__('bahasa.title_upload_lampiran')}}</h5>
                        <a href="#" data-bs-dismiss="modal" onclick="closeWebcam()">Close</a>
                    </div>
                    <div class="modal-body">
                        <div class="webcam-capture"></div>
                        <button class="btn btn-secondary mt-1" data-bs-dismiss="modal" onclick="captureimage()">{{__('bahasa.button_ambil_gambar')}}</button>
                    </div>
                </div>
            </div>
        </div>
    <!-- * App Capsule -->

    <script>

    var posisi = 'depan';

    $( document ).ready(function() {
        const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
        if (isMobile) {
            posisi = 'belakang'
        } else {
            posisi = 'depan'
        }

    });

    function start_take_capture(){
        var getwidth = ((window.innerWidth > 0) ? window.innerWidth : screen.width)-30;
        
            if(posisi === 'depan'){
                console.log('depan nih')
                Webcam.set({
                    width: getwidth,
                    height: 450,
                    image_format: 'jpeg',
                    jpeg_quality:80,
                    // facingMode: { exact: 'user' }
                });
            } else {
                console.log('belakagn nih')
                Webcam.set({
                    width: getwidth,
                    height: 450,
                    image_format: 'jpeg',
                    jpeg_quality:80,
                    facingMode: { exact: 'environment' }
                });
            }
        
    
            var cameras = new Array(); //create empty array to later insert available devices
            navigator.mediaDevices.enumerateDevices() // get the available devices found in the machine
            .then(function(devices) {
                devices.forEach(function(device) {
                var i = 0;
                    if(device.kind=== "videoinput"){ //filter video devices only
                        cameras[i]= device.deviceId; // save the camera id's in the camera array
                        i++;
                    }
                });
            })

            if(posisi === 'depan'){
                Webcam.set('constraints',{
                    width: getwidth,
                    height: 450,
                    image_format: 'jpeg',
                    jpeg_quality:80,
                    // facingMode: { exact: 'user' }
                });
            } else {
                Webcam.set('constraints',{
                    width: getwidth,
                    height: 450,
                    image_format: 'jpeg',
                    jpeg_quality:80,
                    facingMode: { exact: 'environment' }
                });
            }

            
        
            Webcam.on( 'error', function(err) {
                notif('bg-danger', err)
            } );
        
            Webcam.attach('.webcam-capture');
            
        
        }


        function captureimage() {
            Webcam.snap( function(data_uri) {
                $('#preview_lampiran').attr('src', data_uri)
                $('#lampiran').val(data_uri)
                Webcam.reset('.webcam-capture')
            } );
        }

    </script>
@endsection
