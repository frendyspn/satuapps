@extends('layout.template')

@section('title', $title)

@section('content')
    <!-- App Capsule -->
    <div id="appCapsule" class="full-height">
        <div class="section">
            <div class="row mt-2">
                <div class="col-12">
                    <div class="stat-box">
                        <div class="title">Saldo</div>
                        <div class="value text-success">Rp.{{number_format($saldo)}}</div>
                    </div>
                </div>
            </div>
            <!-- <div class="row mt-2">
                <div class="col-6">
                    <div class="stat-box">
                        <div class="title">Total Pendapatan</div>
                        <div class="value" style="font-size:14px">Rp.{{number_format($saldoKurir)}}</div>
                    </div>
                </div>

                <div class="col-6">
                    <div class="stat-box">
                        <div class="title">Total Komisi</div>
                        <div class="value" style="font-size:14px">Rp.{{number_format($saldoKomisi)}}</div>
                    </div>
                </div>

            </div> -->
        </div>

        <div class="section mt-2">
            <div class="wallet-card">
                
                <div class="wallet-footer">
                    <div class="item">
                        <a href="#" data-bs-toggle="modal" data-bs-target="#ModalDeposit" onclick="GetDeposit()">
                            <div class="icon-wrapper bg-danger">
                                <ion-icon name="add-outline" role="img" class="md hydrated" aria-label="arrow down outline"></ion-icon>
                            </div>
                            <strong>Deposit</strong>
                        </a>
                    </div>
                    <div class="item">
                        <a href="#" data-bs-toggle="modal" data-bs-target="#ModalTransfer">
                            <div class="icon-wrapper">
                                <ion-icon name="cube-outline" role="img" class="md hydrated" aria-label="arrow forward outline"></ion-icon>
                            </div>
                            <strong>Transfer</strong>
                        </a>
                    </div>
                    <div class="item">
                        <a href="#" data-bs-toggle="modal" data-bs-target="#ModalWithdraw">
                            <div class="icon-wrapper bg-success">
                                <ion-icon name="cart-outline" role="img" class="md hydrated" aria-label="card outline"></ion-icon>
                            </div>
                            <strong>Withdraw</strong>
                        </a>
                    </div>

                </div>
                <!-- * Wallet Footer -->
            </div>
        </div>

        <div class="section">
            <div class="transactions">';
            <?php
            $tglShow = '';
            foreach ($dtWallet as $row) {
                if (date('Ymd', strtotime($row->waktu_pendapatan)) != $tglShow) {
                    echo '<div class="section-title">'.date('d M Y', strtotime($row->waktu_pendapatan)).'</div>';
                    $tglShow = date('Ymd', strtotime($row->waktu_pendapatan));
                }
                if ($row->akun == 'deposit') {
                    $ket = 'Top Up Saldo';
                } else {
                    $ket = $row->keterangan;
                }

                if ($row->status == 'Proses') {
                    $status = ' <small class="text-warning">PROSES<small>';
                } else {
                    $status = '';
                }
                
                echo '
                <a href="#" class="item" onclick="viewImage('.$row->id_pendapatan.',\''.$row->keterangan.'\')">
                    <div class="detail">
                        <div>
                            <strong>'.$ket.$status.'</strong>
                            <p>'.date('d M Y', strtotime($row->waktu_pendapatan)).'</p>
                        </div>
                    </div>
                    <div class="right">';
                        if ($row->transaksi == 'debit') {
                            echo '<div class="price"> '.__('bahasa.kurs').number_format($row->nominal).'</div>';
                        } else {
                            echo '<div class="price text-danger"> '.__('bahasa.kurs').number_format($row->nominal).'</div>';
                        }
                        
                        
                    echo '</div>
                </a>
                ';
            }
            ?>
            </div>
        </div>

    </div>
    
    <!-- * App Capsule -->


    <div class="modal fade modalbox" id="ModalDeposit" tabindex="-1" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tambah Deposit</h5>
                    <a href="#" data-bs-dismiss="modal">Close</a>
                </div>
                <div class="modal-body">
                <form method="post" action="{{url('proses_deposit')}}">
                    @csrf
                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="label" for="jumlah_deposit">Jumlah Deposit</label>
                                <input type="number" class="form-control" id="jumlah_deposit" name="jumlah_deposit" onkeyup="hitungDeposit($(this).val())" min="10000" required>
                                <i class="clear-input">
                                    <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="label" for="kode_unik_deposit">Kode Unik</label>
                                <input type="number" class="form-control" id="kode_unik_deposit" name="kode_unik_deposit" readonly>
                                <i class="clear-input">
                                    <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="label" for="total_deposit">Total Deposit</label>
                                <input type="text" class="form-control" id="total_deposit" name="total_deposit" readonly>
                                <i class="clear-input">
                                    <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary btn-block">Transfer Sekarang</button>

                        <h4><br/>Pastikan untuk melakukan transfer sesuai nominal pada "TOTAL DEPOSIT", dan transfer ke rekening dibawah ini</h4>
                        @foreach($list_rekening as $rek)
                        <br/>
                        <h5>Bank: {{$rek->nama_bank}}</h5>
                        <h5>Nomor: {{$rek->no_rekening}}</h5>
                        <h5>Penerima: {{$rek->pemilik_rekening}}</h5>
                        @endforeach
                        
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade modalbox" id="ModalTransfer" tabindex="-1" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Transfer Saldo</h5>
                    <a href="#" data-bs-dismiss="modal">Close</a>
                </div>
                <div class="modal-body">
                <form method="post" action="{{url('proses_transfer')}}">
                    @csrf
                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="label" for="no_hp_penerima_transfer">Nomor HP Penerima</label>
                                <input type="number" class="form-control" id="no_hp_penerima_transfer" name="no_hp_penerima_transfer" onchange="cekNoHpPenerima($(this).val())" required>
                                <i class="clear-input">
                                    <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <h3 id="penerima_transfer"></h3>

                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="label" for="jumlah_transfer">Jumlah Transfer <small class="text-danger">Saldo {{number_format($saldo)}}</small></label>
                                <input type="number" class="form-control" id="jumlah_transfer" name="jumlah_transfer" min="10000" required>
                                <i class="clear-input">
                                    <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        
                        <button type="submit" class="btn btn-primary btn-block">Transfer Sekarang</button>

                        
                        
                    </form>
                </div>
            </div>
        </div>
    </div>



    <div class="modal fade modalbox" id="ModalWithdraw" tabindex="-1" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Withdraw Saldo</h5>
                    <a href="#" data-bs-dismiss="modal">Close</a>
                </div>
                <div class="modal-body">
                <form method="post" action="{{url('proses_withdraw')}}">
                    @csrf
                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="label" for="bank_withdraw">Bank</label>
                                <select class="form-control" name="bank_withdraw" id="bank_withdraw">
                                    <option value="">--Pilih Bank--</option>
                                    <option value="BCA">BCA</option>
                                    <option value="BRI">BRI</option>
                                    <option value="BNI">BNI</option>
                                    <option value="MANDIRI">MANDIRI</option>
                                </select>
                                <i class="clear-input">
                                    <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="label" for="nomor_rekening_withdraw">Nomor Rekening</label>
                                <input type="number" class="form-control" id="nomor_rekening_withdraw" name="nomor_rekening_withdraw" required>
                                <i class="clear-input">
                                    <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="label" for="nama_rekening_withdraw">Nama Pemilik Rekening</label>
                                <input type="text" class="form-control" id="nama_rekening_withdraw" name="nama_rekening_withdraw" required>
                                <i class="clear-input">
                                    <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="label" for="jumlah_withdraw">Jumlah Withdraw <small class="text-danger">Saldo {{number_format($saldo)}}</small></label>
                                <input type="number" class="form-control" id="jumlah_withdraw" name="jumlah_withdraw" min="10000" required>
                                <i class="clear-input">
                                    <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        
                        <button type="submit" class="btn btn-primary btn-block" onclick="$(this).attr('disabled')">Withdraw Sekarang</button>

                        
                        
                    </form>
                </div>
            </div>
        </div>
    </div>





    <script>
        var kodeUnik = '{{ $kode_unik_deposit }}'
        function GetDeposit() {
            console.log(kodeUnik)
            var unik = 0;
            if (kodeUnik) {
                unik = Math.random() * (999 - 111) + 111;
            }
            $('#kode_unik_deposit').val(unik.toFixed(0))

        }

        function hitungDeposit(jumlah) {
            var total = parseInt(jumlah) + parseInt($('#kode_unik_deposit').val())
            $('#total_deposit').val( formatNumber(total) )
        }


        function cekNoHpPenerima(no_hp) {
            // loaderShow()
            $.ajax({
                url: "{{route('cek_no_hp')}}",
                type: "POST",   
                dataType:"JSON",
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') 
                },
                data:{
                    no_hp
                },
                success: function(response) {
                    console.log(response)
                    // loaderHide()
                    $('#penerima_transfer').html('Penerima: '+response['nama_lengkap'])
                    $('#penerima_transfer').removeClass('bg-danger')
                },
                error: function(err) {
                    // loaderHide()
                    $('#penerima_transfer').html('User Tidak Ditemukan')
                    $('#penerima_transfer').addClass('bg-danger')
                }    
            });
        }
    </script>
@endsection
