@extends('layout.template')

@section('title', $title)

@section('content')
    <!-- App Capsule -->
    <div id="appCapsule" class="full-height">

        <div class="section">

            <div class="wallet-card mb-2">
                <!-- Balance -->
                <div class="balance">
                    <div class="left">
                        <span class="title">{{__('bahasa.total_saldo')}}</span>
                        <h1 class="total" id="saldo">{{__('bahasa.kurs')}} 0</h1>
                    </div>
                    <!-- <div class="right">
                        <a href="#" class="button" data-bs-toggle="modal" data-bs-target="#depositActionSheet">
                            <ion-icon name="add-outline"></ion-icon>
                        </a>
                    </div> -->
                </div>
                <!-- * Balance -->
                <!-- Wallet Footer -->
                <!-- <div class="wallet-footer">
                    <div class="item">
                        <a href="#" data-bs-toggle="modal" data-bs-target="#withdrawActionSheet">
                            <div class="icon-wrapper bg-danger">
                                <ion-icon name="arrow-down-outline"></ion-icon>
                            </div>
                            <strong>Withdraw</strong>
                        </a>
                    </div>
                    <div class="item">
                        <a href="#" data-bs-toggle="modal" data-bs-target="#sendActionSheet">
                            <div class="icon-wrapper">
                                <ion-icon name="arrow-forward-outline"></ion-icon>
                            </div>
                            <strong>Send</strong>
                        </a>
                    </div>
                    <div class="item">
                        <a href="app-cards.html">
                            <div class="icon-wrapper bg-success">
                                <ion-icon name="card-outline"></ion-icon>
                            </div>
                            <strong>Cards</strong>
                        </a>
                    </div>
                    <div class="item">
                        <a href="#" data-bs-toggle="modal" data-bs-target="#exchangeActionSheet">
                            <div class="icon-wrapper bg-warning">
                                <ion-icon name="swap-vertical"></ion-icon>
                            </div>
                            <strong>Exchange</strong>
                        </a>
                    </div>

                </div> -->
                <!-- * Wallet Footer -->
            </div>

            <div class="section full mb-3 mt-3">
                <!-- <div class="section-title">News Update</div> -->

                <!-- carousel single -->
                <div class="carousel-single splide">
                    <div class="splide__track">
                        <ul class="splide__list" >

                        @foreach ($dtBanner as $row) 
                            
                            <li class="splide__slide">
                                @if($row->url == '')
                                <a href="#">
                                @else
                                <a href="{{$row->url}}" target="_blank">
                                @endif
                                <div class="card">
                                    <img src="{{image_url()}}public/uploads/banner/{{$row->gambar}}" class="card-img-top" alt="image">
                                    <div class="card-body">
                                        <h6 class="card-title">{{$row->judul}}</h6>
                                    </div>
                                </div>
                                </a>
                            </li>
                            
                        @endforeach

                        </ul>
                    </div>
                </div>

            </div>
            
            <div id="content-order"></div>

        </div>

    </div>
    
    <!-- * App Capsule -->

        <!-- Modal Ambil Barang -->
        <div class="modal fade modalbox" id="ModalAmbilBarang" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{__('bahasa.title_ambil_foto_barang')}}</h5>
                        <a href="#" data-bs-dismiss="modal" onclick="closeWebcam()">{{__('bahasa.title_button_tutup')}}</a>
                    </div>
                    <div class="modal-body">
                        <div class="webcam-capture"></div>
                        <input type="hidden" id="id_order">
                        <button class="btn btn-secondary mt-1" onclick="captureimage()">{{__('bahasa.button_ambil_gambar')}}</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- * Modal Ambil Barang -->


        <!-- Modal Ambil Barang -->
        <div class="modal fade modalbox" id="ModalSerahBarang" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{__('bahasa.title_serah_foto_barang')}}</h5>
                        <a href="#" data-bs-dismiss="modal" onclick="closeWebcamSerah()">{{__('bahasa.title_button_tutup')}}</a>
                    </div>
                    <div class="modal-body">
                        <div class="webcam-capture-serah"></div>
                        <input type="hidden" id="id_order_serah">
                        <input type="text" class="form-control mt-1 mb-1" id="penerima_barang" placeholder="Penerima Barang">
                        <button class="btn btn-secondary" onclick="captureimageserah()">{{__('bahasa.button_ambil_gambar')}}</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- * Modal Ambil Barang -->



        <div class="modal fade modalbox" id="ModalUpdatePesananShop" tabindex="-1" aria-hidden="true" style="display: none;">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{__('bahasa.title_edit_pesanan')}}</h5>
                        <a href="#" data-bs-dismiss="modal">{{__('bahasa.title_button_tutup')}}</a>
                    </div>
                    <div class="modal-body">
                        <form action="update_pesanan_shop" method="post">
                            @csrf
                            <div class="form-group boxed">
                                <input type="hidden" class="form-control" id="id_konsumen" name="id_konsumen" placeholder="Nama Barang">
                                <input type="hidden" class="form-control" id="id_pesanan" name="id_pesanan" placeholder="Nama Barang">
                                <div class="input-wrapper">
                                    <label class="label" for="text4b">{{__('bahasa.nama_barang')}}</label>
                                    <input type="text" class="form-control" id="nama_barang" name="nama_barang" placeholder="{{__('bahasa.nama_barang')}}" readonly>
                                    
                                </div>
                            </div>
                            <div class="form-group boxed">
                                <div class="row">
                                    <div class="input-wrapper col-6">
                                        <label class="label" for="text4b">{{__('bahasa.satuan')}}</label>
                                        <input type="text" class="form-control" id="satuan_barang" name="satuan_barang" placeholder="{{__('bahasa.satuan')}}">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                                        </i>
                                    </div>

                                    <div class="input-wrapper col-6">
                                        <label class="label" for="text4b">{{__('bahasa.qty')}}</label>
                                        <input type="number" class="form-control" id="qty_barang" name="qty_barang">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>
                            </div>

                            
                            <div class="form-group boxed">
                                <div class="input-wrapper">
                                    <label class="label" for="text4b">{{__('bahasa.harga_satuan')}}</label>
                                    <input type="number" class="form-control" id="harga_barang" name="harga_barang">
                                    <i class="clear-input">
                                        <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                                    </i>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary btn-block">{{__('bahasa.title_button_update')}}</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>

    <script>

    var posisi = 'depan';

    $( document ).ready(function() {
        getOrder()
        getSaldo()
        setInterval(function() {
            getOrder()
        }, 5000);

        const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
        if (isMobile) {
            posisi = 'belakang'
        } else {
            posisi = 'depan'
        }

    });

    function getSaldo() {
        
        $.ajax({
            url: "{{route('get_saldo')}}",
            type: "POST",   
            dataType:"HTML",
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') 
            },
            success: function(response) {
                console.log(response)
                $('#saldo').html(response)
            },
            error: function(err) {
                // console.log(err);
                // $('#content-order').html(err)
            }    
        });
    }

    function getOrder() {
        
        $.ajax({
            url: "{{route('get_order')}}",
            type: "POST",   
            dataType:"HTML",
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') 
            },
            data: {
                position: $('#latitude').val(),
            },
            success: function(response) {
                $('#content-order').html(response)
                
                if ($('.ada_order').length > 0) {
                    var notify = new Audio();
                    notify.src = navigator.userAgent.match(/Firefox/) ? "{{ asset('assets/notif/notif-1.wav') }}" : "{{ asset('assets/notif/notif-1.wav') }}";
                    notify.play()
                }
            },
            error: function(err) {
                // console.log(err);
                $('#content-order').html(err)
            }    
        });
    }

    function ambilOrder(idOrder) {
        $.ajax({
            url: "{{route('ambil_order')}}",
            type: "POST",   
            dataType:"JSON",
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') 
            },
            data: {
                idOrder
            },
            success: function(response) {
                notif('bg-primary', response)
                console.log('sukses '+response)
            },
            error: function(err) {
                notif('bg-danger', err.responseJSON)
                console.log('gagal '+err.responseJSON)
            }    
        });
    }


    

    function start_take_capture(id_order){
        $('#id_order').val(id_order)
        var getwidth = ((window.innerWidth > 0) ? window.innerWidth : screen.width)-30;
        
            if(posisi === 'depan'){
                console.log('depan nih')
                Webcam.set({
                    width: getwidth,
                    height: 450,
                    image_format: 'jpeg',
                    jpeg_quality:80,
                    // facingMode: { exact: 'user' }
                });
            } else {
                console.log('belakagn nih')
                Webcam.set({
                    width: getwidth,
                    height: 450,
                    image_format: 'jpeg',
                    jpeg_quality:80,
                    facingMode: { exact: 'environment' }
                });
            }
        
    
            var cameras = new Array(); //create empty array to later insert available devices
            navigator.mediaDevices.enumerateDevices() // get the available devices found in the machine
            .then(function(devices) {
                devices.forEach(function(device) {
                var i = 0;
                    if(device.kind=== "videoinput"){ //filter video devices only
                        cameras[i]= device.deviceId; // save the camera id's in the camera array
                        i++;
                    }
                });
            })

            if(posisi === 'depan'){
                Webcam.set('constraints',{
                    width: getwidth,
                    height: 450,
                    image_format: 'jpeg',
                    jpeg_quality:80,
                    // facingMode: { exact: 'user' }
                });
            } else {
                Webcam.set('constraints',{
                    width: getwidth,
                    height: 450,
                    image_format: 'jpeg',
                    jpeg_quality:80,
                    facingMode: { exact: 'environment' }
                });
            }

            
        
            Webcam.on( 'error', function(err) {
                notif('bg-danger', err)
            } );
        
            Webcam.attach('.webcam-capture');
            
        
        }


        function captureimage() {
            Webcam.snap( function(data_uri) {
                $.ajax({
                    url: "{{route('ambil_barang')}}",
                    type: "POST",   
                    dataType:"JSON",
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') 
                    },
                    data: {
                        id_order : $('#id_order').val(),
                        foto : data_uri
                    },
                    success: function(response) {
                        // location.reload()
                    },
                    error: function(err) {
                        // location.reload()
                    }    
                });
            } );
        }


        function antar_penumpang(id_order) {
            loaderShow()
                $.ajax({
                    url: "{{route('ambil_barang')}}",
                    type: "POST",   
                    dataType:"JSON",
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') 
                    },
                    data: {
                        id_order
                    },
                    success: function(response) {
                        location.reload()
                    },
                    error: function(err) {
                        location.reload()
                    }    
                });
        }

        function turunkan_penumpang(id_order) {
            loaderShow()
                $.ajax({
                    url: "{{route('serah_barang')}}",
                    type: "POST",   
                    dataType:"JSON",
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') 
                    },
                    data: {
                        id_order
                    },
                    success: function(response) {
                        location.reload()
                    },
                    error: function(err) {
                        location.reload()
                    }    
                });
        }

    function closeWebcam() {
        console.log('tutup')
        Webcam.reset('.webcam-capture')
    }


    function start_take_capture_serah(id_order){
        $('#id_order_serah').val(id_order)
        var getwidth = ((window.innerWidth > 0) ? window.innerWidth : screen.width)-30;
        
            if(posisi === 'depan'){
                Webcam.set({
                    width: getwidth,
                    height: 450,
                    image_format: 'jpeg',
                    jpeg_quality:80,
                    // facingMode: { exact: 'user' }
                });
            } else {
                Webcam.set({
                    width: getwidth,
                    height: 450,
                    image_format: 'jpeg',
                    jpeg_quality:80,
                    facingMode: { exact: 'environment' }
                });
            }
        
    
            var cameras = new Array(); //create empty array to later insert available devices
            navigator.mediaDevices.enumerateDevices() // get the available devices found in the machine
            .then(function(devices) {
                devices.forEach(function(device) {
                var i = 0;
                    if(device.kind=== "videoinput"){ //filter video devices only
                        cameras[i]= device.deviceId; // save the camera id's in the camera array
                        i++;
                    }
                });
            })

            if(posisi === 'depan'){
                Webcam.set('constraints',{
                    width: getwidth,
                    height: 450,
                    image_format: 'jpeg',
                    jpeg_quality:80,
                    // facingMode: { exact: 'user' }
                });
            } else {
                Webcam.set('constraints',{
                    width: getwidth,
                    height: 450,
                    image_format: 'jpeg',
                    jpeg_quality:80,
                    facingMode: { exact: 'environment' }
                });
            }

            
        
            Webcam.on( 'error', function(err) {
                notif('bg-danger', err)
            } );
        
            Webcam.attach('.webcam-capture-serah');
            
        
        }


        function captureimageserah() {
            var penerima = $('#penerima_barang').val()

            if (penerima == '') {
                notif('bg-danger', "{{__('bahasa.notif_penerima_barang_harus_diisi')}}")
                return
            }
            Webcam.snap( function(data_uri) {
                $.ajax({
                    url: "{{route('serah_barang')}}",
                    type: "POST",   
                    dataType:"JSON",
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') 
                    },
                    data: {
                        id_order : $('#id_order_serah').val(),
                        penerima : penerima,
                        foto : data_uri
                    },
                    success: function(response) {
                        location.reload()
                    },
                    error: function(err) {
                        notif('bg-danger', err)
                    }    
                });
            } );
        }
    
    function closeWebcamSerah() {
        Webcam.reset('.webcam-capture-serah')
    }


    function editPesanan(id,nama_produk,satuan,jumlah,harga_jual,id_konsumen) {
        console.log(id+' || '+nama_produk+' || '+satuan+' || '+jumlah+' || '+harga_jual)
        $('#id_konsumen').val('')
        $('#id_pesanan').val('')
        $('#nama_barang').val('')
        $('#satuan_barang').val('')
        $('#qty_barang').val('')
        $('#harga_barang').val('')

        $('#ModalUpdatePesananShop').modal('show')

        $('#id_konsumen').val(id_konsumen)
        $('#id_pesanan').val(id)
        $('#nama_barang').val(nama_produk)
        $('#satuan_barang').val(satuan)
        $('#qty_barang').val(jumlah)
        $('#harga_barang').val(harga_jual)
    }
    </script>

@endsection
