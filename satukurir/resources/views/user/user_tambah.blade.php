@extends('layout.template')

@section('title', $title)

@section('content')
    <!-- App Capsule -->
    
    
    <div id="appCapsule" class="full-height">
        
        <form method="post" action="{{route('user_simpan')}}">
            @csrf
            <div class="section mt-2">
                <div class="section-title">User</div>
                <div class="card">
                    <div class="card-body">
                    <input type="hidden" class="form-control" id="id_user" name="id_user"  value="<?php if($detail_user) {echo $detail_user->id_konsumen;} else {echo'-';} ?>">
                        <div class="form-group basic">
                            <div class="input-wrapper">
                                <label class="label" for="nama_user">Nama User</label>
                                <input type="text" class="form-control" id="nama_user" name="nama_user" placeholder="Masukan Nama User" value="<?php if($detail_user) {echo $detail_user->nama_lengkap;} else {echo'';} ?>" required>
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <div class="form-group basic">
                            <div class="input-wrapper">
                                <label class="label" for="hp_user">No HP</label>
                                <input type="number" class="form-control" id="hp_user" name="hp_user" placeholder="Masukan Nomor HP" value="<?php if($detail_user) {echo $detail_user->no_hp;} else {echo'';} ?>" required>
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <div class="form-group basic">
                            <div class="input-wrapper">
                                <label class="label" for="email_user">Email</label>
                                <input type="email" class="form-control" id="email_user" name="email_user" placeholder="Masukan Email User" value="<?php if($detail_user) {echo $detail_user->email;} else {echo'';} ?>" required>
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <div class="form-group basic">
                            <div class="input-wrapper">
                                <label class="label" for="tanggal_lahir">Tanggal Lahir</label>
                                <input type="date" class="form-control" id="tanggal_lahir" name="tanggal_lahir" placeholder="Masukan Tanggal Lahir User" value="<?php if($detail_user) {echo $detail_user->tanggal_lahir;} else {echo'';} ?>" required>
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <div class="form-group basic">
                            <div class="input-wrapper">
                                <label class="label" for="cabang_user" >Cabang</label>
                                <select name="cabang_user" id="cabang_user" class="form-control" required>
                                    <option value="">--Pilih Cabang--</option>
                                    @foreach($list_cabang as $row)
                                    <option @if($detail_user->id_reseller == $row->id_reseller) ? selected : '' @endif @endphp value="{{$row->id_reseller}}">{{strtoupper($row->nama_reseller)}}</option>
                                    @endforeach
                                </select>
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>


                    </div>
                </div>
            </div>

            

            <div class="section mb-7 p-2">
                <div class="form-button transparent">
                    <button type="submit" class="btn btn-primary btn-block btn-lg">Simpan</button>
                    @if($detail_user)
                    <button type="button" class="btn btn-secondary btn-block btn-lg mt-5" onclick="notification('verif-delete')">Delete</button>
                    @endif
                </div>
            </div>
            

        </form>
    </div>


        @if($detail_user)
        <div id="verif-delete" class="notification-box" tabindex="-1">
            <div class="notification-dialog ios-style bg-danger">
                <div class="notification-header">
                    <div class="in">
                        <strong>User Akan Dihapus</strong>
                    </div>
                    <div class="right">
                        <a href="#" class="close-button">
                            <ion-icon name="close-circle"></ion-icon>
                        </a>
                    </div>
                </div>
                <div class="notification-content">
                    <div class="in">
                        <h3 class="subtitle">Yakin Menghapus User Ini?</h3>
                        <div class="text">
                            Tekan 'Ya' Untuk Mengkonfirmasi
                        </div>
                    </div>
                </div>
                <div class="notification-footer">
                    <a href="{{url('user_hapus/'.$detail_user->id_konsumen)}}"  class="notification-button" onclick="$('#DialogLoading').modal('show')">
                        Ya
                    </a>
                    <a href="#" class="notification-button close-button" data-dismiss="modal">
                        Batal
                    </a>
                </div>
            </div>
        </div>
        @endif

    <script>
        
        

        $( document ).ready(function() {
            

            
            
        });

        
    </script>
    
    <!-- * App Capsule -->

@endsection
