@extends('layout.template')



@section('content')
    <!-- App Capsule -->
    <div id="appCapsule">

    <div id="loader">
        <img src="{{asset('assets/img/loading-icon.png')}}" alt="icon" class="loading-icon">
    </div>

    
    

    <script>
        $( document ).ready(function() {
            handlePermission()
        });

        function handlePermission() {
            return navigator.permissions
                    .query({name:'notifications'})
                    .then(permissionQuery)
                    .catch(permissionError);
        }

        function permissionQuery(result) {
            console.log(result);
            var newPrompt;

            if (result.state == 'granted') {
                // window.location.href = "{{route('home')}}";
                // alert('Setujui')
                initFirebaseMessagingRegistration()

            } else if (result.state == 'prompt') {
                // we can ask the user
                // alert('Tanya')
                newPrompt = Notification.requestPermission();

            } else if (result.state == 'denied') {
                // notifications were disabled
                console.log('blocked')
                if (confirm("Ijinkan aplikasi mengirim notifikasi") == true) {
                    Notification.requestPermission(function (permission) {
                        if (permission === "granted") {
                            handlePermission();
                        }
                    });
                } else {
                    handlePermission()
                }
            }

            result.onchange = () => console.debug({updatedPermission: result});

            return newPrompt;
        }

        function permissionError(result) {
            console.debug({result});
        }
    
        
    </script>


    <!-- * App Capsule -->

@endsection
