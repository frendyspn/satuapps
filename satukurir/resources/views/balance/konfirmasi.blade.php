@extends('layout.template')

@section('title', $title)

@section('content')
    <!-- App Capsule -->
    <div id="appCapsule" class="full-height">
        <div class="section">
            <div class="splash-page mt-5 mb-5">

                <div class="transfer-verification">
                    <div class="transfer-amount">
                        <span class="caption">Jumlah</span>
                        <h2>Rp. {{number_format($deposit->nominal)}}</h2>
                    </div>
                    <div class="from-to-block mb-5">
                        <div class="arrow"></div>
                    </div>
                </div>
                @if($deposit->status == 'Pending')
                <form  method="post" action="{{url('upload_bukti_transfer')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="card">
                        <div class="card-body">

                                @csrf
                                <input type="hidden" name="id_deposit" id="id_deposit" value="{{$deposit->id_pendapatan}}">
                                <div class="custom-file-upload" id="fileUpload1">
                                    <input type="file" id="fileuploadInput" name="fileuploadInput" accept=".png, .jpg, .jpeg">
                                    <label for="fileuploadInput">
                                        <span>
                                            <strong>
                                                <ion-icon name="arrow-up-circle-outline" role="img" class="md hydrated" aria-label="arrow up circle outline"></ion-icon>
                                                <i>Bukti Transfer</i>
                                            </strong>
                                        </span>
                                    </label>
                                </div>

                                <button type="submit" class="btn btn-primary btn-block">Upload</button>



                        </div>
                    </div>
                </form>
                @endif
                <h2 class="mb-2 mt-2">Konfirmasi Transfer</h2>
                <p>
                    Silahkan transfer dengan nilai <strong class="text-primary">Rp. {{number_format($deposit->nominal)}}</strong> Pada salah satu bank dibawah <br>
                    @foreach($list_rekening as $rek)
                    <br/>
                    <h5>Bank: {{$rek->nama_bank}}</h5>
                    <h5>Nomor: {{$rek->no_rekening}}</h5>
                    <h5>Penerima: {{$rek->pemilik_rekening}}</h5>
                    @endforeach
                </p>
            </div>
        </div>
    </div>
    

@endsection
