<?php
// KAMBOJA
return [
    // Umum
    'ganti_bahasa' => 'ប្តូរ​ភាសា',
    'Nomor_HP' => 'លេខទូរស័ព្ទ',
    'Nama' => 'ឈ្មោះ',
    'Email' => 'អ៊ីមែល',
    'Pilih_Jenis_Kendaraan' => 'រើសប្រភេទរថយន្ត',
    'placeholder_merk_kendaraan' => 'ម៉ាក និង ប្រភេទរថយន្',
    'Nama_Lengkap' => 'ឈ្មោះ​ពេញ',
    'Jenis_Kelamin' => 'ភេទ',
    'Pilih_Jenis_Kelamin' => 'ជ្រើសរើសភេទ',
    'Laki_laki' => 'ប្រុស',
    'Perempuan' => 'ស្',
    'Tanggal_Lahir' => 'កាលបរិច្ឆេទកំណើត',
    'Tempat_Lahir' => 'កន្លែងកំណើត',
    'Provinsi' => 'ខេត្',
    'Kota' => 'ខេត្',
    'Kecamatan' => 'សង្កាត់',
    'Alamat' => 'អាសយដ្ឋាន (ឈ្មោះ​ផ្លូវ)',
    'Pilih_Provinsi' => 'ជ្រើសរើសខេត្',
    'Pilih_Kota' => 'ជ្រើសរើសក្រុង',
    'Pilih_Kecamatan' => 'ជ្រើសរើសសង្កាត់',
    'Plat_Nomor_Kendaraan' => 'លេខ​ស្លាក​រថយន្ត',
    'upload_lampiran' => 'ផ្ទុក​ឯកសារ​ជា​ទិន្នន័យ អត្តសញ្ញាណកំណើត',
    'Ambil_Foto_Lampiran' => 'ថតរូបបញ្ជូល',
    'title_upload_lampiran' => 'រូបថតបញ្ជូលជា​កាតសម្គាល់',
    'button_ambil_gambar' => 'ថត​រូប',
    'Saldo' => 'សមតុល្',
    'kurs' => 'KHR.',
    'Pendapatan' => 'ប្រាក់ចំណូល',
    'Komisi' => 'កម្មវិធី',
    'hari_ini' => 'ថ្ងៃនេះ',
    'bulan_ini' => 'ខែនេះ',
    'tahun_ini' => 'ឆ្នាំនេះ',
    'total_saldo' => 'សមតុល្យសរុប',
    'title_ambil_foto_barang' => 'រូបថតផលិតផលដែលបានយក',
    'title_serah_foto_barang' => 'រូបថតផលិតផលដែលបានបញ្ជូន',
    'title_button_tutup' => 'បិទ',
    'title_button_update' => 'កែប្រែ',
    'title_edit_pesanan' => 'កែប្រែ​ការកម្មង់',
    'masukan_otp' => 'បញ្ចូល​កូដ OTP',
    'jangan_refresh_halaman_otp' => 'កុំ​រៀបការបង្ហាញទំព័រនេះ',
    'verifikasi' => 'ការផ្ទៀងផ្ទាត់',
    'nama_barang' => 'ឈ្មោះផលិតផល',
    'satuan' => 'ឯកត្រូនិក',
    'qty' => 'ចំនួន',
    'harga_satuan' => 'តម្លៃឯកត្រូនិក',
    'Beranda' => 'ទំព័រដើម',
    'History' => 'ប្រវត្តិ',
    'Keluar' => 'ចាកចេញ',
    'Perhatian' => 'សូមប្រើប្រាស់',
    'mohon_tunggu' => 'សូមរង់ចាំ',
    'ambil_order' => 'យក​ការបញ្ជាទិញ',
    'tujuan' => 'ទិសដៅ',
    'pickup' => 'យកទៅ',
    'tunai' => 'សាច់ប្រាក់',
    'nontunai' => 'អេឡិចវិត',
    'dari' => 'ពី',
    'ke' => 'ទៅ',
    'pendapatan_kurir' => 'ចំណូលអ្នកដឹកជញ្ជូន',
    'Potongan admin kurir' => 'ការពិនិត្យការដឹកជញ្ជូន',
    'History' => 'ប្រវត្តិ',


    // Notifikasi
    'notif_penerima_barang_harus_diisi' => 'ឈ្មោះ​អ្នក​ទទួល​ត្រូវ​តែ​ត្រូវ​បំពេញ',
    'notif_browser_tidak_mendukung_geolocasi' => 'សុំទោស កម្មវិធីរុក្ខារបស់អ្នកមិនគាំទ្រការសណ្ឋានគ្រប់គ្រង HTML5 ទេ។',
    'notif_browser_tidak_membagikan_lokasi' => 'អ្នកបានជ្រើសរើសកម្មវិធីកូនរបស់អ្នកដើម្បីមិនចែកចាយទីតាំងរបស់អ្នកទេប៉ុន្តែមិនបានយកពីអ្នកទេ។ យើងមិននឹងសួរអ្នកទទួលវិញទេ។',
    'notif_browser_gps_tidak_ada' => 'បណ្ដាញ​មិន​សកម្ម ឬ​សេវាកម្ម​កំណត់​ទី​តាំង​មិន​អាច​ទទួល​បាន​ទេ។',
    'notif_timeout_lokasi' => 'ពេលព្រមានកម្មវិធីបានបញ្ចប់មុននឹងអាចទទួលទិន្នន័យទីតាំងបាន.',
    'belum_terdaftar_sebagai_kurir' => 'មិនទាន់ចុះឈ្មោះជាអ្នកដឹកជញ្ជូននៅឡើងទេ',
    'Nomor HP Tidak Terdaftar' => 'លេខទូរស័ព្ទមិនបានចុះឈ្មោះ',
    'Terjadi_Kesalahan_Silahkan_Ulangi_Lagi' => 'មានកំហុសកើតឡើង សូមព្យាយាមម្តងទៀត',
    'Kode_OTP_Salah' => 'លេខ OTP មិនត្រឹមត្រូវ',
    'notif_akun_suspend' => 'គណនីរបស់អ្នកត្រូវបានផ្អាក',
    'notif_belum_ada_order' => 'មិនមានការបញ្ជាទិញថ្មីទេ',
    'notif_false_order_saldo_kurang' => 'មិនបានយកការបញ្ជាទិញទេ, សូមបញ្ចូលសមតុល្យរបស់អ្នក',

    // Login
    'Masuk' => 'ចូល',
    'Lengkapi_Form_Dibawah' => 'បំពេញទម្រង់ខាងក្រោម',
    'Daftar_Disini' => 'ចុះឈ្មោះនៅទីនេះ',
    'Log_in' => 'ចូល',
    
    // Form Pendaftaran
    'Daftar' => 'ចុះឈ្មោះ',
    'Saya_Sudah_Terdaftar' => 'ញុំបានចុះឈ្មោះរួចហើយ',
    'Lanjutkan' => 'បន្ត',
    
];