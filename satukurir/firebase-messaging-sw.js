/*
    Give the service worker access to Firebase Messaging.
    Note that you can only use Firebase Messaging here, other Firebase libraries are not available in the service worker.
    */
    importScripts('https://www.gstatic.com/firebasejs/8.3.2/firebase-app.js');
    importScripts('https://www.gstatic.com/firebasejs/8.3.2/firebase-messaging.js');
    /*
    Initialize the Firebase app in the service worker by passing in the messagingSenderId.
    * New configuration for app@pulseservice.com
    */
    firebase.initializeApp({
    
      apiKey: "AIzaSyBRM-xAqytalTRmEYj7jTmLgf7vxEQNmcU",
      authDomain: "satukurirwebpush.firebaseapp.com",
      projectId: "satukurirwebpush",
      storageBucket: "satukurirwebpush.appspot.com",
      messagingSenderId: "642680161917",
      appId: "1:642680161917:web:9b0a9e5f3e074ecbce9314"
    });


    
    /*
    Retrieve an instance of Firebase Messaging so that it can handle background messages.
    */
    const messaging = firebase.messaging();
    messaging.setBackgroundMessageHandler(function({data:{title,body,icon}}) {
      // return self.registration.showNotification('apaaaaaa',{body,icon});
      if (!("Notification" in window)) {
        alert("This browser does not support system notifications.");
      } else if (Notification.permission === "granted") {
          // If it's okay let's create a notification
          var notification = new Notification('apaaaa',{body});
          notification.onclick = function(event) {
              event.preventDefault();
              window.open(payload.notification.click_action , '_blank');
              notification.close();
          }
      }
      console.log('Message received. ');
  });