<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Http;
use Session;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;

use App;
use DB;

class LoginController extends Controller
{

    public function index()
    {
        App::setLocale(session()->get('locale'));
        if (cek_login()){
            // return redirect('/home');
        }
        
        $data['title'] = 'Masuk';
        // $data['header'] = 'goback';

        return view('login',$data);
    }


    public function prosesLogin(Request $req)
    {
        $validated = $req->validate([
            'nomor_hp' => 'required|numeric',
        ]);

        $no_hp = $req->nomor_hp;
        $response = Http::attach('username',$no_hp) 
            ->withHeaders([ 
                'Authorization'=> api_token(),
            ]) 
            ->post(api_url().'api/kurir/login'); 
        $result = json_decode($response->body());

        // echo $response->body();
        // return;

        if($response->getStatusCode() == '200'){
            return redirect('/otp')->with(['nomor' => $no_hp, 'otp' => 'Ini Hanya Untuk Development, OTP Login '.$result->Message]);
        } else {
            return redirect('/login')->with(['error_msg' => $result->Message]);
        }

    }


    public function otpLogin(Request $req)
    {
        App::setLocale(session()->get('locale'));
        if (Session::get('nomor') == '') {
            return redirect('/login')->with(['error_msg' => __('bahasa.Terjadi_Kesalahan_Silahkan_Ulangi_Lagi')]);
        }

        $data['title'] = 'OTP';
        $data['header'] = 'goback';

        return view('otp',$data);
    }


    public function prosesOtp(Request $req)
    {
        $validated = $req->validate([
            'nomor_hp' => 'required|numeric',
            'otp' => 'required|numeric',
        ]);

        $response = Http::attach('username',$req->nomor_hp)
        ->attach('otp',$req->otp) 
        ->withHeaders([ 
            'Authorization'=> api_token(),
        ]) 
        ->post(api_url().'api/kurir/verifikasi_otp'); 

        $result = json_decode($response->body());

        if($response->getStatusCode() == '200'){
            if ($this->getDataToken($result->token) == null) {
                return redirect('/login')->with(['error_msg' => __('bahasa.belum_terdaftar_sebagai_kurir')]);
            }

            Session::put('token', $result->token);
            Session::put('nama_lengkap', $result->nama_lengkap);
            Session::put('email', $result->email);
            Session::put('no_hp', $result->no_hp);
            Session::put('level', $result->level);

            return redirect('/device_token');
            // return view('layout.loadingLogin');

            // return redirect('/home')->with(['sukses' => 'Selamat Datang, '.$result->nama_lengkap]);
        } else {
            $error = str_replace(' ','_',$result->Message);
            return redirect('/login')->with(['error_msg' => $error]);
        }
    }

    public function GetDeviceToken(Request $req)
    {
        return view('layout.loadingLogin');
    }

    public function saveToken($device_token, $latitude, Request $req)
    {
        $dtToken['device_token'] = $device_token;
        $dtToken['latitude'] = $latitude;
        $user_token = Session::get('token');

        
        $getUser = DB::table('rb_konsumen')->where('remember_token', $user_token)->first();

        if (!$getUser) {
            return redirect('/login');
        }
        
        $updateToken = DB::table('rb_sopir')->where('id_konsumen', $getUser->id_konsumen)->update($dtToken);

        return redirect('home');
    }

    public function logout(){
        Session::flush();
        return redirect('login');
    }




    public function registrasi()
    {
        App::setLocale(session()->get('locale'));
        $data['title'] = 'Registrasi';
        // $data['header'] = 'goback';

        return view('registrasi',$data);
    }


    public function registrasiCekHP(Request $req)
    {
        App::setLocale(session()->get('locale'));
        $validated = $req->validate([
            'nomor_hp' => 'required|numeric',
        ]);

        $no_hp = $req->nomor_hp;

        $cekExist = DB::table('rb_konsumen')->where('no_hp', $no_hp)->first();

        if ($cekExist) {

            $cekRegistered = DB::table('rb_sopir')->where('id_konsumen', $cekExist->id_konsumen)->first();

            if($cekRegistered){
                return redirect('/login')->with(['error_msg' => 'Sudah Terdaftar, Silahkan Login']);
            }
            
            $response = Http::attach('username',$no_hp) 
                ->withHeaders([ 
                    'Authorization'=> api_token(),
                ]) 
                ->post(api_url().'api/kurir/login'); 
            $result = json_decode($response->body());
    
            if($response->getStatusCode() == '200'){
                return redirect('/otp_registrasi')->with(['nomor' => $no_hp, 'otp' => 'Ini Hanya Untuk Development, OTP Login '.$result->Message]);
            } else {
                return redirect('/registrasi')->with(['error_msg' => $result->Message]);
            }
        }

        $data['title'] = 'Registrasi';
        $data['nomor'] = $no_hp;
        return view('registrasi_data_diri', $data);
    }


    public function otpRegistrasi(Request $req)
    {
        App::setLocale(session()->get('locale'));
        if (Session::get('nomor') == '') {
            return redirect('/login')->with(['error_msg' => __('bahasa.Terjadi_Kesalahan_Silahkan_Ulangi_Lagi')]);
        }

        $data['title'] = 'OTP';
        $data['header'] = 'goback';

        return view('otp_registrasi',$data);
    }


    public function prosesOtpRegistrasi(Request $req)
    {
        $validated = $req->validate([
            'nomor_hp' => 'required|numeric',
            'otp' => 'required|numeric',
        ]);

        $response = Http::attach('username',$req->nomor_hp)
        ->attach('otp',$req->otp) 
        ->withHeaders([ 
            'Authorization'=> api_token(),
        ]) 
        ->post(api_url().'api/kurir/verifikasi_otp'); 

        $result = json_decode($response->body());

        if($response->getStatusCode() == '200'){
            Session::put('token', $result->token);
            Session::put('nama_lengkap', $result->nama_lengkap);
            Session::put('email', $result->email);
            Session::put('no_hp', $result->no_hp);
            Session::put('level', $result->level);

            return redirect('/confirm_registrasi');
            // return view('layout.loadingLogin');

            // return redirect('/home')->with(['sukses' => 'Selamat Datang, '.$result->nama_lengkap]);
        } else {
            return redirect('/registrasi')->with(['error_msg' => $result->Message]);
        }
    }

    public function confirmRegistrasi(Request $req)
    {
        App::setLocale(session()->get('locale'));
        $data['title'] = 'Konfirmasi Pendaftaran';
        $data['header'] = 'goback';

        $data['jenis_kendaraan'] = DB::table('rb_jenis_kendaraan')->get();

        return view('registrasi_confirm',$data);
    }


    public function confirmRegistrasiProses(Request $req)
    {
        if (!cek_login()){
            return redirect('/registrasi');
        }

        $validated = $req->validate([
            'jenis_kendaraan' => 'required',
            'merek' => 'required',
            'plat_nomor' => 'required',
            'lampiran' => 'required',
        ]);

        $dtKonsumen = DB::table('rb_konsumen')->where('remember_token', Session::get('token'))->first();


        $dt_sopir['id_konsumen'] = $dtKonsumen->id_konsumen;
        $dt_sopir['kecamatan_id'] = $dtKonsumen->kecamatan_id;
        $dt_sopir['kota_id'] = $dtKonsumen->kota_id;
        $dt_sopir['provinsi_id'] = $dtKonsumen->provinsi_id;
        $dt_sopir['id_jenis_kendaraan'] = $req->jenis_kendaraan;
        $dt_sopir['plat_nomor'] = $req->plat_nomor;
        $dt_sopir['merek'] = $req->merek;
        $dt_sopir['lampiran'] = $req->lampiran;
        $dt_sopir['aktif'] = 'N';
        $dt_sopir['lainnya'] = '-';

        DB::table('rb_sopir')->insert($dt_sopir);

        return redirect('/login')->with(['sukses' => 'Pendaftaran Berhasil, Silahkan Tunggu Admin Mengkonfirmasi Akun Anda']);
    }

    public function dataDiriRegistrasiProses(Request $req)
    {
        $validated = $req->validate([
            'nomor_hp' => 'required|numeric|unique:rb_konsumen,no_hp',
            'nama' => 'required',
            'email' => 'required|unique:rb_konsumen,email',
            'jenis_kelamin' => 'required',
            'tanggal_lahir' => 'required',
            'tempat_lahir' => 'required',
            'provinsi' => 'required',
            'kota' => 'required',
            'kecamatan' => 'required',
            'alamat' => 'required',
        ]);

        $no_hp = $req->nomor_hp;
        $nama = $req->nama;
        $email = $req->email;
        $jenis_kelamin = $req->jenis_kelamin;
        $tanggal_lahir = $req->tanggal_lahir;
        $tempat_lahir = $req->tempat_lahir;
        $provinsi = $req->provinsi;
        $kota = $req->kota;
        $kecamatan = $req->kecamatan;
        $alamat = $req->alamat;

        $dtKonsumen['username'] = $email;
        $dtKonsumen['password'] = Hash::make('password');
        $dtKonsumen['nama_lengkap'] = $nama;
        $dtKonsumen['email'] = $email;
        $dtKonsumen['jenis_kelamin'] = $jenis_kelamin;
        $dtKonsumen['tanggal_lahir'] = $tanggal_lahir;
        $dtKonsumen['tempat_lahir'] = $tempat_lahir;
        $dtKonsumen['alamat_lengkap'] = $alamat;
        $dtKonsumen['kecamatan_id'] = $kecamatan;
        $dtKonsumen['kota_id'] = $kota;
        $dtKonsumen['provinsi_id'] = $provinsi;
        $dtKonsumen['no_hp'] = $no_hp;
        $dtKonsumen['foto'] = '';
        $dtKonsumen['tanggal_daftar'] = date('Y-m-d');

        DB::table('rb_konsumen')->insert($dtKonsumen);

        $response = Http::attach('username',$no_hp) 
            ->withHeaders([ 
                'Authorization'=> api_token(),
            ]) 
            ->post(api_url().'api/kurir/login'); 
        $result = json_decode($response->body());
    
        if($response->getStatusCode() == '200'){
            return redirect('/otp_registrasi')->with(['nomor' => $no_hp, 'otp' => 'Ini Hanya Untuk Development, OTP Login '.$result->Message]);
        } else {
            return redirect('/registrasi')->with(['error_msg' => $result->Message]);
        }
    }
}
