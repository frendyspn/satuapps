<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Session;
use DB;
use App;
class HistoryController extends Controller
{
    public function index(Request $req)
    {
        App::setLocale(session()->get('locale'));
        if (!cek_login()){
            return redirect('/login');
        }

        if ($this->getDataToken(Session::get('token')) == null) {
            return redirect('/home');
        }
        
        $data['title'] = __('bahasa.History');
        $data['header'] = 'goback';
        $data['menu'] = '';
        $data['tglAwal'] = date('Y-m-01');
        $data['tglAkhir'] = date('Y-m-d');
        $data['pendapatanHariIni'] = DB::table('rb_pendapatan_kurir')->select(DB::raw('sum(nominal) as saldo'))->where('id_konsumen', $this->getDataToken(Session::get('token'))->id_konsumen)->where('transaksi', 'debit')->whereBetween('waktu_pendapatan', [date('Y-m-d 00:00:01'), date('Y-m-d 23:59:59')])->first()->saldo;
        $data['pendapatanBulanIni'] = DB::table('rb_pendapatan_kurir')->select(DB::raw('sum(nominal) as saldo'))->where('id_konsumen', $this->getDataToken(Session::get('token'))->id_konsumen)->where('transaksi', 'debit')->whereBetween('waktu_pendapatan', [date('Y-m-01'),date('Y-m-t')])->first()->saldo;
        $data['pendapatanTahunIni'] = DB::table('rb_pendapatan_kurir')->select(DB::raw('sum(nominal) as saldo'))->where('id_konsumen', $this->getDataToken(Session::get('token'))->id_konsumen)->where('transaksi', 'debit')->whereBetween('waktu_pendapatan', [date('Y-01-01'),date('Y-12-t')])->first()->saldo;

        return view('history',$data);
    }


    public function searchData(Request $req)
    {
        App::setLocale(session()->get('locale'));
        $tglAwal = date('Y-m-d 00:00:01', strtotime($req->tgl_awal));
        $tglAkhir = date('Y-m-d 23:59:59', strtotime($req->tgl_akhir));

        $user_id = $this->getDataToken(Session::get('token'))->id_sopir;

        $getData = DB::table('kurir_order')->where('id_sopir', $user_id)->whereBetween('tanggal_order', [$tglAwal, $tglAkhir])->orderBy('tanggal_order','desc')->get();

        echo '
        <div class="section mt-2">
            <div class="card">
                <ul class="listview flush transparent no-line image-listview detailed-list mt-1 mb-1">';

                if (count($getData) <= 0) {
                    echo '<span class="badge bg-danger align-center">'.__('bahasa.notif_belum_ada_order').'</span>';
                }
        foreach ($getData as $val) {
            $responseJemput = Http::attach('token',Session::get('token')) 
            ->withHeaders([ 
                'Authorization'=> api_token(),
            ]) 
            ->post('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$val->titik_jemput.'&key=AIzaSyA3LBjGfAARNWUtl7mVk2APMsi4w4Y9Nx8');

            $dtJemput = json_decode($responseJemput->body());

            $responseAntar = Http::attach('token',Session::get('token')) 
            ->withHeaders([ 
                'Authorization'=> api_token(),
            ]) 
            ->post('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$val->titik_antar.'&key=AIzaSyA3LBjGfAARNWUtl7mVk2APMsi4w4Y9Nx8');

            $dtAntar = json_decode($responseAntar->body());

            if ($val->status == 'CANCEL') {
                $status = '<span class="badge bg-danger">'.$val->status.'</span>';
            } else if ($val->status == 'FINISH') {
                $status = '<span class="badge bg-primary">'.$val->status.'</span>';
            } else {
                $status = '<span class="badge bg-warning">'.$val->status.'</span>';
            }

            if ($val->metode_pembayaran == 'CASH') {
                $pembayaran = '<small class="badge bg-danger">'.__('bahasa.tunai').'</small>';
            } else {
                $pembayaran = '<small class="badge bg-warning">'.__('bahasa.nontunai').'</small>';
            }
            

            echo '
            <li>
                <a href="#" class="item">
                    <div class="in">
                        <div>
                            <strong>'.date('d M Y', strtotime($val->tanggal_order)).$status.'</strong>
                            <div class="text-small text-secondary"><span class="fw-bold">'.__('bahasa.dari').': </span>'.$dtJemput->plus_code->compound_code.'</div>
                            <div class="text-small text-secondary"><span class="fw-bold">'.__('bahasa.ke').': </span>'.$dtAntar->plus_code->compound_code.'</div>
                        </div>
                        <div class="text-end">
                            <strong>'.__('bahasa.kurs').number_format($val->tarif).'</strong>
                            <div>'.$pembayaran.'</div>
                            <div>&nbsp;</div>
                            <div>&nbsp;</div>
                            <div>&nbsp;</div>
                        </div>
                    </div>
                </a>
            </li>
            ';
        }
        echo '
                </div>
            </div>
        </div>';

    }


}