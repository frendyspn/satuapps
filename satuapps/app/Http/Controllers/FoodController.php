<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Session;
use DB;
use App;
class FoodController extends Controller
{
    public function index(Request $req)
    {
        App::setLocale(session()->get('locale'));

        if (!cek_login()){
            return redirect('/login');
        }
        $cekUser = DB::table('rb_konsumen')->where('remember_token', Session::get('token'))->first();
        $cekOrderExist = DB::table('kurir_order')->where('id_pemesan', $cekUser->id_konsumen)->whereNotIn('status', ['FINISH', 'CANCEL'])->first();
        if ($cekOrderExist) {
            return redirect('detail_food/'.base64_encode($cekOrderExist->id));
        }
        $data['saldo'] = get_saldo(Session::get('token'));
        
        $data['title'] = 'Food';
        $data['header'] = 'default';
        $data['menu'] = '';

        return view('food.home',$data);
    }


    public function getOngkos(Request $req)
    {
        $titikJemput = $req->titikJemput;
        $titikAntar = $req->titikAntar;

        $response = Http::withHeaders([ 
            'Accept'=> '*/*', 
        ]) 
        ->get('https://apisatutoko.kitabuatin.com/api/kurir/v1/hitungJarak/'.trim($titikJemput,' ').'/'.trim($titikAntar, ' ').'/REGULAR');
        
        if ($response->body() < 0) {
            http_response_code(404);
            exit(json_encode('Jarak Terlalu Jauh'));
        }

        http_response_code(200);
        $data['ongkir'] = number_format($response->body());

        return $data;
    }


    

    public function detailOrder($id)
    {
        App::setLocale(session()->get('locale'));

        if (!cek_login()){
            return redirect('/login');
        }

        $id = base64_decode($id);

        $data['order'] = DB::table('kurir_order as a')->select('a.id', 'a.titik_jemput', 'a.titik_antar', 'a.service', 'a.id_penjualan', 'a.status', 'a.source', 'a.tarif', 'a.id_sopir', 'a.kode_order', 'c.nama_reseller as nama_toko', 'c.alamat_lengkap as alamat_toko', 'd.nama_lengkap as nama_konsumen', 'd.alamat_lengkap as alamat_konsumen', 'a.pemberi_barang', 'a.alamat_jemput', 'a.alamat_antar', 'a.penerima_barang', DB::raw('(select sum(jumlah*(harga_jual-diskon)) from rb_penjualan_detail where id_penjualan = a.id_penjualan) as total_belanja'))->leftJoin('rb_penjualan as b', 'b.id_penjualan', 'a.id_penjualan')->leftJoin('rb_reseller as c', 'c.id_reseller', 'b.id_penjual')->leftJoin('rb_konsumen as d', 'd.id_konsumen', 'b.id_pembeli')->where('a.id', $id)->first();
        $data['kurir'] = DB::table('rb_sopir as a')->select('a.*', 'b.nama_lengkap')->leftJoin('rb_konsumen as b', 'b.id_konsumen', 'a.id_konsumen')->where('a.id_sopir', $data['order']->id_sopir)->first();
        $data['timeline'] = DB::table('kurir_order_log')->where('id_order', $id)->get();
        $data['warna_timeline'] = ['bg-primary','bg-info','bg-danger','bg-warning'];
        
        $data['title'] = 'Detail Order';
        $data['header'] = 'default';
        $data['menu'] = '';

        return view('food.detail',$data);
    }


    public function getRestoLongLat(Request $req)
    {
        App::setLocale(session()->get('locale'));

        $longlat = explode(',', $req->longlat) ;

        $max_distance = DB::table('rb_config')->where('field', 'max_jarak_km')->first();

        $results = DB::table('rb_kategori_produk as a')
        ->select(
            'b.id_reseller', 'c.nama_reseller', 'c.kordinat', 'c.foto',
            DB::raw('6371 * 2 * ASIN(SQRT(POWER(SIN(('.$longlat[0].' - CAST(SUBSTRING_INDEX(c.kordinat, \',\', 1) AS DECIMAL(9, 6))) * PI() / 180 / 2), 2) + COS('.$longlat[0].' * PI() / 180) * COS(CAST(SUBSTRING_INDEX(c.kordinat, \',\', -1) AS DECIMAL(9, 6)) * PI() / 180) * POWER(SIN(('.$longlat[1].' - CAST(SUBSTRING_INDEX(c.kordinat, \',\', -1) AS DECIMAL(9, 6))) * PI() / 180 / 2), 2))) AS distance'))
        ->leftJoin('rb_produk as b', 'b.id_kategori_produk', '=', 'a.id_kategori_produk')
        ->leftJoin('rb_reseller as c', 'c.id_reseller', '=', 'b.id_reseller')
        ->where('a.is_food', '1');

        if ($max_distance->value > 0) {
            $results = $results->where(DB::raw('6371 * 2 * ASIN(SQRT(POWER(SIN(('.$longlat[0].' - CAST(SUBSTRING_INDEX(c.kordinat, \',\', 1) AS DECIMAL(9, 6))) * PI() / 180 / 2), 2) + COS('.$longlat[0].' * PI() / 180) * COS(CAST(SUBSTRING_INDEX(c.kordinat, \',\', -1) AS DECIMAL(9, 6)) * PI() / 180) * POWER(SIN(('.$longlat[1].' - CAST(SUBSTRING_INDEX(c.kordinat, \',\', -1) AS DECIMAL(9, 6))) * PI() / 180 / 2), 2)))'), '<=', $max_distance->value);
        }

        $results = $results->whereNotNull('c.kordinat')
        ->groupBy('c.nama_reseller', 'c.kordinat','b.id_reseller', 'c.foto')
        ->limit(20)
        ->get();


        $dt = '';

        if (count($results) > 0) {
            foreach ($results as $row) {
                $dt .= '
                <div class="col-6 mb-2">
                    <div class="bill-box">
                        <div class="img-wrapper">
                            <img src="https://satutoko.id/asset/foto_user/'.$row->foto.'" alt="img" class="image-block imaged w48">
                        </div>
                        <div class="price" style="font-size:1rem">'.$row->nama_reseller.'</div>
                        <p><ion-icon name="location-outline"></ion-icon>'.number_format($row->distance,2).' KM</p>
                        <a href="'.url('get_resto_menu/'.base64_encode($row->id_reseller)).'" class="btn btn-primary btn-block btn-sm">'.__('bahasa.btn_beli_disini').'</a>
                    </div>
                </div>
                ';
            }
        } else {
            $dt .= '
            <div class="col-12 mb-2">
                <div class="bill-box">
                    '.__('bahasa.notif_belum_ada_toko_disekitar').'...
                </div>
            </div>
            ';
        }
        
        

        echo $dt;
    }


    public function getRestoMenu($id)
    {
        App::setLocale(session()->get('locale'));

        $id = base64_decode($id);

        $dtReseller = DB::table('rb_reseller')->where('id_reseller', $id)->first();

        $dtMenu = DB::table('rb_kategori_produk as a')
        ->select(
            'b.id_reseller', 'b.id_produk', 'b.nama_produk', 'b.satuan', 'b.harga_reseller', 'b.harga_konsumen', 'b.harga_premium', 'b.gambar', 'a.nama_kategori', 'a.id_kategori_produk'
        )
        ->leftJoin('rb_produk as b', 'b.id_kategori_produk', '=', 'a.id_kategori_produk')
        ->where('a.is_food', '1')
        ->where('b.id_reseller', $id)
        ->orderBy('a.id_kategori_produk')
        ->orderBy('b.nama_produk')
        ->get();

        $data['title'] = 'Food';
        $data['header'] = $dtReseller->nama_reseller;
        $data['menu'] = '';
        $data['produk'] = $dtMenu;

        return view('food.resto_detail',$data);
    }


    public function addCartFood(Request $req)
    {
        if (!Session::has('cart_food')) {
            $arrCart['id_reseller'] = '';
            $arrCart['keranjang'] = array();
            Session::put('cart_food', json_encode($arrCart));
        }

        $cart_exist = json_decode(Session::get('cart_food'), true);

        // return json_encode($cart_exist['keranjang']);

        $id_reseller    = $req->id_reseller;
        $id_produk      = $req->id_produk;
        $nama_produk    = $req->nama_produk;
        $harga_produk   = (int)$req->harga_produk;
        $gambar         = $req->gambar;
        $replace        = $req->replace;

        if ($cart_exist['id_reseller'] == $id_reseller || $cart_exist['id_reseller'] == '' ) {
            if ($cart_exist['id_reseller'] == '') {
                $cart_exist['id_reseller'] = $id_reseller;
            }
            $key = array_search($id_produk, array_column($cart_exist['keranjang'], 'id_produk'));
            // return $id_produk.' - '.$key;
            if ($key == '') {
                $dtArr['id_produk'] = $id_produk;
                $dtArr['nama_produk'] = $nama_produk;
                $dtArr['harga_produk'] = $harga_produk;
                $dtArr['gambar_produk'] = $gambar;
                $dtArr['qty'] = 1;
                array_push($cart_exist['keranjang'], $dtArr);
            } else {
                $cart_exist['keranjang'][$key]['qty'] = $cart_exist['keranjang'][$key]['qty'] + 1;
            }
            
            
        } else {

        }

        Session::put('cart_food', json_encode($cart_exist));
        return json_encode($cart_exist);
    }

    public function viewCartFood(Request $req)
    {
        if (!Session::has('cart_food')) {
            $arrCart['id_reseller'] = '';
            $arrCart['keranjang'] = array();
            Session::put('cart_food', json_encode($arrCart));
        }

        return Session::get('cart_food');
    }

    public function indexCart(Request $req)
    {
        $data['title'] = 'Cart Food';
        $data['header'] = 'goback';
        $data['menu'] = '';
        

        return view('food.cart',$data);
    }

    public function getCart(Request $req)
    {
        App::setLocale(session()->get('locale'));

        if (!Session::has('cart_food')) {
            return '<div class="col-12">'.__('bahasa.notif_keranjang_kosong').'</div>';
            // $arrCart['id_reseller'] = '';
            // $arrCart['keranjang'] = array();
        }

        $arrCart = json_decode(Session::get('cart_food'), true);

        // return json_encode($arrCart['keranjang']);

        $dt = '';

        $total_belanja = 0;
        $ongkir = 0;

        for ($i=0; $i < count($arrCart['keranjang']); $i++) { 
            $dt .='
                <a href="#" class="item">
                    <div class="detail">
                        <img src="https://satutoko.id/asset/foto_produk/'.$arrCart['keranjang'][$i]['gambar_produk'].'" alt="img" class="image-block imaged w48">
                        <div>
                            <strong>'.$arrCart['keranjang'][$i]['nama_produk'].'</strong>
                            <p>'.__('bahasa.kurs').number_format($arrCart['keranjang'][$i]['harga_produk']).'</p>
                        </div>
                    </div>
                    <div class="right">
                        <div class="price" style="text-align:right">'.__('bahasa.kurs').number_format($arrCart['keranjang'][$i]['harga_produk'] * $arrCart['keranjang'][$i]['qty']).'</div>
                        <div class="row">
                            <div class="col border">
                                <span onclick="update_cart('.$i.', '.($arrCart['keranjang'][$i]['qty']-1).')"><ion-icon name="remove-outline"></ion-icon></span>
                            </div>
                            <div class="col">
                                <input onchange="update_cart('.$i.', $(this).val())" type="text" style="width:50px;text-align:center" value="'.$arrCart['keranjang'][$i]['qty'].'">
                            </div>
                            <div class="col border">
                                <span onclick="update_cart('.$i.', '.($arrCart['keranjang'][$i]['qty']+1).')"><ion-icon name="add-outline"></ion-icon></span>
                            </div>
                        </div>
                    </div>
                </a>
                ';
            $total_belanja = $total_belanja + ($arrCart['keranjang'][$i]['harga_produk'] * $arrCart['keranjang'][$i]['qty']);
        }
        
        $longlat = explode(',', $req->titik_antar) ;
        $titik_jemput = DB::table('rb_reseller')->select('*', DB::raw('6371 * 2 * ASIN(SQRT(POWER(SIN(('.$longlat[0].' - CAST(SUBSTRING_INDEX(kordinat, \',\', 1) AS DECIMAL(9, 6))) * PI() / 180 / 2), 2) + COS('.$longlat[0].' * PI() / 180) * COS(CAST(SUBSTRING_INDEX(kordinat, \',\', -1) AS DECIMAL(9, 6)) * PI() / 180) * POWER(SIN(('.$longlat[1].' - CAST(SUBSTRING_INDEX(kordinat, \',\', -1) AS DECIMAL(9, 6))) * PI() / 180 / 2), 2))) AS distance'))->where('id_reseller', $arrCart['id_reseller'])->first();

        
        $response = Http::withHeaders([ 
            'Accept'=> '*/*', 
        ]) 
        ->get('https://apisatutoko.kitabuatin.com/api/kurir/v1/hitungJarak/'.trim($titik_jemput->kordinat,' ').'/'.trim($req->titik_antar, ' ').'/REGULAR');
        
        if ($response->body() > 0) {
            $ongkir = (int)$response->body();
        }

        $dt .='
        <ul class="listview flush transparent simple-listview no-space mt-3 bg-white">
                <li>
                    <strong>'.__('bahasa.sub_total').'</strong>
                    <span>'.number_format($total_belanja).'</span>
                </li>
                <li>
                    <strong>'.__('bahasa.biaya_kirim').' - '.number_format($titik_jemput->distance).'KM</strong>
                    <span>'.number_format($ongkir).'</span>
                </li>
                <li>
                    <strong>'.__('bahasa.total').'</strong>
                    <h3>'.number_format($total_belanja + $ongkir).'</h3>
                </li>
            </ul>
        ';

        $saldo = get_saldo(Session::get('token'));

        $dt .= '
        <form method="post" action="'.route('put_order_food').'">
        '.csrf_field().'
        <div class="btn-group mt-2 col-12" role="group">
            <input type="radio" class="btn-check" name="pembayaran_via" value="CASH" id="pembayaran_cash"'; 
            
            if($saldo <= 0) {$dt .='checked=""';}
            
            $dt .= '>
            <label class="btn btn-outline-primary" for="btnradio1">'.__('bahasa.tunai').'</label>

            <input type="radio" class="btn-check" name="pembayaran_via" value="WALLET" id="pembayaran_sallet"'; 
            
            if($saldo > 0) {$dt .='checked=""';}
            if($saldo <= 0) {$dt .='disabled="disabled"';}
            
            $dt .='>
            <label class="btn btn-outline-primary" for="btnradio2">'.__('bahasa.nontunai').' '.__('bahasa.kurs').number_format($saldo).'</label>
        </div>


        <div class="accordion-item">
            
            <input type="hidden" id="titik_antar" name="titik_antar" value="'.$req->titik_antar.'">
            <input type="hidden" id="alamat_antar" name="alamat_antar" value="'.$req->alamat_antar.'">
            <button class="btn btn-primary btn-block mt-2" id="btn-order">Order '.__('bahasa.kurs').number_format($total_belanja + $ongkir).'</button>
            </form>
        </div>
        ';
        

        return $dt;
    }

    public function updateCart(Request $req)
    {
        $arrCart = json_decode(Session::get('cart_food'), true);
        $arrCart['keranjang'][$req->index]['qty'] = $req->qty;
        Session::put('cart_food', json_encode($arrCart));
        return true;
    }


    public function hapusCart(Request $req)
    {
        Session::forget('cart_food');

        return redirect('cart_food');
    }
    

    public function putOrder(Request $req)
    {
        // return json_encode($req->all());
        
        $arrCart = json_decode(Session::get('cart_food'), true);

        $titik_jemput = DB::table('rb_reseller')->where('id_reseller', $arrCart['id_reseller'])->first();

        $titikAmbil = $titik_jemput->kordinat;
        
        $titikAntar = $req->titik_antar;
        $alamatAntar = $req->alamat_antar;

        $metodePembayaran = $req->pembayaran_via;

        

        

        $cekUser = DB::table('rb_konsumen')->where('remember_token', Session::get('token'))->first();
        $response = Http::withHeaders([ 
            'Accept'=> '*/*', 
        ]) 
        ->get('https://apisatutoko.kitabuatin.com/api/kurir/v1/hitungJarak/'.trim($titik_jemput->kordinat,' ').'/'.trim($req->titik_antar, ' ').'/REGULAR');
        if ($response->body() > 0) {
            $ongkir = (int)$response->body();
        } else {
            $ongkir = 0;
        }

        $dtPenjualanHeader['kode_transaksi'] = 'FOOD-'.time();
        $dtPenjualanHeader['id_pembeli'] = $cekUser->id_konsumen;
        $dtPenjualanHeader['id_penjual'] = $arrCart['id_reseller'];
        $dtPenjualanHeader['status_pembeli'] = 'konsumen';
        $dtPenjualanHeader['status_penjual'] = 'reseller';
        $dtPenjualanHeader['kurir'] = 'ongkir_lokal';
        $dtPenjualanHeader['service'] = '-';
        $dtPenjualanHeader['ongkir'] = $ongkir;
        $dtPenjualanHeader['waktu_transaksi'] = date('Y-m-d H:i:s');
        $dtPenjualanHeader['proses'] = '4';
        $dtPenjualanHeader['proses'] = '4';
        $idHead = DB::table('rb_penjualan')->insertGetId($dtPenjualanHeader);

        for ($i=0; $i < count($arrCart['keranjang']); $i++) { 
            $dtPenjualanDetail['id_penjualan'] = $idHead;
            $dtPenjualanDetail['id_produk'] = $arrCart['keranjang'][$i]['id_produk'];
            $dtPenjualanDetail['jumlah'] = $arrCart['keranjang'][$i]['qty'];
            $dtPenjualanDetail['diskon'] = 0;
            $dtPenjualanDetail['harga_jual'] = $arrCart['keranjang'][$i]['harga_produk'];
            $dtPenjualanDetail['fee_produk_end'] = 0;
            $dtPenjualanDetail['satuan'] = '-';
            $dtPenjualanDetail['reff'] = 0;
            DB::table('rb_penjualan_detail')->insert($dtPenjualanDetail); 
        }

        $dataTambahan = array(
            'alamat_jemput' => $titik_jemput->alamat_lengkap,
            'alamat_antar' => $alamatAntar,
            'id_penjualan' => $idHead,
        );

        $response = Http::attach('titik_jemput',$titikAmbil)
                ->attach('titik_tujuan',$titikAntar)
                ->attach('service','REGULAR')
                ->attach('source','APPS')
                ->attach('login_token',Session::get('token'))
                ->attach('jenis_layanan','FOOD')
                ->attach('metode_pembayaran',$metodePembayaran)
                ->attach('data_tambahan',json_encode($dataTambahan))
                ->withHeaders([ 
                    'Accept'=> '*/*',
                    'Authorization'=> api_token(),
                ]) 
                ->post(api_url().'api/kurir/v1/orderKurir'); 

        $getOrder = json_decode($response->body());

        Session::forget('cart_food');

        return redirect('detail_food/'.base64_encode($getOrder->id));
    }


}