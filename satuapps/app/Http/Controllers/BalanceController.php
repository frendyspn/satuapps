<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Session;
use DB;
class BalanceController extends Controller
{
    public function index(Request $req)
    {
        if (!cek_login()){
            return redirect('/login');
        }

        if ($this->getDataToken(Session::get('token')) == null) {
            return redirect('/home');
        }
        
        $data['title'] = 'Saldo';
        $data['header'] = 'goback';
        $data['menu'] = '';

        $response = Http::attach('token',Session::get('token'))
                ->withHeaders([ 
                    'Accept'=> '*/*',
                    'Authorization'=> api_token(),
                ]) 
                ->post(api_url().'api/v1/get_saldo'); 

        $getSaldo = json_decode($response->body());


        $data['dtWallet'] = $getSaldo->history;

        $data['saldo'] = $getSaldo->saldo;

        $data['saldoKurir'] = $getSaldo->pendapatan;

        $data['saldoKomisi'] = $getSaldo->komisi;

        $data['kode_unik_deposit'] = $getSaldo->kode_unik_deposit;

        $data['list_rekening'] = $getSaldo->rekening;

        return view('balance',$data);
    }


    public function ProsesDeposit(Request $req)
    {
        $jumlah_deposit = $req->jumlah_deposit;
        $kode_unik_deposit = $req->kode_unik_deposit;

        $dtInsert['id_rekening'] = 0;
        $dtInsert['id_konsumen'] = $this->getDataToken(Session::get('token'))->id_konsumen;
        $dtInsert['nominal'] = $jumlah_deposit+$kode_unik_deposit;
        $dtInsert['status'] = 'Pending';
        $dtInsert['transaksi'] = 'debit';
        $dtInsert['akun'] = 'deposit';
        $dtInsert['waktu_pendapatan'] = date('Y-m-d H:i:s');
        $dtInsert['withdraw_fee'] = 0;

        $id = DB::table('rb_pendapatan_kurir')->insertGetId($dtInsert);

        return redirect('konfirmasi_deposit/'.base64_encode($id));
    }


    public function KonfirmasiDeposit($id)
    {
        $data['title'] = 'Konfirmasi Transfer';
        $data['header'] = 'goback';
        $data['menu'] = '';

        $id = base64_decode($id);

        $response = Http::attach('token',Session::get('token'))
                ->withHeaders([ 
                    'Accept'=> '*/*',
                    'Authorization'=> api_token(),
                ]) 
                ->post(api_url().'api/v1/get_saldo'); 

        $getSaldo = json_decode($response->body());

        $data['deposit'] = DB::table('rb_pendapatan_kurir')->where('id_pendapatan', $id)->first();
        $data['list_rekening'] = $getSaldo->rekening;


        return view('balance.konfirmasi',$data);
    }


    public function UploadBuktiTransfer(Request $req)
    {

        $banner=$_FILES['fileuploadInput']['name']; 
        $file=explode('/',$_FILES['fileuploadInput']['type']);
        $bannerexptype=$file[1];
        $date = time();
        $rand=rand(10000,99999);
        $encname=$date.$rand;
        $bannername=$req->id_deposit.'.'.$bannerexptype;
        $bannerpath=public_path() . '/uploads/bukti_deposit/'.$bannername;
        move_uploaded_file($_FILES["fileuploadInput"]["tmp_name"],$bannerpath);

        $response = Http::attach('token',Session::get('token'))
        ->attach('id',$req->id_deposit)
        ->attach('image',(string)url('').'/public/uploads/bukti_deposit/'.$bannername)
                ->withHeaders([ 
                    'Accept'=> '*/*',
                    'Authorization'=> api_token(),
                ]) 
                ->post(api_url().'api/v1/upload_deposit'); 

        $getSaldo = json_decode($response->body());

        return redirect('balance');
    }


    public function ProsesTransfer(Request $req)
    {
        $no_hp_penerima_transfer = $req->no_hp_penerima_transfer;
        $jumlah_transfer = $req->jumlah_transfer;

        $dtMe = $this->getDataToken(Session::get('token'));

        if ($no_hp_penerima_transfer == '') {
            return redirect('balance')->with('error_msg', 'No HP Penerima Harus Diisi');
        }

        if ($jumlah_transfer == '' && $jumlah_transfer < 10000) {
            return redirect('balance')->with('error_msg', 'Jumlah Transfer Minimal 10.000');
        }
        
        if ($dtMe->no_hp == $no_hp_penerima_transfer) {
            return redirect('balance')->with('error_msg', 'No HP Penerima Harus User Lain');
        }

        $getPenerima = DB::table('rb_konsumen as b')->where('b.no_hp', $no_hp_penerima_transfer)->first();
        if (!$getPenerima) {
            return redirect('balance')->with('error_msg', 'No HP Penerima Tidak Ditemukan');
        }

        $response = Http::attach('token',Session::get('token'))
                ->withHeaders([ 
                    'Accept'=> '*/*',
                    'Authorization'=> api_token(),
                ])
                ->post(api_url().'api/v1/get_saldo'); 

        $getSaldo = json_decode($response->body());

        if ($getSaldo->saldo < $jumlah_transfer) {
            return redirect('balance')->with('error_msg', 'Saldo Tidak Cukup');
        }

        $dtPengirim['id_rekening'] = 0;
        $dtPengirim['id_konsumen'] = $dtMe->id_konsumen;
        $dtPengirim['nominal'] = $jumlah_transfer;
        $dtPengirim['withdraw_fee'] = 0;
        $dtPengirim['status'] = 'Sukses';
        $dtPengirim['transaksi'] = 'kredit';
        $dtPengirim['keterangan'] = 'Transfer Ke '.$getPenerima->nama_lengkap;
        $dtPengirim['akun'] = 'transfer';
        $dtPengirim['waktu_pendapatan'] = date('Y-m-d H:i:s');


        $dtPenerima['id_rekening'] = 0;
        $dtPenerima['id_konsumen'] = $getPenerima->id_konsumen;
        $dtPenerima['nominal'] = $jumlah_transfer;
        $dtPenerima['withdraw_fee'] = 0;
        $dtPenerima['status'] = 'Sukses';
        $dtPenerima['transaksi'] = 'debit';
        $dtPenerima['keterangan'] = 'Transfer Dari '.$dtMe->nama_lengkap;
        $dtPenerima['akun'] = 'transfer';
        $dtPenerima['waktu_pendapatan'] = date('Y-m-d H:i:s');

        $pengirim = DB::table('rb_pendapatan_kurir')->insert($dtPengirim);
        if ($pengirim) {
            DB::table('rb_pendapatan_kurir')->insert($dtPenerima);
            return redirect('balance')->with('error_msg', 'Berhasil Melakukan Transfer');
        } else {
            return redirect('balance')->with('error_msg', 'Terjadi Kesalahan Silahkan Coba Lagi');
        }
    }


    public function ProsesWithdraw(Request $req)
    {
        $bank_withdraw = $req->bank_withdraw;
        $nomor_rekening_withdraw = $req->nomor_rekening_withdraw;
        $nama_rekening_withdraw = $req->nama_rekening_withdraw;
        $jumlah_withdraw = $req->jumlah_withdraw;

        $dtMe = $this->getDataToken(Session::get('token'));

        if ($jumlah_withdraw == '' && $jumlah_withdraw < 10000) {
            return redirect('balance')->with('error_msg', 'Jumlah Withdraw Minimal 10.000');
        }

        $response = Http::attach('token',Session::get('token'))
                ->withHeaders([ 
                    'Accept'=> '*/*',
                    'Authorization'=> api_token(),
                ])
                ->post(api_url().'api/v1/get_saldo'); 

        $getSaldo = json_decode($response->body());

        if ($getSaldo->saldo < $jumlah_withdraw) {
            return redirect('balance')->with('error_msg', 'Saldo Tidak Cukup');
        }

        $dtPenerima['id_rekening'] = 0;
        $dtPenerima['id_konsumen'] = $dtMe->id_konsumen;
        $dtPenerima['nominal'] = $jumlah_withdraw;
        $dtPenerima['withdraw_fee'] = 0;
        $dtPenerima['status'] = 'Proses';
        $dtPenerima['transaksi'] = 'kredit';
        $dtPenerima['keterangan'] = 'Withdraw Ke '.$bank_withdraw;
        $dtPenerima['akun'] = 'withdraw';
        $dtPenerima['waktu_pendapatan'] = date('Y-m-d H:i:s');
        $dtPenerima['bank_withdraw'] = $bank_withdraw;
        $dtPenerima['nomor_rekening_withdraw'] = $nomor_rekening_withdraw;
        $dtPenerima['nama_rekening_withdraw'] = $nama_rekening_withdraw;

        $pengirim = DB::table('rb_pendapatan_kurir')->insert($dtPenerima);

        if ($pengirim) {
            return redirect('balance')->with('sukses', 'Berhasil Melakukan Withdraw');
        } else {
            return redirect('balance')->with('error_msg', 'Terjadi Kesalahan Silahkan Coba Lagi');
        }
    }

}