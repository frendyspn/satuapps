<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Session;
use DB;
use App;
class ShopController extends Controller
{
    public function index(Request $req)
    {
        App::setLocale(session()->get('locale'));

        if (!cek_login()){
            return redirect('/login');
        }
        $cekUser = DB::table('rb_konsumen')->where('remember_token', Session::get('token'))->first();
        $cekOrderExist = DB::table('kurir_order')->where('id_pemesan', $cekUser->id_konsumen)->whereNotIn('status', ['FINISH', 'CANCEL'])->first();
        if ($cekOrderExist) {
            return redirect('detail_shop/'.base64_encode($cekOrderExist->id));
        }
        $data['saldo'] = get_saldo(Session::get('token'));

        $data['toko_input'] = DB::table('kurir_toko_manual')->where('id_konsumen', $cekUser->id_konsumen)->get();
        
        $data['title'] = 'Shop';
        $data['header'] = 'default';
        $data['menu'] = '';

        return view('shop.home',$data);
    }



    public function getStoreLongLat(Request $req)
    {
        App::setLocale(session()->get('locale'));
        
        $longlat = explode(',', $req->longlat) ;

        $max_distance = DB::table('rb_config')->where('field', 'max_jarak_km')->first();

        $results = DB::table('rb_kategori_produk as a')
        ->select(
            'b.id_reseller', 'c.nama_reseller', 'c.kordinat', 'c.foto',
            DB::raw('6371 * 2 * ASIN(SQRT(POWER(SIN(('.$longlat[0].' - CAST(SUBSTRING_INDEX(c.kordinat, \',\', 1) AS DECIMAL(9, 6))) * PI() / 180 / 2), 2) + COS('.$longlat[0].' * PI() / 180) * COS(CAST(SUBSTRING_INDEX(c.kordinat, \',\', -1) AS DECIMAL(9, 6)) * PI() / 180) * POWER(SIN(('.$longlat[1].' - CAST(SUBSTRING_INDEX(c.kordinat, \',\', -1) AS DECIMAL(9, 6))) * PI() / 180 / 2), 2))) AS distance'))
        ->leftJoin('rb_produk as b', 'b.id_kategori_produk', '=', 'a.id_kategori_produk')
        ->leftJoin('rb_reseller as c', 'c.id_reseller', '=', 'b.id_reseller')
        ->where('a.is_shop', '1');

        if ($max_distance->value > 0) {
            $results = $results->where(DB::raw('6371 * 2 * ASIN(SQRT(POWER(SIN(('.$longlat[0].' - CAST(SUBSTRING_INDEX(c.kordinat, \',\', 1) AS DECIMAL(9, 6))) * PI() / 180 / 2), 2) + COS('.$longlat[0].' * PI() / 180) * COS(CAST(SUBSTRING_INDEX(c.kordinat, \',\', -1) AS DECIMAL(9, 6)) * PI() / 180) * POWER(SIN(('.$longlat[1].' - CAST(SUBSTRING_INDEX(c.kordinat, \',\', -1) AS DECIMAL(9, 6))) * PI() / 180 / 2), 2)))'), '<=', $max_distance->value);
        }

        $results = $results->whereNotNull('c.kordinat')
        ->groupBy('c.nama_reseller', 'c.kordinat','b.id_reseller', 'c.foto')
        ->limit(20)
        ->get();


        $dt = '';

        if (count($results) > 0) {
            foreach ($results as $row) {
                $dt .= '
                <div class="col-6 mb-2">
                    <div class="bill-box">
                        <div class="img-wrapper">';

                        if ($row->foto == '') {
                            $dt .= '<img src="https://satutoko.id/asset/foto_statis/no-image.jpg" alt="img" class="image-block imaged w48" width="48px" height="48px">';
                        } else {
                            $dt .= '<img src="https://satutoko.id/asset/foto_user/'.$row->foto.'" alt="img" class="image-block imaged w48" width="48px" height="48px">';
                        }
                        
                $dt .= '</div>
                        <div class="price" style="font-size:1rem">'.$row->nama_reseller.'</div>
                        <p><ion-icon name="location-outline"></ion-icon>'.number_format($row->distance,2).' KM</p>
                        <a href="'.url('detail_store/store/'.base64_encode($row->id_reseller)).'" class="btn btn-primary btn-block btn-sm">'.__('bahasa.btn_beli_disini').'</a>
                    </div>
                </div>
                ';
            }
        } else {
            $dt .= '
            <div class="col-12 mb-2">
                <div class="bill-box">
                    '.__('bahasa.notif_belum_ada_toko_disekitar').'
                </div>
            </div>
            ';
        }
        
        

        echo $dt;
    }


    public function inputStore(Request $req)
    {
        App::setLocale(session()->get('locale'));

        $data['title'] = 'Input Store';
        $data['header'] = 'default';
        $data['menu'] = '';

        return view('shop.input_store',$data);
    }


    public function addStore(Request $req)
    {
        $nama_toko = $req->nama_toko;
        $alamat_toko = $req->alamat_toko;
        $patokan_toko = $req->patokan_toko;
        $titik_toko = $req->titik_toko;

        $cekUser = DB::table('rb_konsumen')->where('remember_token', Session::get('token'))->first();

        $dtInsert['nama_toko'] = $nama_toko;
        $dtInsert['patokan_toko'] = $patokan_toko;
        $dtInsert['alamat_toko'] = $alamat_toko;
        $dtInsert['kordinat_toko'] = $titik_toko;
        $dtInsert['id_konsumen'] = $cekUser->id_konsumen;
        $dtInsert['created_at'] = date('Y-m-d H:i:s');

        $idStore = DB::table('kurir_toko_manual')->insertGetId($dtInsert);

        return redirect('detail_store/addon/'.base64_encode($idStore));
    }


    public function getProdukStore($type, $id)
    {
        App::setLocale(session()->get('locale'));

        $id = base64_decode($id);

        // cek if toko input manual
        if ($type == 'addon') {
            $data['produk'] = [];
            $store = DB::table('kurir_toko_manual')->select('*', 'id as id_reseller', 'nama_toko as nama_reseller')->where('id',$id)->first();
        } else {
            $data['produk'] = DB::table('rb_kategori_produk as a')
            ->select(
                'b.id_reseller', 'b.id_produk', 'b.nama_produk', 'b.satuan', 'b.harga_reseller', 'b.harga_konsumen', 'b.harga_premium', 'b.gambar', 'a.nama_kategori', 'a.id_kategori_produk'
            )
            ->leftJoin('rb_produk as b', 'b.id_kategori_produk', '=', 'a.id_kategori_produk')
            ->where('a.is_shop', '1')
            ->where('b.id_reseller', $id)
            ->orderBy('a.id_kategori_produk')
            ->orderBy('b.nama_produk')
            ->get();
            $store = DB::table('rb_reseller')->where('id_reseller',$id)->first();
        }

        $data['store'] = $store;
        $data['title'] = 'Shop';
        $data['header'] = $store->nama_reseller;
        $data['menu'] = '';
        return view('shop.store_detail',$data);
        
    }



    public function addCartShop(Request $req)
    {
        if (!Session::has('cart_shop')) {
            $arrCart['id_reseller'] = '';
            $arrCart['type_reseller'] = '';
            $arrCart['keranjang'] = array();
            Session::put('cart_shop', json_encode($arrCart));
        }

        $cart_exist = json_decode(Session::get('cart_shop'), true);

        // return json_encode($req->all());

        $id_reseller    = $req->id_reseller;
        $id_produk      = $req->id_produk;
        $nama_produk    = $req->nama_produk;
        $harga_produk   = (int)$req->harga_produk;
        $satuan         = $req->satuan;
        $qty            = (int) $req->qty;
        $type_reseller  = $req->type_reseller;

        if ($qty == 0) {
            http_response_code(400);
            exit(json_encode('Qty Kosong'));
        }


        if ($cart_exist['type_reseller'] != $type_reseller) {
            Session::forget('cart_shop');
            $arrCart['id_reseller'] = '';
            $arrCart['type_reseller'] = '';
            $arrCart['keranjang'] = array();
            Session::put('cart_shop', json_encode($arrCart));
            $cart_exist = json_decode(Session::get('cart_shop'), true);
        }
        

        if ($cart_exist['id_reseller'] == '') {
            $cart_exist['id_reseller'] = $id_reseller;
            $cart_exist['type_reseller'] = $type_reseller;
        }

        if ($type_reseller == 'addon') {
            $dtArr['id_produk'] = $id_produk;
            $dtArr['nama_produk'] = $nama_produk;
            $dtArr['harga_produk'] = $harga_produk;
            $dtArr['satuan'] = $satuan;
            $dtArr['qty'] = $qty;
            array_push($cart_exist['keranjang'], $dtArr);
        } else {
            $key = array_search($id_produk, array_column($cart_exist['keranjang'], 'id_produk'));
            // return $id_produk.' - '.$key;
            if ($key == '') {
                $dtArr['id_produk'] = $id_produk;
                $dtArr['nama_produk'] = $nama_produk;
                $dtArr['harga_produk'] = $harga_produk;
                $dtArr['satuan'] = $satuan;
                $dtArr['qty'] = $qty;
                array_push($cart_exist['keranjang'], $dtArr);
            } else {
                $cart_exist['keranjang'][$key]['qty'] = $cart_exist['keranjang'][$key]['qty'] + $qty;
            }
        }
        
        Session::put('cart_shop', json_encode($cart_exist));
        http_response_code(200);
        return Session::get('cart_shop');
    }


    public function viewCartShop(Request $req)
    {
        if (!Session::has('cart_shop')) {
            $arrCart['id_reseller'] = '';
            $arrCart['type_reseller'] = '';
            $arrCart['keranjang'] = array();
            Session::put('cart_shop', json_encode($arrCart));
        }

        return Session::get('cart_shop');
    }



    public function indexCart(Request $req)
    {
        App::setLocale(session()->get('locale'));

        $data['title'] = 'Cart Shop';
        $data['header'] = 'goback';
        $data['menu'] = '';
        

        return view('shop.cart',$data);
    }


    public function getCart(Request $req)
    {
        App::setLocale(session()->get('locale'));

        if (!Session::has('cart_shop')) {
            return '<div class="col-12">Keranjang Kosong</div>';
            // $arrCart['id_reseller'] = '';
            // $arrCart['keranjang'] = array();
        }

        $arrCart = json_decode(Session::get('cart_shop'), true);

        // return json_encode($arrCart['keranjang']);

        $dt = '';

        if ($arrCart['type_reseller'] == 'addon') {
            $dtReseller = DB::table('kurir_toko_manual')->where('id', $arrCart['id_reseller'])->first();
        } else {
            $dtReseller = DB::table('rb_reseller')->select('*', 'alamat_lengkap as alamat_toko', 'nama_reseller as nama_toko')->where('id_reseller', $arrCart['id_reseller'])->first();
        }

        $dt .= '
        <div class="transactions mb-1">
            <a href="#" class="item">
                <div class="detail">
                    <div>
                        <strong>Toko: '.$dtReseller->nama_toko.'</br><span>'.$dtReseller->alamat_toko.'</span></strong>
                    </div>
                </div>
            </a>
            <a class="btn btn-secondary btn-sm" href="'.url('detail_store/'.$arrCart['type_reseller'].'/'.base64_encode($arrCart['id_reseller'])).'">'.__('bahasa.btn_tambah_barang').'</a>
        </div>
        ';
        

        $total_belanja = 0;
        $ongkir = 0;

        for ($i=0; $i < count($arrCart['keranjang']); $i++) { 
            $dt .='
                <a href="#" class="item">
                    <div class="detail">
                        <div>
                            <strong>'.$arrCart['keranjang'][$i]['nama_produk'].'</strong>
                            <p>'.__('bahasa.kurs').number_format($arrCart['keranjang'][$i]['harga_produk']).'</p>
                        </div>
                    </div>
                    <div class="right">
                        <div class="price" style="text-align:right">'.__('bahasa.kurs').number_format($arrCart['keranjang'][$i]['harga_produk'] * $arrCart['keranjang'][$i]['qty']).'</div>
                        <div class="row">
                            <div class="col border">
                                <span onclick="update_cart('.$i.', '.($arrCart['keranjang'][$i]['qty']-1).')"><ion-icon name="remove-outline"></ion-icon></span>
                            </div>
                            <div class="col">
                                <input onchange="update_cart('.$i.', $(this).val())" type="text" style="width:50px;text-align:center" value="'.$arrCart['keranjang'][$i]['qty'].'">
                            </div>
                            <div class="col border">
                                <span onclick="update_cart('.$i.', '.($arrCart['keranjang'][$i]['qty']+1).')"><ion-icon name="add-outline"></ion-icon></span>
                            </div>
                        </div>
                    </div>
                </a>
                ';
            $total_belanja = $total_belanja + ($arrCart['keranjang'][$i]['harga_produk'] * $arrCart['keranjang'][$i]['qty']);
        }
        
        $longlat = explode(',', $req->titik_antar) ;
        if ($arrCart['type_reseller'] == 'addon') {
            $titik_jemput = DB::table('kurir_toko_manual')->select('*','alamat_toko as alamat_lengkap', 'kordinat_toko as kordinat', 'nama_toko as nama_reseller', DB::raw('6371 * 2 * ASIN(SQRT(POWER(SIN(('.$longlat[0].' - CAST(SUBSTRING_INDEX(kordinat_toko, \',\', 1) AS DECIMAL(9, 6))) * PI() / 180 / 2), 2) + COS('.$longlat[0].' * PI() / 180) * COS(CAST(SUBSTRING_INDEX(kordinat_toko, \',\', -1) AS DECIMAL(9, 6)) * PI() / 180) * POWER(SIN(('.$longlat[1].' - CAST(SUBSTRING_INDEX(kordinat_toko, \',\', -1) AS DECIMAL(9, 6))) * PI() / 180 / 2), 2))) AS distance'))->where('id', $arrCart['id_reseller'])->first();
        } else {
            $titik_jemput = DB::table('rb_reseller')->select('*',DB::raw('6371 * 2 * ASIN(SQRT(POWER(SIN(('.$longlat[0].' - CAST(SUBSTRING_INDEX(kordinat, \',\', 1) AS DECIMAL(9, 6))) * PI() / 180 / 2), 2) + COS('.$longlat[0].' * PI() / 180) * COS(CAST(SUBSTRING_INDEX(kordinat, \',\', -1) AS DECIMAL(9, 6)) * PI() / 180) * POWER(SIN(('.$longlat[1].' - CAST(SUBSTRING_INDEX(kordinat, \',\', -1) AS DECIMAL(9, 6))) * PI() / 180 / 2), 2))) AS distance'))->where('id_reseller', $arrCart['id_reseller'])->first();
        }
        // $titik_jemput = DB::table('rb_reseller')->select('*', DB::raw('6371 * 2 * ASIN(SQRT(POWER(SIN(('.$longlat[0].' - CAST(SUBSTRING_INDEX(kordinat, \',\', 1) AS DECIMAL(9, 6))) * PI() / 180 / 2), 2) + COS('.$longlat[0].' * PI() / 180) * COS(CAST(SUBSTRING_INDEX(kordinat, \',\', -1) AS DECIMAL(9, 6)) * PI() / 180) * POWER(SIN(('.$longlat[1].' - CAST(SUBSTRING_INDEX(kordinat, \',\', -1) AS DECIMAL(9, 6))) * PI() / 180 / 2), 2))) AS distance'))->where('id_reseller', $arrCart['id_reseller'])->first();

        
        $response = Http::withHeaders([ 
            'Accept'=> '*/*', 
        ]) 
        ->get('https://apisatutoko.kitabuatin.com/api/kurir/v1/hitungJarak/'.trim($titik_jemput->kordinat,' ').'/'.trim($req->titik_antar, ' ').'/REGULAR');
        
        if ($response->body() > 0) {
            $ongkir = (int)$response->body();
        }

        $dt .='
        <ul class="listview flush transparent simple-listview no-space mt-3 bg-white">
                <li>
                    <strong>'.__('bahasa.sub_total').'</strong>
                    <span>'.number_format($total_belanja).'</span>
                </li>
                <li>
                    <strong>'.__('bahasa.biaya_kirim').' - '.number_format($titik_jemput->distance).'KM</strong>
                    <span>'.number_format($ongkir).'</span>
                </li>
                <li>
                    <strong>'.__('bahasa.total').'</strong>
                    <h3>'.number_format($total_belanja + $ongkir).'</h3>
                </li>
            </ul>
        ';

        $saldo = get_saldo(Session::get('token'));

        $dt .= '
        <form method="post" action="'.route('put_order_shop').'">
        '.csrf_field().'
        <div class="btn-group mt-2 col-12" role="group">
            <input type="radio" class="btn-check" name="pembayaran_via" value="CASH" id="pembayaran_cash"'; 
            
            if($saldo <= 0) {$dt .='checked=""';}
            
            $dt .= '>
            <label class="btn btn-outline-primary" for="btnradio1">Cash</label>

            <input type="radio" class="btn-check" name="pembayaran_via" value="WALLET" id="pembayaran_sallet"'; 
            
            if($saldo > 0) {$dt .='checked=""';}
            if($saldo <= 0) {$dt .='disabled="disabled"';}
            
            $dt .='>
            <label class="btn btn-outline-primary" for="btnradio2">Wallet '.__('bahasa.kurs').number_format($saldo).'</label>
        </div>


        <div class="accordion-item">
            
            <input type="hidden" id="titik_antar" name="titik_antar" value="'.$req->titik_antar.'">
            <input type="hidden" id="alamat_antar" name="alamat_antar" value="'.$req->alamat_antar.'">
            <button class="btn btn-primary btn-block mt-2" id="btn-order">Order '.__('bahasa.kurs').number_format($total_belanja + $ongkir).'</button>
            </form>
        </div>
        ';
        

        return $dt;
    }


    public function updateCart(Request $req)
    {
        $arrCart = json_decode(Session::get('cart_shop'), true);
        $arrCart['keranjang'][$req->index]['qty'] = $req->qty;
        Session::put('cart_shop', json_encode($arrCart));
        return true;
    }



    public function putOrder(Request $req)
    {
        // return json_encode($req->all());
        
        $arrCart = json_decode(Session::get('cart_shop'), true);

        if ($arrCart['type_reseller'] == 'addon') {
            $titik_jemput = DB::table('kurir_toko_manual')->select('*','alamat_toko as alamat_lengkap', 'kordinat_toko as kordinat', 'nama_toko as nama_reseller')->where('id', $arrCart['id_reseller'])->first();
            $id_reseller = '';
        } else {
            $titik_jemput = DB::table('rb_reseller')->where('id_reseller', $arrCart['id_reseller'])->first();
            $id_reseller = $arrCart['id_reseller'];
        }

        $titikAmbil = $titik_jemput->kordinat;
        
        $titikAntar = $req->titik_antar;
        $alamatAntar = $req->alamat_antar;

        $metodePembayaran = $req->pembayaran_via;

        

        

        $cekUser = DB::table('rb_konsumen')->where('remember_token', Session::get('token'))->first();
        $response = Http::withHeaders([ 
            'Accept'=> '*/*', 
        ]) 
        ->get('https://apisatutoko.kitabuatin.com/api/kurir/v1/hitungJarak/'.trim($titik_jemput->kordinat,' ').'/'.trim($req->titik_antar, ' ').'/REGULAR');
        if ($response->body() > 0) {
            $ongkir = (int)$response->body();
        } else {
            $ongkir = 0;
        }


        $dataTambahan = array(
            'alamat_jemput' => $titik_jemput->alamat_lengkap,
            'alamat_antar' => $alamatAntar,
            'pemberi_barang' => $titik_jemput->nama_reseller,
            'id_reseller' => $id_reseller,
        );

        $response = Http::attach('titik_jemput',$titikAmbil)
                ->attach('titik_tujuan',$titikAntar)
                ->attach('service','REGULAR')
                ->attach('source','APPS')
                ->attach('login_token',Session::get('token'))
                ->attach('jenis_layanan','SHOP')
                ->attach('metode_pembayaran',$metodePembayaran)
                ->attach('data_tambahan',json_encode($dataTambahan))
                ->withHeaders([ 
                    'Accept'=> '*/*',
                    'Authorization'=> api_token(),
                ]) 
                ->post(api_url().'api/kurir/v1/orderKurir'); 

        $getOrder = json_decode($response->body());

        for ($i=0; $i < count($arrCart['keranjang']); $i++) { 
            $dtPenjualanDetail['id_kurir_order'] = $getOrder->id;
            $dtPenjualanDetail['nama_produk'] = $arrCart['keranjang'][$i]['nama_produk'];
            $dtPenjualanDetail['jumlah'] = $arrCart['keranjang'][$i]['qty'];
            $dtPenjualanDetail['diskon'] = 0;
            $dtPenjualanDetail['harga_jual'] = $arrCart['keranjang'][$i]['harga_produk'];
            $dtPenjualanDetail['fee_produk_end'] = 0;
            $dtPenjualanDetail['satuan'] = $arrCart['keranjang'][$i]['satuan'];
            $dtPenjualanDetail['reff'] = 0;
            DB::table('rb_penjualan_shop')->insert($dtPenjualanDetail); 
        }

        Session::forget('cart_shop');

        return redirect('detail_shop/'.base64_encode($getOrder->id));
    }


    public function detailOrder($id)
    {
        App::setLocale(session()->get('locale'));
        
        if (!cek_login()){
            return redirect('/login');
        }

        $id = base64_decode($id);

        $data['order'] = DB::table('kurir_order as a')->select('a.id', 'a.titik_jemput', 'a.titik_antar', 'a.service', 'a.id_penjualan', 'a.status', 'a.source', 'a.tarif', 'a.id_sopir', 'a.kode_order', 'c.nama_reseller as nama_toko', 'c.alamat_lengkap as alamat_toko', 'd.nama_lengkap as nama_konsumen', 'd.alamat_lengkap as alamat_konsumen', 'a.pemberi_barang', 'a.alamat_jemput', 'a.alamat_antar', 'a.penerima_barang', DB::raw('(select sum(jumlah*(harga_jual-diskon)) from rb_penjualan_detail where id_penjualan = a.id_penjualan) as total_belanja'))->leftJoin('rb_penjualan as b', 'b.id_penjualan', 'a.id_penjualan')->leftJoin('rb_reseller as c', 'c.id_reseller', 'b.id_penjual')->leftJoin('rb_konsumen as d', 'd.id_konsumen', 'b.id_pembeli')->where('a.id', $id)->first();
        $data['belanja'] = DB::table('rb_penjualan_shop')->where('id_kurir_order', $id)->get();
        $data['kurir'] = DB::table('rb_sopir as a')->select('a.*', 'b.nama_lengkap')->leftJoin('rb_konsumen as b', 'b.id_konsumen', 'a.id_konsumen')->where('a.id_sopir', $data['order']->id_sopir)->first();
        $data['timeline'] = DB::table('kurir_order_log')->where('id_order', $id)->get();
        $data['warna_timeline'] = ['bg-primary','bg-info','bg-danger','bg-warning'];
        
        $data['title'] = 'Detail Order';
        $data['header'] = 'default';
        $data['menu'] = '';

        return view('shop.detail',$data);
    }


    public function hapusCart(Request $req)
    {
        Session::forget('cart_shop');

        return redirect('cart_shop');
    }
}