<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use Illuminate\Http\Request;

use DB;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    public function api_token(Request $req)
    {
        return api_token();
    }

    public function GetProvinsi(Request $req)
    {
        $dtProvinsi = DB::table('tb_ro_provinces')->orderBy('province_name')->get();
        return $dtProvinsi;
    }

    public function GetKota(Request $req)
    {
        $dtKota = DB::table('tb_ro_cities')->where('province_id', $req->id_provinsi)->orderBy('city_name')->get();
        return $dtKota;
    }

    public function GetKecamatan(Request $req)
    {
        $dtKota = DB::table('tb_ro_subdistricts')->where('city_id', $req->id_kota)->orderBy('subdistrict_name')->get();
        return $dtKota;
    }

    public function getDataToken($token)
    {
        $getKurir = DB::table('rb_konsumen as b')->where('b.remember_token', $token)->first();
        return $getKurir;
    }

    public function getDataHp(Request $req)
    {
        $no_hp = $req->no_hp;
        $getKurir = DB::table('rb_konsumen as b')->where('b.no_hp', $no_hp)->first();
        if ($getKurir) {
            return $getKurir;
        } else {
            http_response_code(404);
            exit(json_encode(['Message' => 'user Tidak Ditemukan']));
        }
        
    }


    public function getConfig($config)
    {
        $getKurir = DB::table('rb_config')->where('field', $config)->first();
        return $getKurir->value;
    }

    
}
