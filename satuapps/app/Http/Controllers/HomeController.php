<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use DB;
use App;
class HomeController extends Controller
{
    public function index(Request $req)
    {
        App::setLocale(session()->get('locale'));

        if (!cek_login()){
            return redirect('/login');
        }
        
        $data['saldo'] = get_saldo(Session::get('token'));
        $data['title'] = __('bahasa.Beranda');
        $data['header'] = 'default';
        $data['menu'] = '';
        $data['dtBanner'] = DB::table('banner_apps')->where('posisi', 'user')->where('is_aktif', '1')->where('is_deleted', '0')->get();

        return view('home.home',$data);
    }

    public function getSaldo(Request $req)
    {
        App::setLocale(session()->get('locale'));

        $userToken = Session::get('token');

        $getKurir = DB::table('rb_sopir as a')->select('a.*')->leftJoin('rb_konsumen as b', 'b.id_konsumen', 'a.id_konsumen')->where('b.remember_token', $userToken)->first();

        if (!$getKurir) {
            return number_format(0);
        }

        $saldo_debit = DB::table('rb_pendapatan_kurir')->select(DB::raw("sum(nominal) as total"))->where('id_konsumen', $getKurir->id_konsumen)->where('transaksi', 'debit')->first();

        $saldo_kredit = DB::table('rb_pendapatan_kurir')->select(DB::raw("sum(nominal) as total"))->where('id_konsumen', $getKurir->id_konsumen)->where('transaksi', 'kredit')->first();

        return __('bahasa.kurs').number_format($saldo_debit->total - $saldo_kredit->total);
    }

    public function getOrder(Request $req)
    {
        // echo '<p>'.date('H:i:s').'</p>';

        App::setLocale(session()->get('locale'));

        $position = $req->position;
        
        $userToken = Session::get('token');

        $getKurir = DB::table('rb_sopir as a')->select('a.*')->leftJoin('rb_konsumen as b', 'b.id_konsumen', 'a.id_konsumen')->where('b.remember_token', $userToken)->first();

        if (!$getKurir) {
            echo '<h2 class="text-danger">'.__('bahasa.notif_akun_suspend').'</h2>';
            return;
        }

        if ($getKurir->aktif == 'N') {
            echo '<h2 class="text-danger">'.__('bahasa.notif_akun_belum_aktif').'</h2>';
            return;
        }

        $getOrderExisting = DB::table('kurir_order')->where('id_sopir', $getKurir->id_sopir)->whereNotIn('status', ['cancel','finish'])->first();

        if($getOrderExisting){
            $getOrder = DB::table('kurir_order as a')->select('a.id', 'a.titik_jemput', 'a.titik_antar', 'a.service', 'a.id_penjualan', 'a.status', 'a.source', 'a.tarif', 'a.id_sopir', 'a.kode_order', 'c.nama_reseller as nama_toko', 'c.alamat_lengkap as alamat_toko', 'd.nama_lengkap as nama_konsumen', 'd.alamat_lengkap as alamat_konsumen')->leftJoin('rb_penjualan as b', 'b.id_penjualan', 'a.id_penjualan')->leftJoin('rb_reseller as c', 'c.id_reseller', 'b.id_penjual')->leftJoin('rb_konsumen as d', 'd.id_konsumen', 'b.id_pembeli')->where('a.id', $getOrderExisting->id)->first();
            $btnAmbilBarang = '';
            $btnSerahBarang = '';

            if ($getOrder->status == 'PROCESS') {
                $btnAmbilBarang = '<button class="btn btn-sm btn-secondary" data-bs-toggle="modal" data-bs-target="#ModalAmbilBarang" onclick="start_take_capture('.$getOrder->id.')">'.__('bahasa.ambil_barang').'</button>';
            }

            if ($getOrder->status == 'ONTHEWAY') {
                $btnSerahBarang = '<button class="btn btn-sm btn-secondary" data-bs-toggle="modal" data-bs-target="#ModalSerahBarang" onclick="start_take_capture_serah('.$getOrder->id.')">'.__('bahasa.serahkan_barang').'</button>';
            }

            echo '
            <ul class="listview image-listview mb-2">
                <li>
                    <div class="item">
                        <div class="icon-box bg-primary">
                            <a class="text-white" href="https://www.google.com/maps/dir/?api=1&origin='.$position.'&destination='.$getOrder->titik_jemput.'"><ion-icon name="location-outline"></ion-icon></a>
                        </div>
                        <div class="in">
                            <div>
                                <header>'.__('bahasa.pickup').'</header>
                                '.$getOrder->nama_toko.'
                                <footer>'.$getOrder->alamat_toko.'</footer>
                            </div>
                            '.$btnAmbilBarang.'
                        </div>
                    </div>
                </li>
                <li>
                    <div class="item">
                        <div class="icon-box bg-info">
                        <a class="text-white" href="https://www.google.com/maps/dir/?api=1&origin='.$position.'&destination='.$getOrder->titik_antar.'"><ion-icon name="location-outline"></ion-icon></a>
                        </div>
                        <div class="in">
                            <div>
                                <header>'.__('bahasa.tujuan').'</header>
                                '.$getOrder->nama_konsumen.'
                                <footer>'.$getOrder->alamat_konsumen.'</footer>
                            </div>
                            '.$btnSerahBarang.'
                        </div>
                    </div>
                </li>
                <li>
                    <div class="item">
                        <div class="icon-box bg-primary">
                            <ion-icon name="cash-outline"></ion-icon>
                        </div>
                        <div class="in">
                            <h3>
                                '.__('bahasa.kurs').number_format($getOrder->tarif).'
                            </h3>
                        </div>
                    </div>
                </li>
            </ul>
            ';

            return;
        }

        $getOrder = DB::table('kurir_order as a')->select('a.id', 'a.titik_jemput', 'a.titik_antar', 'a.service', 'a.id_penjualan', 'a.status', 'a.source', 'a.tarif', 'a.id_sopir', 'a.kode_order', 'c.nama_reseller as nama_toko', 'c.alamat_lengkap as alamat_toko', 'd.nama_lengkap as nama_konsumen', 'd.alamat_lengkap as alamat_konsumen')->leftJoin('rb_penjualan as b', 'b.id_penjualan', 'a.id_penjualan')->leftJoin('rb_reseller as c', 'c.id_reseller', 'b.id_penjual')->leftJoin('rb_konsumen as d', 'd.id_konsumen', 'b.id_pembeli')->where('status', 'new')->get();

        if (count($getOrder) == 0) {
            echo '<ul class="listview image-listview mb-2"><li><div class="item text-danger">'.__('bahasa.notif_belum_ada_order').'</div></li></ul>';
        }

        foreach ($getOrder as $order) {
            echo '
            <div class="ada_order"></div>
            <ul class="listview image-listview mb-2">
                <li>
                    <div class="item">
                        <div class="icon-box bg-primary">
                            <ion-icon name="location-outline"></ion-icon>
                        </div>
                        <div class="in">
                            <div>
                                <header>'.__('bahasa.pickup').'</header>
                                '.$order->nama_toko.'
                                <footer>'.$order->alamat_toko.'</footer>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="item">
                        <div class="icon-box bg-info">
                            <ion-icon name="location-outline"></ion-icon>
                        </div>
                        <div class="in">
                            <div>
                                <header>'.__('bahasa.tujuan').'</header>
                                '.$order->nama_konsumen.'
                                <footer>'.$order->alamat_konsumen.'</footer>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="item">
                        <div class="in">
                            <h3>
                                '.__('bahasa.kurs').number_format($order->tarif).'
                            </h3>
                            <a class="btn btn-sm btn-primary" onclick="ambilOrder('.$order->id.')">'.__('bahasa.ambil_order').'</a>
                        </div>
                    </div>
                </li>
            </ul>
            ';
        }

        // echo '<div class="text-wrap" style="width: 6rem;">'.json_encode($getOrder).'</div>';
    }


    public function ambilOrder(Request $req)
    {
        App::setLocale(session()->get('locale'));

        $id_order = $req->idOrder;

        $cekOrder = DB::table('kurir_order')->where('id', $id_order)->first();

        if (!$cekOrder) {
            http_response_code(400);
            exit(json_encode([__('bahasa.ambil_order')]));
        }

        $userToken = Session::get('token');

        $getKurir = DB::table('rb_sopir as a')->select('a.id_sopir')->leftJoin('rb_konsumen as b', 'b.id_konsumen', 'a.id_konsumen')->where('b.remember_token', $userToken)->first();

        $cekKurirKirim = DB::table('kurir_order')->where('id_sopir', $getKurir->id_sopir)->where('status','PROCESS')->first();

        if ($cekKurirKirim) {
            http_response_code(400);
            exit(json_encode([__('bahasa.ambil_order')]));
        }

        $ambil = DB::table('kurir_order')->where('id', $id_order)->update(['id_sopir' => $getKurir->id_sopir, 'status'=>'PROCESS']);

        if ($ambil) {
            http_response_code(200);
            exit(json_encode([__('bahasa.ambil_order')]));
        } else {
            http_response_code(400);
            exit(json_encode([__('bahasa.notif_order_gagal_diambil')]));
        }
        
    }

    public function ambilBarang(Request $req)
    {
        $id_order = $req->id_order;
        $foto = $req->foto;

        $data['foto_ambil_barang'] = $foto;
        $data['waktu_ambil_barang'] = date('Y-m-d H:i:s');
        $data['status'] = 'ONTHEWAY';

        return DB::table('kurir_order')->where('id', $id_order)->update($data);
    }

    public function serahBarang(Request $req)
    {
        $id_order = $req->id_order;
        $foto = $req->foto;
        $penerima = $req->penerima;

        $cekKurirKirim = DB::table('kurir_order')->where('id', $id_order)->first();

        $dtSopir = DB::table('rb_sopir')->where('id_sopir', $cekKurirKirim->id_sopir)->first();

        $dtPendapatan['id_konsumen'] = $dtSopir->id_konsumen;
        $dtPendapatan['nominal'] = $cekKurirKirim->tarif;
        $dtPendapatan['id_rekening'] = 0;
        $dtPendapatan['withdraw_fee'] = 0;
        $dtPendapatan['status'] = 'Sukses';
        $dtPendapatan['transaksi'] = 'debit';
        $dtPendapatan['keterangan'] = '';
        $dtPendapatan['akun'] = 'sopir';
        $dtPendapatan['waktu_pendapatan'] = date('Y-m-d H:i:s');
        DB::table('rb_pendapatan_kurir')->insert($dtPendapatan);

        $potonganKomisi = $cekKurirKirim->tarif * ($this->getConfig('fee_kurir_total')/100);
        $dtPotongan['id_konsumen'] = $dtSopir->id_konsumen;
        $dtPotongan['nominal'] = $potonganKomisi;
        $dtPotongan['id_rekening'] = 0;
        $dtPotongan['withdraw_fee'] = 0;
        $dtPotongan['status'] = 'Sukses';
        $dtPotongan['transaksi'] = 'kredit';
        $dtPotongan['keterangan'] = 'Potongan admin kurir';
        $dtPotongan['akun'] = 'sopir';
        $dtPotongan['waktu_pendapatan'] = date('Y-m-d H:i:s');
        DB::table('rb_pendapatan_kurir')->insert($dtPotongan);

        $this->pembagianKomisiKurir($potonganKomisi, $cekKurirKirim->id_sopir, $id_order);

        $data['foto_serah_terima_barang'] = $foto;
        $data['waktu_serah_terima_barang'] = date('Y-m-d H:i:s');
        $data['status'] = 'FINISH';
        $data['penerima_barang'] = $penerima;

        return DB::table('kurir_order')->where('id', $id_order)->update($data);
    }


    public function pembagianKomisiKurir($komisi, $id_sopir, $id_order)
    {
        $persenSistem = $this->getConfig('fee_kurir_sistem');
        $persenReff = $this->getConfig('fee_kurir_ref');
        $persenKoordinator = $this->getConfig('fee_kurir_koordinator_kota');
        $persenKoordinatorKecamatan = $this->getConfig('fee_kurir_koordinator_kecamatan');
        $persenKasCabang = $this->getConfig('fee_kurir_kas_cabang');
        $persenLainnya = $this->getConfig('fee_kurir_lainnya');
        $persenAgen = $this->getConfig('fee_kurir_agen');

        $dtKurir = DB::table('rb_sopir')->where('id_sopir', $id_sopir)->first();
        $kecamatanKurir = $dtKurir->kecamatan_id;
        $kotaKurir = $dtKurir->kota_id;

        if ($persenSistem > 0) {
            $komisiSistem = $komisi * ($persenSistem/100);
            $dtSistem['id_konsumen'] = 0;
            $dtSistem['nominal'] = $komisiSistem;
            $dtSistem['id_rekening'] = 0;
            $dtSistem['withdraw_fee'] = 0;
            $dtSistem['status'] = 'Sukses';
            $dtSistem['transaksi'] = 'debit';
            $dtSistem['keterangan'] = 'Komisi admin kurir';
            $dtSistem['akun'] = 'admin';
            $dtSistem['waktu_pendapatan'] = date('Y-m-d H:i:s');
            DB::table('rb_pendapatan_kurir')->insert($dtSistem);
        }

        if ($persenReff > 0) {
            $komisiReff = $komisi * ($persenReff/100);
            $kasihAdmin = false;

            $cekReff = DB::table('rb_konsumen as a')->select('a.referral_id')->leftJoin('rb_sopir as b', 'b.id_konsumen', 'a.id_konsumen')->where('b.id_sopir', $id_sopir)->first();
            if ($cekReff) {
                if ($cekReff->referral_id != '') {
                    $dtReff['id_konsumen'] = $cekReff->referral_id;
                    $dtReff['nominal'] = $komisiReff;
                    $dtReff['id_rekening'] = 0;
                    $dtReff['withdraw_fee'] = 0;
                    $dtReff['status'] = 'Sukses';
                    $dtReff['transaksi'] = 'debit';
                    $dtReff['keterangan'] = 'Komisi Refferal Kurir';
                    $dtReff['akun'] = 'refferal';
                    $dtReff['waktu_pendapatan'] = date('Y-m-d H:i:s');
                    DB::table('rb_pendapatan_kurir')->insert($dtReff);
                } else {
                    $kasihAdmin = true;
                }
            } else {
                $kasihAdmin = true;
            }

            if ($kasihAdmin) {
                $dtReff['id_konsumen'] = 0;
                $dtReff['nominal'] = $komisiReff;
                $dtReff['id_rekening'] = 0;
                $dtReff['withdraw_fee'] = 0;
                $dtReff['status'] = 'Sukses';
                $dtReff['transaksi'] = 'debit';
                $dtReff['keterangan'] = 'Komisi Refferal kurir';
                $dtReff['akun'] = 'admin';
                $dtReff['waktu_pendapatan'] = date('Y-m-d H:i:s');
                DB::table('rb_pendapatan_kurir')->insert($dtReff);
            }
            
        }

        if ($persenKoordinator > 0) {
            $komisiKoordinator = $komisi * ($persenKoordinator/100);
            $kasihAdmin = false;

            $cekKoordinator = DB::table('rb_sopir')->where('kota_id', $kotaKurir)->where('koordinator_kota', '1')->where('id_sopir', '!=', $id_sopir)->first();
            if ($cekKoordinator) {
                $dtKoordinator['id_konsumen'] = $cekAkunKoordinator->id_konsumen;
                $dtKoordinator['nominal'] = $komisiKoordinator;
                $dtKoordinator['id_rekening'] = 0;
                $dtKoordinator['withdraw_fee'] = 0;
                $dtKoordinator['status'] = 'Sukses';
                $dtKoordinator['transaksi'] = 'debit';
                $dtKoordinator['keterangan'] = 'Komisi Koordinator Kurir';
                $dtKoordinator['akun'] = 'koordinator';
                $dtKoordinator['waktu_pendapatan'] = date('Y-m-d H:i:s');
                DB::table('rb_pendapatan_kurir')->insert($dtKoordinator);
                    
            } else {
                $kasihAdmin = true;
            }

            if ($kasihAdmin) {
                $dtKoordinator['id_konsumen'] = 0;
                $dtKoordinator['nominal'] = $komisiKoordinator;
                $dtKoordinator['id_rekening'] = 0;
                $dtKoordinator['withdraw_fee'] = 0;
                $dtKoordinator['status'] = 'Sukses';
                $dtKoordinator['transaksi'] = 'debit';
                $dtKoordinator['keterangan'] = 'Komisi Koordinator Kota kurir';
                $dtKoordinator['akun'] = 'admin';
                $dtKoordinator['waktu_pendapatan'] = date('Y-m-d H:i:s');
                DB::table('rb_pendapatan_kurir')->insert($dtKoordinator);
            }
            
        }

        if ($persenKoordinatorKecamatan > 0) {
            $komisiKoordinatorKecamatan = $komisi * ($persenKoordinatorKecamatan/100);
            $kasihAdmin = false;

            $cekKoordinator = DB::table('rb_sopir')->where('kecamatan_id', $kecamatanKurir)->where('koordinator_kecamatan', '1')->where('id_sopir', '!=', $id_sopir)->first();
            if ($cekKoordinator) {
                $dtKoordinator['id_konsumen'] = $cekAkunKoordinator->id_konsumen;
                $dtKoordinator['nominal'] = $komisiKoordinatorKecamatan;
                $dtKoordinator['id_rekening'] = 0;
                $dtKoordinator['withdraw_fee'] = 0;
                $dtKoordinator['status'] = 'Sukses';
                $dtKoordinator['transaksi'] = 'debit';
                $dtKoordinator['keterangan'] = 'Komisi Koordinator Kecamatan Kurir';
                $dtKoordinator['akun'] = 'koordinator';
                $dtKoordinator['waktu_pendapatan'] = date('Y-m-d H:i:s');
                DB::table('rb_pendapatan_kurir')->insert($dtKoordinator);
                    
            } else {
                $kasihAdmin = true;
            }

            if ($kasihAdmin) {
                $dtKoordinator['id_konsumen'] = 0;
                $dtKoordinator['nominal'] = $komisiKoordinatorKecamatan;
                $dtKoordinator['id_rekening'] = 0;
                $dtKoordinator['withdraw_fee'] = 0;
                $dtKoordinator['status'] = 'Sukses';
                $dtKoordinator['transaksi'] = 'debit';
                $dtKoordinator['keterangan'] = 'Komisi Koordinator kurir';
                $dtKoordinator['akun'] = 'admin';
                $dtKoordinator['waktu_pendapatan'] = date('Y-m-d H:i:s');
                DB::table('rb_pendapatan_kurir')->insert($dtKoordinator);
            }
            
        }


        if ($persenKasCabang > 0) {
            $komisiKasCabang = $komisi * ($persenKasCabang/100);

            $dtKasCabang['id_konsumen'] = 0;
            $dtKasCabang['nominal'] = $komisiKasCabang;
            $dtKasCabang['id_rekening'] = 0;
            $dtKasCabang['withdraw_fee'] = 0;
            $dtKasCabang['status'] = 'Sukses';
            $dtKasCabang['transaksi'] = 'debit';
            $dtKasCabang['keterangan'] = '';
            $dtKasCabang['akun'] = 'cabang';
            $dtKasCabang['waktu_pendapatan'] = date('Y-m-d H:i:s');
            DB::table('rb_pendapatan_kurir')->insert($dtKasCabang);
        }


        if ($persenAgen > 0) {
            $komisiAgen = $komisi * ($persenAgen/100);
            $kasihAdmin = false;

            $cek_agen = DB::table('kurir_order')->where('id', $id_order)->where('id_agen', '>', '0')->first();
            if ($cek_agen) {
                $dtAgen['id_konsumen'] = $cek_agen->id_agen;
                $dtAgen['nominal'] = $komisiAgen;
                $dtAgen['id_rekening'] = 0;
                $dtAgen['withdraw_fee'] = 0;
                $dtAgen['status'] = 'Sukses';
                $dtAgen['transaksi'] = 'debit';
                $dtAgen['keterangan'] = 'Komisi Agen Kurir';
                $dtAgen['akun'] = 'agen';
                $dtAgen['waktu_pendapatan'] = date('Y-m-d H:i:s');
                DB::table('rb_pendapatan_kurir')->insert($dtAgen);
            } else {
                $kasihAdmin = true;
            }

            if ($kasihAdmin) {
                $dtAgen['id_konsumen'] = 0;
                $dtAgen['nominal'] = $komisiAgen;
                $dtAgen['id_rekening'] = 0;
                $dtAgen['withdraw_fee'] = 0;
                $dtAgen['status'] = 'Sukses';
                $dtAgen['transaksi'] = 'debit';
                $dtAgen['keterangan'] = 'Komisi Agen kurir';
                $dtAgen['akun'] = 'admin';
                $dtAgen['waktu_pendapatan'] = date('Y-m-d H:i:s');
                DB::table('rb_pendapatan_kurir')->insert($dtAgen);
            }
        }
    }


    
}
