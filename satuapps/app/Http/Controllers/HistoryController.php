<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Session;
use DB;
use App;
class HistoryController extends Controller
{
    public function index(Request $req)
    {
        App::setLocale(session()->get('locale'));

        if (!cek_login()){
            return redirect('/login');
        }

        if ($this->getDataToken(Session::get('token')) == null) {
            return redirect('/home');
        }
        
        $data['title'] = __('bahasa.History');
        $data['header'] = 'goback';
        $data['menu'] = '';
        $data['tglAwal'] = date('Y-m-01');
        $data['tglAkhir'] = date('Y-m-d');
        
        return view('history',$data);
    }


    public function searchData(Request $req)
    {
        App::setLocale(session()->get('locale'));
        
        $tglAwal = date('Y-m-d 00:00:01', strtotime($req->tgl_awal));
        $tglAkhir = date('Y-m-d 23:59:59', strtotime($req->tgl_akhir));

        $user_id = $this->getDataToken(Session::get('token'))->id_konsumen;

        $getData = DB::table('kurir_order')->where('id_pemesan', $user_id)->whereBetween('tanggal_order', [$tglAwal, $tglAkhir])->orderBy('tanggal_order','desc')->get();

        echo '
        <div class="section mt-2">
            <div class="card">
                <ul class="listview flush transparent no-line image-listview detailed-list mt-1 mb-1">';

                if (count($getData) <= 0) {
                    echo '<span class="badge bg-danger align-center">'.__('bahasa.notif_belum_ada_order').'</span>';
                }
        foreach ($getData as $val) {
            if ($val->alamat_jemput == '') {
                $responseJemput = Http::attach('token',Session::get('token')) 
                ->withHeaders([ 
                    'Authorization'=> api_token(),
                ]) 
                ->post('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$val->titik_jemput.'&key=AIzaSyA3LBjGfAARNWUtl7mVk2APMsi4w4Y9Nx8');

                $dtJemput = json_decode($responseJemput->body());
                $dtJemput = $dtJemput->plus_code->compound_code;
            } else {
                $dtJemput = $val->alamat_jemput;
            }
            

            if ($val->alamat_antar == ''){
                $responseAntar = Http::attach('token',Session::get('token')) 
                ->withHeaders([ 
                    'Authorization'=> api_token(),
                ]) 
                ->post('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$val->titik_antar.'&key=AIzaSyA3LBjGfAARNWUtl7mVk2APMsi4w4Y9Nx8');

                $dtAntar = json_decode($responseAntar->body());
                $dtAntar = $dtAntar->plus_code->compound_code;
            } else {
                $dtAntar = $val->alamat_antar;
            }
            

            if ($val->status == 'CANCEL') {
                $status = '<span class="badge bg-danger">'.$val->status.'</span>';
            } else if ($val->status == 'FINISH') {
                $status = '<span class="badge bg-primary">'.$val->status.'</span>';
            } else {
                $status = '<span class="badge bg-warning">'.$val->status.'</span>';
            }

            if ($val->jenis_layanan == 'FOOD') {
                $link = url('detail_food/'.base64_encode($val->id));
            } else if ($val->jenis_layanan == 'SHOP') {
                $link = url('detail_shop/'.base64_encode($val->id));
            } else if ($val->jenis_layanan == 'RIDE') {
                $link = url('detail_ride/'.base64_encode($val->id));
            } else if ($val->jenis_layanan == 'KURIR') {
                $link = url('detail_order/'.base64_encode($val->id));
            } else {
                $link = '#';
            }
            
            

            echo '
            <li>
                <a href="'.$link.'" class="item">
                    <div class="in">
                        <div>
                            <strong>'.date('d M Y', strtotime($val->tanggal_order)).$status.'<span class="badge bg-secondary">'.$val->jenis_layanan.'</span></strong>
                            <div class="text-small text-secondary"><span class="fw-bold">'.__('bahasa.dari').': </span>'.$dtJemput.'</div>
                            <div class="text-small text-secondary"><span class="fw-bold">'.__('bahasa.ke').': </span>'.$dtAntar.'</div>
                        </div>
                        <div class="text-end">
                            <strong>'.__('bahasa.kurs').number_format($val->tarif).'</strong>
                            <div>&nbsp;</div>
                            <div>&nbsp;</div>
                            <div>&nbsp;</div>
                            <div>&nbsp;</div>
                        </div>
                    </div>
                </a>
            </li>
            ';
        }
        echo '
                </div>
            </div>
        </div>';

    }


}