<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Session;
use DB;
use App;
class RideController extends Controller
{
    public function index(Request $req)
    {
        App::setLocale(session()->get('locale'));

        if (!cek_login()){
            return redirect('/login');
        }
        $cekUser = DB::table('rb_konsumen')->where('remember_token', Session::get('token'))->first();
        $cekOrderExist = DB::table('kurir_order')->where('id_pemesan', $cekUser->id_konsumen)->whereNotIn('status', ['FINISH', 'CANCEL'])->first();
        if ($cekOrderExist) {
            return redirect('detail_order/'.base64_encode($cekOrderExist->id));
        }
        $data['saldo'] = get_saldo(Session::get('token'));
        
        $data['title'] = 'Ride';
        $data['header'] = 'default';
        $data['menu'] = '';

        return view('ride.home',$data);
    }


    public function getOngkos(Request $req)
    {
        App::setLocale(session()->get('locale'));

        $titikJemput = $req->titikJemput;
        $titikAntar = $req->titikAntar;

        $response = Http::withHeaders([ 
            'Accept'=> '*/*', 
        ]) 
        ->get('https://apisatutoko.kitabuatin.com/api/kurir/v1/hitungJarak/'.trim($titikJemput,' ').'/'.trim($titikAntar, ' ').'/REGULAR');
        
        if ($response->body() < 0) {
            http_response_code(404);
            exit(json_encode(__('bahasa.notif_jarak_kejauhan')));
        }

        http_response_code(200);
        $data['ongkir'] = number_format($response->body());

        return $data;
    }


    public function putOrder(Request $req)
    {
        
        $titikAmbil = $req->titik_ambil;
        $alamatAmbil = $req->alamat_ambil;
        $kotaAmbil = $req->kota_ambil;
        $provinsiAmbil = $req->provinsi_ambil;
        $kodePosAmbil = $req->kode_pos_ambil;
        $negaraAmbil = $req->negara_ambil;
        $alamatLongAmbil = $req->alamat_long_ambil;

        $titikAntar = $req->titik_antar;
        $alamatAntar = $req->alamat_antar;
        $kotaAntar = $req->kota_antar;
        $provinsiAntar = $req->provinsi_antar;
        $kodePosAntar = $req->kode_pos_antar;
        $negaraAntar = $req->negara_antar;
        $alamatLongAntar = $req->alamat_long_antar;

        $metodePembayaran = $req->pembayaran_via;

        $dataTambahan = array(
            'alamat_jemput' => $alamatLongAmbil,

            'alamat_antar' => $alamatLongAntar,
        );

        $response = Http::attach('titik_jemput',$titikAmbil)
                ->attach('titik_tujuan',$titikAntar)
                ->attach('service','REGULAR')
                ->attach('source','APPS')
                ->attach('login_token',Session::get('token'))
                ->attach('jenis_layanan','RIDE')
                ->attach('metode_pembayaran',$metodePembayaran)
                ->attach('data_tambahan',json_encode($dataTambahan))
                ->withHeaders([ 
                    'Accept'=> '*/*',
                    'Authorization'=> api_token(),
                ]) 
                ->post(api_url().'api/kurir/v1/orderKurir'); 

        $getOrder = json_decode($response->body());

        return redirect('detail_ride/'.base64_encode($getOrder->id));
    }


    public function detailOrder($id)
    {
        App::setLocale(session()->get('locale'));

        if (!cek_login()){
            return redirect('/login');
        }

        $id = base64_decode($id);

        $data['order'] = DB::table('kurir_order as a')->select('a.id', 'a.titik_jemput', 'a.titik_antar', 'a.service', 'a.id_penjualan', 'a.status', 'a.source', 'a.tarif', 'a.id_sopir', 'a.kode_order', 'c.nama_reseller as nama_toko', 'c.alamat_lengkap as alamat_toko', 'd.nama_lengkap as nama_konsumen', 'd.alamat_lengkap as alamat_konsumen', 'a.pemberi_barang', 'a.alamat_jemput', 'a.alamat_antar', 'a.penerima_barang')->leftJoin('rb_penjualan as b', 'b.id_penjualan', 'a.id_penjualan')->leftJoin('rb_reseller as c', 'c.id_reseller', 'b.id_penjual')->leftJoin('rb_konsumen as d', 'd.id_konsumen', 'b.id_pembeli')->where('a.id', $id)->first();
        $data['timeline'] = DB::table('kurir_order_log')->where('id_order', $id)->get();
        $data['warna_timeline'] = ['bg-primary','bg-info','bg-danger','bg-warning'];
        
        $data['title'] = __('bahasa.detail_order');
        $data['header'] = 'default';
        $data['menu'] = '';

        return view('ride.detail',$data);
    }





}