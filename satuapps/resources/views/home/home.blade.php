@extends('layout.template')

@section('title', $title)

@section('content')
    <!-- App Capsule -->
    <div id="appCapsule" class="full-height">

        <div class="section wallet-card-section pt-1">
            <div class="wallet-card">
                <!-- Balance -->
                <div class="balance">
                    <div class="left">
                        <span class="title">{{__('bahasa.selamat_datang')}}, {{Session::get('nama_lengkap')}}</span>
                        <!-- <h1 class="total" id="saldo">Rp. {{number_format($saldo)}}</h1> -->
                    </div>
                    <!-- <div class="right">
                        <a href="#" class="button" data-bs-toggle="modal" data-bs-target="#depositActionSheet">
                            <ion-icon name="add-outline" role="img" class="md hydrated" aria-label="add outline"></ion-icon>
                        </a>
                    </div> -->
                </div>
                <!-- * Balance -->
                <!-- Wallet Footer -->
                <div class="wallet-footer">
                    <div class="item">
                        <a href="{{route('ride')}}">
                            <div class="icon-wrapper bg-danger">
                                <ion-icon name="bicycle-outline" role="img" class="md hydrated" aria-label="arrow down outline"></ion-icon>
                            </div>
                            <strong>Ride</strong>
                        </a>
                    </div>
                    <div class="item">
                        <a href="{{route('send')}}">
                            <div class="icon-wrapper">
                                <ion-icon name="cube-outline" role="img" class="md hydrated" aria-label="arrow forward outline"></ion-icon>
                            </div>
                            <strong>Send</strong>
                        </a>
                    </div>
                    <div class="item">
                        <a href="{{route('shop')}}">
                            <div class="icon-wrapper bg-success">
                                <ion-icon name="cart-outline" role="img" class="md hydrated" aria-label="card outline"></ion-icon>
                            </div>
                            <strong>Shop</strong>
                        </a>
                    </div>
                    <div class="item">
                        <a href="{{route('food')}}">
                            <div class="icon-wrapper bg-warning">
                                <ion-icon name="fast-food" role="img" class="md hydrated" aria-label="swap vertical"></ion-icon>
                            </div>
                            <strong>Food</strong>
                        </a>
                    </div>

                </div>
                <!-- * Wallet Footer -->
            </div>
        </div>



        <div class="section full mb-3 mt-3">
            <!-- <div class="section-title">News Update</div> -->

            <!-- carousel single -->
            <div class="carousel-single splide">
                <div class="splide__track">
                    <ul class="splide__list" >

                    @foreach ($dtBanner as $row) 
                        
                        <li class="splide__slide">
                            @if($row->url == '')
                            <a href="#">
                            @else
                            <a href="{{$row->url}}" target="_blank">
                            @endif
                            <div class="card">
                                <img src="{{image_url()}}public/uploads/banner/{{$row->gambar}}" class="card-img-top" alt="image">
                                <div class="card-body">
                                    <h5 class="card-title">{{$row->judul}}</h5>
                                    <p class="card-text">
                                    {{$row->keterangan}}
                                    </p>
                                </div>
                            </div>
                            </a>
                        </li>
                        
                    @endforeach

                    </ul>
                </div>
            </div>

        </div>

        

    </div>
    
    

    <script>

    var posisi = 'depan';

    $( document ).ready(function() {
        // getOrder()
        // getSaldo()
        // setInterval(function() {
            // getOrder()
        // }, 5000);

        const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
        if (isMobile) {
            // posisi = 'belakang'
        } else {
            // posisi = 'depan'
        }


    });


    </script>

@endsection
