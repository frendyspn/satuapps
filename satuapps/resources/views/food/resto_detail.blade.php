@extends('layout.template')

@section('title', $title)

@section('content')

<div id="appCapsule" class="full-height">
    <div class="section tab-content mt-2 mb-1">
        <div class="tab-pane fade active show" id="waiting" role="tabpanel">
            <div class="row">
                @php $kategori = ''; @endphp
                @foreach($produk as $row)
                @if($row->id_kategori_produk != $kategori)
                <div class="col-12">
                    <p class="fw-bold">{{$row->nama_kategori}}</p>
                </div>
                @php $kategori = $row->id_kategori_produk; @endphp
                @endif
                <div class="col-6 mb-2">
                    <div class="bill-box">
                        <div class="img-wrapper">
                            @php
                            $img = explode(';', $row->gambar)
                            @endphp
                            @if($img[0] != '')
                            <img src="https://satutoko.id/asset/foto_produk/{{$img[0]}}" alt="img" class="image-block imaged w48">
                            @else
                            <img src="https://satutoko.id/asset/foto_statis/no-image.jpg" alt="img" class="image-block imaged w48">
                            @endif
                        </div>
                        <p>{{$row->nama_produk}}</p>
                        <div class="price" style="font-size:1rem">{{__('bahasa.kurs')}} {{number_format($row->harga_konsumen)}}</div>
                        
                        <a href="#" onclick="tambah_cart('{{$row->id_reseller}}', '{{$row->id_produk}}', '{{$row->nama_produk}}', '{{$row->harga_konsumen}}', '{{$img[0]}}')" class="btn btn-primary btn-block btn-sm"><ion-icon name="cart-outline"></ion-icon></a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

<script>
    var arr_cart = []

    $( document ).ready(function() {
        view_cart()
    });

    function tambah_cart(id_reseller, id_produk, nama_produk, harga_produk, gambar, replace = false) {
        console.log(id_produk)
        $.ajax({
            url: "{{route('add_cart_food')}}",
            type: "POST",   
            dataType:"JSON",
            data: {id_reseller, id_produk, nama_produk, harga_produk, gambar, replace},
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') 
            },
            success: function(response) {
                console.log(response)
                $('#total_cart').html(response.keranjang.length)
                loaderHide()
                notif('bg-primary', "{{__('bahasa.notif_berhasil_masuk_keranjang')}}")
            },
            error: function(err) {
                // console.log(err);
                // $('#content-order').html(err)
            }
        });
    }

    function view_cart() {
        $.ajax({
            url: "{{route('view_cart_food')}}",
            type: "POST",   
            dataType:"JSON",
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') 
            },
            success: function(response) {
                console.log(response)
                $('#total_cart').html(response.keranjang.length)
            },
            error: function(err) {
                // console.log(err);
                // $('#content-order').html(err)
            }
        });
    }

    
</script>
@endsection