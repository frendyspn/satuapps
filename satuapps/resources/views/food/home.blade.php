@extends('layout.template')

@section('title', $title)



@section('content')



    <style>
    .map {
       width: 300px;
    }
    </style>



<script>
    var posisi = 'depan';
    var longlat = ''

    $( document ).ready(function() {
        loaderShow()
        $('#latitude').change(function(){
            console.log('OKE 2')
            longlat = $('#latitude').val()
            $.get({ url: `https://maps.googleapis.com/maps/api/geocode/json?latlng=`+longlat+`&sensor=false&key=AIzaSyAjruIgxJm6OS9ewzXUJ17CR7DMPcozpfo`, success(data) {
                console.log(data.results[0].formatted_address);
                $('#alamat_antar').html(data.results[0].formatted_address)
                getResto(longlat)
            }});
        });

    });
</script>





    <!-- App Capsule -->
    <div id="appCapsule" class="full-height">

        <div class="section inset mt-2">
            <div class="accordion" id="accordionExample1">
                <form action="{{route('put_order_ride')}}" method="post">
                    @csrf
                <div class="accordion-item">
                    <h2 class="accordion-header">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accordion01" aria-expanded="false" onclick="initMapAmbil()">
                            <span id="alamat_antar">{{__('bahasa.antar_ke')}}....</span>
                        </button>
                    </h2>
                    <div id="accordion01" class="accordion-collapse collapse" data-bs-parent="#accordionExample1" style="">
                        <div class="accordion-body">
                            
                            <div class="form-group basic row">
                                <div class="input-wrapper col-12">
                                    <label class="label" for="text4">{{__('bahasa.Alamat')}} <span class="text-danger">*{{__('bahasa.harus_diisi')}}</span></label>
                                    <input type="text" class="form-control" id="location-input" name="alamat_antar" placeholder="{{__('bahasa.Alamat')}}" required>
                                    <i class="clear-input">
                                        <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                                    </i>
                                </div>
                            </div>

                            <input type="hidden" class="form-control" id="locality-input" name="kota_antar" placeholder="Kota">
                            <input type="hidden" class="form-control" id="administrative_area_level_1-input" name="provinsi_antar" placeholder="Provinsi">
                            <input type="hidden" class="form-control" id="postal_code-input" name="kode_pos_antar" placeholder="Kode Pos">
                            
                            <input type="hidden" class="form-control" id="catatan_antar" name="catatan_antar" placeholder="Catatan">
            
                            <input type="hidden" class="form-control" id="country-input" name="negara_antar" placeholder="Alamat">

                            <input type="hidden" class="form-control" id="long_lat-input" name="titik_antar" placeholder="Long Lat" onchange="drawMap()">
                            
                            <input type="hidden" class="form-control" id="alamat_long_antar" name="alamat_long_antar" placeholder="Long Lat">

                            <div class="map" id="gmp-map"></div>

                        </div>
                    </div>
                </div>
                
                </form>
            </div>

        </div>

        <div class="section tab-content mt-2 mb-1">

            <!-- waiting tab -->
            <div class="tab-pane fade active show" id="waiting" role="tabpanel">
                <div class="row" id="list_of_resto">
                    
                </div>
            </div>
            <!-- * waiting tab -->
        </div>
        

    </div>
    
    
<script>
    function getResto(longlat){
        console.log('cek '+longlat)
        $.ajax({
            url: "{{route('get_resto_longlat')}}",
            type: "POST",   
            dataType:"HTML",
            data: {longlat},
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') 
            },
            success: function(response) {
                console.log(response)
                $('#list_of_resto').html(response)
                loaderHide()
            },
            error: function(err) {
                // console.log(err);
                // $('#content-order').html(err)
            }    
        });
    }
</script>


<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjruIgxJm6OS9ewzXUJ17CR7DMPcozpfo&libraries=places&callback=getMap&solution_channel=GMP_QB_addressselection_v1_cAB" async defer></script> -->
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjruIgxJm6OS9ewzXUJ17CR7DMPcozpfo&libraries=places&solution_channel=GMP_QB_addressselection_v1_cAB&v=weekly" async defer></script> -->
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjruIgxJm6OS9ewzXUJ17CR7DMPcozpfo&callback=getMap&v=weekly" defer></script> -->
@endsection


@section('js')

@endsection