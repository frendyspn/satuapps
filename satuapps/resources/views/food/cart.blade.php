@extends('layout.template')

@section('title', $title)

@section('content')

<div id="appCapsule" class="full-height">
    <div class="section tab-content mt-2 mb-1">
        <a href="{{route('hapus_cart_food')}}" class="btn btn-primary btn-sm mb-2">{{__('bahasa.hapus_cart')}}</a>
        <input type="hidden" name="titik_antar" id="titik_antar">
        <div class="transactions mb-1">
            <a href="#" class="item">
                <div class="detail">
                    <div>
                        <strong>{{__('bahasa.kirim_ke')}}:</br><span id="alamat_antar"></span></strong>
                    </div>
                </div>
            </a>
        </div>
        <div class="transactions" id="list_cart">
                <!-- item -->
                
                
        </div>
    </div>
</div>

<script>
    $( document ).ready(function() {
        loaderShow()
        $('#latitude').change(function(){
            console.log('OKE 2')
            longlat = $('#latitude').val()
            $('#titik_antar').val(longlat)
            $.get({ url: `https://maps.googleapis.com/maps/api/geocode/json?latlng=`+longlat+`&sensor=false&key=AIzaSyAjruIgxJm6OS9ewzXUJ17CR7DMPcozpfo`, success(data) {
                console.log(data.results[0].formatted_address);
                $('#alamat_antar').html(data.results[0].formatted_address)
                view_cart()
                
            }});
        });
        
        
    });

    function view_cart() {
        loaderShow()
        $.ajax({
            url: "{{route('view_checkout_food')}}",
            type: "POST",   
            dataType:"HTML",
            data:{titik_antar:$('#titik_antar').val(), alamat_antar:$('#alamat_antar').html()},
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') 
            },
            success: function(response) {
                $('#list_cart').html(response)
                loaderHide()
            },
            error: function(err) {
                loaderHide()
                // console.log(err);
                // $('#content-order').html(err)
            }
        });
    }

    function update_cart(index, qty) {
        loaderShow()
        console.log(index+' || '+qty)
        $.ajax({
            url: "{{route('update_checkout_food')}}",
            type: "POST",   
            dataType:"JSON",
            data:{index,qty},
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') 
            },
            success: function(response) {
                view_cart()
                loaderHide()
            },
            error: function(err) {
                loaderHide()
                // console.log(err);
                // $('#content-order').html(err)
            }
        })
    }
</script>
@endsection