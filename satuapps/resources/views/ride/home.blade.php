@extends('layout.template')

@section('title', $title)

@section('content')
    <style>
    .map {
       width: 300px;
    }
    </style>


<script>
    "use strict";

    function initMapAmbil() {
        const CONFIGURATION = {
            "ctaTitle": "Checkout",
            "mapOptions": {"center":{"lat":37.4221,"lng":-122.0841},"fullscreenControl":false,"mapTypeControl":false,"streetViewControl":false,"zoom":18,"zoomControl":false,"maxZoom":22,"mapId":""},
            "mapsApiKey": "AIzaSyAjruIgxJm6OS9ewzXUJ17CR7DMPcozpfo",
            "capabilities": {"addressAutocompleteControl":true,"mapDisplayControl":true,"ctaControl":false}
        };
        const componentForm = [
            'location',
            'locality',
            'administrative_area_level_1',
            'country',
            'postal_code',
        ];

        const getFormInputElement = (component) => document.getElementById(component + '-input');
        const map = new google.maps.Map(document.getElementById("gmp-map"), {
            zoom: CONFIGURATION.mapOptions.zoom,
            center: { lat: 37.4221, lng: -122.0841 },
            mapTypeControl: false,
            fullscreenControl: CONFIGURATION.mapOptions.fullscreenControl,
            zoomControl: CONFIGURATION.mapOptions.zoomControl,
            streetViewControl: CONFIGURATION.mapOptions.streetViewControl
        });
        const marker = new google.maps.Marker({map: map, draggable: true});
        const autocompleteInput = getFormInputElement('location');
        const autocomplete = new google.maps.places.Autocomplete(autocompleteInput, {
            fields: ["address_components", "geometry", "name"],
            types: ["address"],
        });
        autocomplete.addListener('place_changed', function () {
            marker.setVisible(false);
            const place = autocomplete.getPlace();
            if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert('No details available for input: \'' + place.name + '\'');
            return;
            }
            renderAddress(place);
            fillInAddress(place);
        });

        function fillInAddress(place) {  // optional parameter
            const addressNameFormat = {
            'street_number': 'short_name',
            'route': 'long_name',
            'locality': 'long_name',
            'administrative_area_level_1': 'short_name',
            'country': 'long_name',
            'postal_code': 'short_name',
            };
            
            const getAddressComp = function (type) {
            for (const component of place.address_components) {
                if (component.types[0] === type) {
                    $('#alamat_long_jemput').val($('#alamat_long_jemput').val()+component[addressNameFormat[type]]+' ')
                return component[addressNameFormat[type]];
                }
            }
            
            $('#long_lat-input').val(place.geometry.viewport.Ua.hi+','+place.geometry.viewport.Ga.lo)
            $('#alamat_ambil').html($('#alamat_long_jemput').val())
            return '';
            };
            getFormInputElement('location').value = getAddressComp('street_number') + ' '
                    + getAddressComp('route');
            for (const component of componentForm) {
            // Location field is handled separately above as it has different logic.
            if (component !== 'location') {
                getFormInputElement(component).value = getAddressComp(component);
            }
            }

            drawMap()
        }

        function renderAddress(place) {
            map.setCenter(place.geometry.location);
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);
        }


    }

    function initMapAntar() {
        const CONFIGURATION_ANTAR = {
            "ctaTitle": "Checkout",
            "mapOptions": {"center":{"lat":37.4221,"lng":-122.0841},"fullscreenControl":false,"mapTypeControl":false,"streetViewControl":false,"zoom":18,"zoomControl":false,"maxZoom":22,"mapId":""},
            "mapsApiKey": "AIzaSyAjruIgxJm6OS9ewzXUJ17CR7DMPcozpfo",
            "capabilities": {"addressAutocompleteControl":true,"mapDisplayControl":true,"ctaControl":false}
        };
        const componentFormAntar = [
            'location',
            'locality',
            'administrative_area_level_1',
            'country',
            'postal_code',
        ];

        const getFormInputElementAntar = (component) => document.getElementById(component + '-antar-input');
        const map_antar = new google.maps.Map(document.getElementById("gmp-map-antar"), {
            zoom: CONFIGURATION_ANTAR.mapOptions.zoom,
            center: { lat: 37.4221, lng: -122.0841 },
            mapTypeControl: false,
            fullscreenControl: CONFIGURATION_ANTAR.mapOptions.fullscreenControl,
            zoomControl: CONFIGURATION_ANTAR.mapOptions.zoomControl,
            streetViewControl: CONFIGURATION_ANTAR.mapOptions.streetViewControl
        });
        const markerAntar = new google.maps.Marker({map: map_antar, draggable: true});
        const autocompleteInputAntar = getFormInputElementAntar('location');
        const autocompleteantar = new google.maps.places.Autocomplete(autocompleteInputAntar, {
            fields: ["address_components", "geometry", "name"],
            types: ["address"],
        });
        autocompleteantar.addListener('place_changed', function () {
            markerAntar.setVisible(false);
            const place = autocompleteantar.getPlace();
            if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert('No details available for input: \'' + place.name + '\'');
            return;
            }
            renderAddress(place);
            fillInAddressAntar(place);
        });

        function fillInAddressAntar(place) {  // optional parameter
            const addressNameFormat = {
            'street_number': 'short_name',
            'route': 'long_name',
            'locality': 'long_name',
            'administrative_area_level_1': 'short_name',
            'country': 'long_name',
            'postal_code': 'short_name',
            };
            
            const getAddressComp = function (type) {
            for (const component of place.address_components) {
                if (component.types[0] === type) {
                    $('#alamat_long_antar').val($('#alamat_long_antar').val()+component[addressNameFormat[type]]+' ')
                return component[addressNameFormat[type]];
                }
            }
            
            $('#long_lat-antar-input').val(place.geometry.viewport.Ua.hi+','+place.geometry.viewport.Ga.lo)
            $('#alamat_antar').html($('#alamat_long_antar').val())
            return '';
            };
            getFormInputElementAntar('location').value = getAddressComp('street_number') + ' '
                    + getAddressComp('route');
            for (const component of componentFormAntar) {
            // Location field is handled separately above as it has different logic.
            if (component !== 'location') {
                getFormInputElementAntar(component).value = getAddressComp(component);
            }
            }

            drawMap()
        }

        function renderAddress(place) {
            map_antar.setCenter(place.geometry.location);
            markerAntar.setPosition(place.geometry.location);
            markerAntar.setVisible(true);
        }

    }




    function getMap(titik_ambil, titik_antar) {
        var arr_titik_ambil = titik_ambil.split(',');
        var arr_titik_antar = titik_antar.split(',');

        console.log(arr_titik_ambil)
        console.log(arr_titik_antar)

        const bounds = new google.maps.LatLngBounds();
        const markersArray = [];
        const map = new google.maps.Map(document.getElementById("map"), {
            center: { lat: 55.53, lng: 9.4 },
            fullscreenControl:false,
            mapTypeControl:false,
            streetViewControl:false,
            zoom:18,
            zoomControl:false,
            maxZoom:22
        });
        // initialize services
        const geocoder = new google.maps.Geocoder();
        const service = new google.maps.DistanceMatrixService();
        // build request
        const origin1 = { lat: parseFloat(arr_titik_ambil[0]), lng: parseFloat(arr_titik_ambil[1]) };
        // const origin2 = "Greenwich, England";
        // const destinationA = "Stockholm, Sweden";
        const destinationA = { lat: parseFloat(arr_titik_antar[0]), lng: parseFloat(arr_titik_antar[1]) };
        const request = {
            origins: [origin1],
            destinations: [destinationA],
            travelMode: google.maps.TravelMode.DRIVING,
            unitSystem: google.maps.UnitSystem.METRIC,
            avoidHighways: false,
            avoidTolls: false,
        };

        // put request on page
        document.getElementById("request").innerText = JSON.stringify(
            request,
            null,
            2,
        );
        // get distance matrix response
        service.getDistanceMatrix(request).then((response) => {
            // put response
            document.getElementById("response").innerText = JSON.stringify(
            response,
            null,
            2,
            );

            // show on map
            const originList = response.originAddresses;
            const destinationList = response.destinationAddresses;

            deleteMarkers(markersArray);

            const showGeocodedAddressOnMap = (asDestination) => {
            const handler = ({ results }) => {
                map.fitBounds(bounds.extend(results[0].geometry.location));
                markersArray.push(
                new google.maps.Marker({
                    map,
                    position: results[0].geometry.location,
                    label: asDestination ? "D" : "O",
                }),
                );
            };
            return handler;
            };

            for (let i = 0; i < originList.length; i++) {
            const results = response.rows[i].elements;

            geocoder
                .geocode({ address: originList[i] })
                .then(showGeocodedAddressOnMap(false));

            for (let j = 0; j < results.length; j++) {
                geocoder
                .geocode({ address: destinationList[j] })
                .then(showGeocodedAddressOnMap(true));
            }
            }
        });
    }

        function deleteMarkers(markersArray) {
            for (let i = 0; i < markersArray.length; i++) {
                markersArray[i].setMap(null);
            }

            markersArray = [];
        }


    function drawMapOrigin(LongLat) {
        var arr_longLat = LongLat.split(',')
        var $map = $('#map');
            var latitude = parseFloat(arr_longLat[0]);
            var longtitude = parseFloat(arr_longLat[1]);
            var center = new google.maps.LatLng(latitude, longtitude);

            var myMarker = new google.maps.Marker({
                position: new google.maps.LatLng(latitude, longtitude),
                draggable: true
            });
            // Render map
            var map = new google.maps.Map($map[0], {
                zoom: 16,
                scrollWheel: false,
                center: center,

                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: false,
                panControlOptions: {
                    position: google.maps.ControlPosition.LEFT_CENTER
                },
                zoomControlOptions: {
                    position: google.maps.ControlPosition.LEFT_CENTER
                },
                scaleControlOptions: {
                    position: google.maps.ControlPosition.LEFT_CENTER
                },
                marker:{map: center, draggable: true}
            });

            
    }


        function drawMap() {
            console.log('Drawing Maps')
            console.log($('#long_lat-input').val() +' - '+ $('#long_lat-antar-input').val())
            if ($('#long_lat-input').val() !== '' && $('#long_lat-antar-input').val() === '') {
                drawMapOrigin($('#long_lat-input').val())
            }else if ($('#long_lat-input').val() !== '' && $('#long_lat-antar-input').val() !== '') {
                console.log('Drawing Maps OKE')
                getMap($('#long_lat-input').val(), $('#long_lat-antar-input').val())
                getOngkir($('#long_lat-input').val(), $('#long_lat-antar-input').val())
            } else{
                drawMapOrigin("-6.2297209,106.6647035")
            }
        }
    </script>



    <script>

    var posisi = 'depan';

    $( document ).ready(function() {
        // getOrder()
        // getSaldo()
        // setInterval(function() {
            // getOrder()
        // }, 5000);
        
        drawMap()
        

        const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
        if (isMobile) {
            posisi = 'belakang'
        } else {
            posisi = 'depan'
        }

    });


    function pakai_info(type) {
        if (type === 'penyerah') {
            $('#nama_penyerah').val('{{Session::get("nama_lengkap")}}')
            $('#telp_penyerah').val('{{Session::get("no_hp")}}')
        } else {
            $('#nama_penerima').val('{{Session::get("nama_lengkap")}}')
            $('#telp_penerima').val('{{Session::get("no_hp")}}')
        }
    }

    function getOngkir(titikJemput, titikAntar) {
        
        $.ajax({
            url: "{{route('get_ongkos')}}",
            type: "POST",   
            dataType:"JSON",
            data: {titikJemput, titikAntar},
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') 
            },
            success: function(response) {
                console.log(response)
                $('#harga-ongkir').html(response.ongkir)
                $('#btn-order').removeAttr('disabled')
            },
            error: function(err) {
                // console.log(err);
                // $('#content-order').html(err)
            }    
        });
    }

    </script>



    <!-- App Capsule -->
    <div id="appCapsule" class="full-height">

        <div class="section inset mt-2">
            <div class="accordion" id="accordionExample1">
                <form action="{{route('put_order_ride')}}" method="post">
                    @csrf
                <div class="accordion-item">
                    <h2 class="accordion-header">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accordion01" aria-expanded="false" onclick="initMapAmbil()">
                            <span id="alamat_ambil">{{__('bahasa.jemput_di')}}....</span>
                        </button>
                    </h2>
                    <div id="accordion01" class="accordion-collapse collapse" data-bs-parent="#accordionExample1" style="">
                        <div class="accordion-body">
                            
                            <div class="form-group basic row">
                                <div class="input-wrapper col-12">
                                    <label class="label" for="text4">{{__('bahasa.Alamat')}} <span class="text-danger">*{{__('bahasa.harus_diisi')}}</span></label>
                                    <input type="text" class="form-control" id="location-input" name="alamat_ambil" placeholder="{{__('bahasa.Alamat')}}" required>
                                    <i class="clear-input">
                                        <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                                    </i>
                                </div>
                            </div>

                            <input type="hidden" class="form-control" id="locality-input" name="kota_ambil" placeholder="Kota">
                            <input type="hidden" class="form-control" id="administrative_area_level_1-input" name="provinsi_ambil" placeholder="Provinsi">
                            <input type="hidden" class="form-control" id="postal_code-input" name="kode_pos_ambil" placeholder="Kode Pos">
                            
                            <input type="hidden" class="form-control" id="catatan_ambil" name="catatan_ambil" placeholder="Catatan">
            
                            <input type="hidden" class="form-control" id="country-input" name="negara_ambil" placeholder="Alamat">

                            <input type="hidden" class="form-control" id="long_lat-input" name="titik_ambil" placeholder="Long Lat" onchange="drawMap()">
                            
                            <input type="hidden" class="form-control" id="alamat_long_jemput" name="alamat_long_ambil" placeholder="Long Lat">

                            <div class="map" id="gmp-map"></div>

                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accordion02" aria-expanded="false" onclick="initMapAntar()">
                            <span id="alamat_antar">{{__('bahasa.antar_ke')}}...</span>
                        </button>
                    </h2>
                    <div id="accordion02" class="accordion-collapse collapse" data-bs-parent="#accordionExample1" style="">
                    <div class="accordion-body">
                            
                            <div class="form-group basic row">
                                <div class="input-wrapper col-12">
                                    <label class="label" for="text4">{{__('bahasa.Alamat')}} <span class="text-danger">*{{__('bahasa.harus_diisi')}}</span></label>
                                    <input type="text" class="form-control" id="location-antar-input" name="alamat_antar" placeholder="Alamat" required>
                                    <i class="clear-input">
                                        <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                                    </i>
                                </div>
                            </div>

                            <input type="hidden" class="form-control" id="locality-antar-input" name="kota_antar" placeholder="Kota">
                            <input type="hidden" class="form-control" id="administrative_area_level_1-antar-input" name="provinsi_antar" placeholder="Provinsi">
                            <input type="hidden" class="form-control" id="postal_code-antar-input" name="kode_pos_antar" placeholder="Kode Pos">
                            
                            <input type="hidden" class="form-control" id="country-antar-input" name="negara_antar" placeholder="Alamat">

                            <input type="hidden" class="form-control" id="long_lat-antar-input" name="titik_antar" placeholder="Long Lat" onchange="drawMap()">

                            <input type="hidden" class="form-control" id="alamat_long_antar" name="alamat_long_antar" placeholder="Long Lat">
                            
                            <div class="map" id="gmp-map-antar"></div>


                        </div>
                    </div>
                </div>

                <div class="section mt-1 mb-2">
                    <div class="section-title">{{__('bahasa.pembayaran')}}</div>
                    <div class="card">

                    <div class="btn-group" role="group">
                        <input type="radio" class="btn-check" name="pembayaran_via" value="CASH" id="pembayaran_cash" @if($saldo <= 0) checked="" @endif>
                        <label class="btn btn-outline-primary" for="btnradio1">{{__('bahasa.tunai')}}</label>

                        <input type="radio" class="btn-check" name="pembayaran_via" value="WALLET" id="pembayaran_sallet" @if($saldo > 0) checked="" @endif @if($saldo <= 0) disabled="disabled" @endif>
                        <label class="btn btn-outline-primary" for="btnradio2">{{__('bahasa.nontunai')}} {{__('bahasa.kurs')}}{{number_format($saldo)}}</label>
                    </div>

                    </div>
                </div>

                <div class="accordion-item">
                    <div id="map" class="col-12 bg-danger mt-1" style="height:500px"></div>
                    <button class="btn btn-primary btn-block mt-2" id="btn-order" disabled>{{__('bahasa.tunai')}} {{__('bahasa.kurs')}}.<span id="harga-ongkir">0</span></button>
                    <div id="sidebar" hidden>
                        <h3 style="flex-grow: 0">Request</h3>
                        <pre style="flex-grow: 1" id="request"></pre>
                        <h3 style="flex-grow: 0">Response</h3>
                        <pre style="flex-grow: 1" id="response"></pre>
                    </div>
                </div>
                </form>
            </div>

        </div>
        

    </div>
    
    
    


<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjruIgxJm6OS9ewzXUJ17CR7DMPcozpfo&libraries=places&callback=getMap&solution_channel=GMP_QB_addressselection_v1_cAB" async defer></script> -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjruIgxJm6OS9ewzXUJ17CR7DMPcozpfo&libraries=places&solution_channel=GMP_QB_addressselection_v1_cAB&v=weekly" async defer></script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjruIgxJm6OS9ewzXUJ17CR7DMPcozpfo&callback=getMap&v=weekly" defer></script> -->
@endsection
