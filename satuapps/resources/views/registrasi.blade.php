@extends('layout.template')

@section('title', $title)

@section('content')
    <!-- App Capsule -->
    <div id="appCapsule">

    <div class="section mt-2 text-center">
            <h1>Daftar</h1>
            <h4>Lengkapi Form Dibawah</h4>
        </div>
        <div class="section mb-5 p-2">

        

            <form method="post" action="{{route('registrasi_cek_no_hp')}}">
                @csrf
                <div class="card">
                    <div class="card-body pb-1">
                        <div class="form-group basic">
                            <div class="input-wrapper">
                                <input type="number" class="form-control" id="nomor_hp" name="nomor_hp" placeholder="Nomor HP">
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="form-links mt-2">
                    <div>
                        <a href="{{route('login')}}">Saya Sudah Terdaftar</a>
                    </div>
                    
                </div>

                <div class="form-button-group  transparent">
                    <button type="submit" class="btn btn-primary btn-block btn-lg">Lanjutkan</button>
                </div>

            </form>
        </div>

    </div>

    
    <!-- * App Capsule -->

@endsection
