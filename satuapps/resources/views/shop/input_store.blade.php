@extends('layout.template')

@section('title', $title)



@section('content')



    <style>
    .map {
       width: 300px;
    }
    </style>



<script>
    var posisi = 'depan';
    var longlat = ''

    
</script>





    <!-- App Capsule -->
    <div id="appCapsule" class="full-height">

        <div class="section tab-content mt-2 mb-1">

            <form action="{{route('simpan_toko')}}" method="post">
                @csrf
                <input type="text" class="form-control" id="nama_toko" name="nama_toko" placeholder="{{__('bahasa.nama_toko')}}">
                <input type="text" class="form-control" id="patokan_toko" name="patokan_toko" placeholder="{{__('bahasa.alamat_nomor')}}">
                
                <input id="pac-input" name="alamat_toko" class="form-control" type="text" placeholder="Cari Alamat" onclick="initMapAmbil()"/>
                <div id="mapnya" class="col-12 bg-danger mt-1" style="height:500px"></div>
                <input type="hidden" id="titik_toko" name="titik_toko">
                <button class="btn btn-primary btn-block">{{__('bahasa.simpan')}}</button>
            </form>
        </div>


        <div class="section tab-content mt-2 mb-1">

            <!-- waiting tab -->
            <div class="tab-pane fade active show" id="waiting" role="tabpanel">
                <div class="row" id="list_of_store">
                    
                </div>
            </div>
            <!-- * waiting tab -->
        </div>
        

    </div>

    
<script>

$( document ).ready(function() {
        loaderShow()
        $('#latitude').change(function(){
            console.log('OKE 2')
            longlat = $('#latitude').val()
            $.get({ url: `https://maps.googleapis.com/maps/api/geocode/json?latlng=`+longlat+`&sensor=false&key=AIzaSyAjruIgxJm6OS9ewzXUJ17CR7DMPcozpfo`, success(data) {
                console.log(data.results[0].formatted_address);
                $('#alamat_antar').html(data.results[0].formatted_address)
                loaderHide()
                // initAutocomplete()
                initMapAmbil()
                // drawMapOrigin(longlat)'
            }});
        });

    });

    function getStore(longlat){
        console.log('cek '+longlat)
        $.ajax({
            url: "{{route('get_store_longlat')}}",
            type: "POST",   
            dataType:"HTML",
            data: {longlat},
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') 
            },
            success: function(response) {
                $('#list_of_store').html(response)
                loaderHide()
            },
            error: function(err) {
                // console.log(err);
                // $('#content-order').html(err)
            }    
        });
    }
</script>

<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjruIgxJm6OS9ewzXUJ17CR7DMPcozpfo&libraries=places&solution_channel=GMP_QB_addressselection_v1_cAB&v=weekly" async defer></script> -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjruIgxJm6OS9ewzXUJ17CR7DMPcozpfo&libraries=places&v=weekly" defer ></script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjruIgxJm6OS9ewzXUJ17CR7DMPcozpfo&libraries=places&callback=getMap&solution_channel=GMP_QB_addressselection_v1_cAB" async defer></script> -->
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjruIgxJm6OS9ewzXUJ17CR7DMPcozpfo&libraries=places&solution_channel=GMP_QB_addressselection_v1_cAB&v=weekly" async defer></script> -->
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjruIgxJm6OS9ewzXUJ17CR7DMPcozpfo&callback=getMap&v=weekly" defer></script> -->


<script>
    function initMapAmbil() {
        (-6.2634878, 106.6111574)
        const CONFIGURATION = {
            "ctaTitle": "Checkout",
            "mapOptions": {"center":{"lat":106.816666,"lng":-6.200000},"fullscreenControl":false,"mapTypeControl":false,"streetViewControl":false,"zoom":18,"zoomControl":false,"maxZoom":22,"mapId":""},
            "mapsApiKey": "AIzaSyAjruIgxJm6OS9ewzXUJ17CR7DMPcozpfo",
            "capabilities": {"addressAutocompleteControl":true,"mapDisplayControl":true,"ctaControl":false}
        };
        const componentForm = [
            'pac',
        ];

        const getFormInputElement = (component) => document.getElementById(component + '-input');
        const map = new google.maps.Map(document.getElementById("mapnya"), {
            zoom: CONFIGURATION.mapOptions.zoom,
            center: { lat: 106.816666, lng: -6.200000 },
            mapTypeControl: false,
            fullscreenControl: CONFIGURATION.mapOptions.fullscreenControl,
            zoomControl: CONFIGURATION.mapOptions.zoomControl,
            streetViewControl: CONFIGURATION.mapOptions.streetViewControl
        });
        const marker = new google.maps.Marker({map: map, draggable: true});
        const autocompleteInput = getFormInputElement('pac');
        const autocomplete = new google.maps.places.Autocomplete(autocompleteInput, {
            fields: ["address_components", "geometry", "name"],
            types: ["address"],
        });
        autocomplete.addListener('place_changed', function () {
            marker.setVisible(false);
            const place = autocomplete.getPlace();
            if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert('No details available for input: \'' + place.name + '\'');
            return;
            }
            // drawMapOrigin(place.geometry.viewport.Ia.hi+','+place.geometry.viewport.Va.hi);
            renderAddress(place)
        });

        google.maps.event.addListener(marker,'dragend',function(event) 
        {
            console.log('ganti')
            $('#titik_toko').val(event.latLng.lng()+','+event.latLng.lat())
        });


        function renderAddress(place) {
            map.setCenter(place.geometry.location);
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);
            $('#titik_toko').val(place.geometry.location)
        }


    }

    function drawMapOrigin(LongLat) {
        var arr_longLat = LongLat.split(',')
        var $map = $('#mapnya');
            var latitude = parseFloat(arr_longLat[0]);
            var longtitude = parseFloat(arr_longLat[1]);
            var center = new google.maps.LatLng(latitude, longtitude);

            var myMarker = new google.maps.Marker({
                position: new google.maps.LatLng(latitude, longtitude),
                draggable: true
            });
            // Render map
            var map = new google.maps.Map($map[0], {
                zoom: 16,
                scrollWheel: false,
                center: center,

                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: false,
                panControlOptions: {
                    position: google.maps.ControlPosition.LEFT_CENTER
                },
                zoomControlOptions: {
                    position: google.maps.ControlPosition.LEFT_CENTER
                },
                scaleControlOptions: {
                    position: google.maps.ControlPosition.LEFT_CENTER
                },
                marker:{map: center, draggable: true}
            });

            
    }


</script>


@endsection


@section('js')

@endsection