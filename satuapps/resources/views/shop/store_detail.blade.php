@extends('layout.template')

@section('title', $title)

@section('content')

<div id="appCapsule" class="full-height">
    <div class="section mt-2 mb-2">
        <div class="section-title">{{__('bahasa.input_barang')}}</div>
        <div class="card">
            <div class="card-body">
            <input type="hidden" class="form-control" id="id_reseller" name="id_reseller" value="{{$store->id_reseller}}">
            <input type="hidden" class="form-control" id="type_store" name="type_store" value="{{ Request::segment(2) }}">

                <div class="form-group boxed">
                    <div class="input-wrapper">
                        <label class="label" for="text4b">{{__('bahasa.nama_barang')}}</label>
                        <input type="text" class="form-control" id="nama_barang" name="nama_barang" placeholder="{{__('bahasa.nama_barang')}}">
                        <i class="clear-input">
                            <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                        </i>
                    </div>
                </div>

                <div class="form-group boxed">
                    <div class="row">
                        <div class="input-wrapper col-6">
                            <label class="label" for="text4b">{{__('bahasa.satuan')}}</label>
                            <input type="text" class="form-control" id="satuan_barang" name="satuan_barang" placeholder="{{__('bahasa.satuan')}}">
                            <i class="clear-input">
                                <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                            </i>
                        </div>

                        <div class="input-wrapper col-6">
                            <label class="label" for="text4b">{{__('bahasa.qty')}}</label>
                            <input type="number" class="form-control" id="qty_barang" name="qty_barang">
                            <i class="clear-input">
                                <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                            </i>
                        </div>
                    </div>
                </div>

                
                <div class="form-group boxed">
                    <div class="input-wrapper">
                        <label class="label" for="text4b">{{__('bahasa.harga_satuan')}}</label>
                        <input type="number" class="form-control" id="harga_barang" name="harga_barang">
                        <i class="clear-input">
                            <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                        </i>
                    </div>
                </div>

                <button onclick="processToCart()" class="btn btn-primary btn-block">{{__('bahasa.beli')}}</button>

            </div>
        </div>
    </div>

    <div class="section tab-content mt-2 mb-1">
        @if(count($produk) > 0)
            <div class="section-title">{{__('bahasa.notif_atau_pilih_barang_dibawah')}}</div>
        @endif
        <div class="tab-pane fade active show" id="waiting" role="tabpanel">
            <div class="row">
                @php $kategori = ''; @endphp
                
                @foreach($produk as $row)
                @if($row->id_kategori_produk != $kategori)
                <div class="col-12">
                    <p class="fw-bold">{{$row->nama_kategori}}</p>
                </div>
                @php $kategori = $row->id_kategori_produk; @endphp
                @endif
                <div class="col-6 mb-2">
                    <div class="bill-box">
                        <div class="img-wrapper">
                            @php
                            $img = explode(';', $row->gambar)
                            @endphp
                            @if($img[0] != '')
                            <img src="https://satutoko.id/asset/foto_produk/{{$img[0]}}" alt="img" class="image-block imaged w48">
                            @else
                            <img src="https://satutoko.id/asset/foto_statis/no-image.jpg" alt="img" class="image-block imaged w48">
                            @endif
                        </div>
                        <p>{{$row->nama_produk}}</p>
                        <div class="price" style="font-size:1rem">Rp. {{number_format($row->harga_konsumen)}}</div>
                        
                        <a href="#" onclick="tambah_cart('{{$row->id_reseller}}', '{{Request::segment(2)}}', '{{$row->id_produk}}', '{{$row->nama_produk}}', '{{$row->harga_konsumen}}', '{{$row->satuan}}')" class="btn btn-primary btn-block btn-sm"><ion-icon name="cart-outline"></ion-icon></a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

<script>
    var arr_cart = []

    $( document ).ready(function() {
        view_cart()
    });

    function processToCart() {
        var id_reseller     = $('#id_reseller').val()
        var type_reseller   = $('#type_store').val()
        var id_produk       = ''
        var nama_produk     = $('#nama_barang').val()
        var harga_produk    = $('#harga_barang').val()
        var satuan          = $('#satuan_barang').val()
        var qty             = $('#qty_barang').val()
        tambah_cart(id_reseller, type_reseller, id_produk, nama_produk, harga_produk, satuan, qty)

        $('#nama_barang').val('')
        $('#harga_barang').val('')
        $('#satuan_barang').val('')
        $('#qty_barang').val('')
    }


    function tambah_cart(id_reseller, type_reseller, id_produk, nama_produk, harga_produk, satuan, qty = '1') {
        console.log(id_produk)
        // loaderShow()
        $.ajax({
            url: "{{route('add_cart_shop')}}",
            type: "POST",   
            dataType:"JSON",
            data: {id_reseller, id_produk, nama_produk, harga_produk, satuan, qty, type_reseller},
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') 
            },
            success: function(response) {
                console.log(response)
                $('#total_cart').html(response.keranjang.length)
                // loaderHide()
                notif('bg-success', 'Berhasil Masuk Keranjang')
            },
            error: function(err) {
                console.log(err);
                notif('bg-danger', err.responseJSON)
                // loaderHide()
                // $('#content-order').html(err)
            }
        });
        loaderHide()
    }

    function view_cart() {
        // loaderShow()
        $.ajax({
            url: "{{route('view_cart_shop')}}",
            type: "POST",   
            dataType:"JSON",
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') 
            },
            success: function(response) {
                console.log(response)
                $('#total_cart').html(response.keranjang.length)
                // loaderHide()
            },
            error: function(err) {
                // console.log(err);
                // $('#content-order').html(err)
            }
        });
    }

    
</script>
@endsection