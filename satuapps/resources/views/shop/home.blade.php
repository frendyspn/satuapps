@extends('layout.template')

@section('title', $title)



@section('content')



    <style>
    .map {
       width: 300px;
    }
    </style>



<script>
    var posisi = 'depan';
    var longlat = ''

    $( document ).ready(function() {
        loaderShow()
        $('#latitude').change(function(){
            console.log('OKE 2')
            longlat = $('#latitude').val()
            $.get({ url: `https://maps.googleapis.com/maps/api/geocode/json?latlng=`+longlat+`&sensor=false&key=AIzaSyAjruIgxJm6OS9ewzXUJ17CR7DMPcozpfo`, success(data) {
                console.log(data.results[0].formatted_address);
                $('#alamat_antar').html(data.results[0].formatted_address)
                getStore(longlat)
            }});
        });

    });
</script>





    <!-- App Capsule -->
    <div id="appCapsule" class="full-height">

        <div class="section inset mt-2">
            <div class="accordion" id="accordionExample1">
                <form action="{{route('put_order_ride')}}" method="post">
                    @csrf
                <div class="accordion-item">
                    <h2 class="accordion-header">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accordion01" aria-expanded="false" onclick="initMapAmbil()">
                            <span id="alamat_antar">{{__('bahasa.antar_ke')}}....</span>
                        </button>
                    </h2>
                    <div id="accordion01" class="accordion-collapse collapse" data-bs-parent="#accordionExample1" style="">
                        <div class="accordion-body">
                            
                            <div class="form-group basic row">
                                <div class="input-wrapper col-12">
                                    <label class="label" for="text4">{{__('bahasa.Alamat')}} <span class="text-danger">*{{__('bahasa.harus_diisi')}}</span></label>
                                    <input type="text" class="form-control" id="location-input" name="alamat_antar" placeholder="Alamat" required>
                                    <i class="clear-input">
                                        <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                                    </i>
                                </div>
                            </div>

                            <input type="hidden" class="form-control" id="locality-input" name="kota_antar" placeholder="Kota">
                            <input type="hidden" class="form-control" id="administrative_area_level_1-input" name="provinsi_antar" placeholder="Provinsi">
                            <input type="hidden" class="form-control" id="postal_code-input" name="kode_pos_antar" placeholder="Kode Pos">
                            
                            <input type="hidden" class="form-control" id="catatan_antar" name="catatan_antar" placeholder="Catatan">
            
                            <input type="hidden" class="form-control" id="country-input" name="negara_antar" placeholder="Alamat">

                            <input type="hidden" class="form-control" id="long_lat-input" name="titik_antar" placeholder="Long Lat" onchange="drawMap()">
                            
                            <input type="hidden" class="form-control" id="alamat_long_antar" name="alamat_long_antar" placeholder="Long Lat">

                            <div class="map" id="gmp-map"></div>

                        </div>
                    </div>
                </div>
                
                </form>
            </div>

        </div>

        <div class="section tab-content mt-2 mb-1">

            <!-- <button class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#ModalInputToko" onclick="initAutocomplete()">Input Toko Manual</button> -->
            <a href="{{route('input_store')}}" class="btn btn-primary btn-sm">{{__('bahasa.input_toko_manual')}}</a>
        </div>

        <div class="section tab-content mt-2 mb-1">

            <!-- waiting tab -->
            <div class="tab-pane fade active show" role="tabpanel">
                <div class="row" >
                    @foreach($toko_input as $row)
                    <div class="col-6 mb-2">
                        <div class="bill-box">
                            <div class="price" style="font-size:1rem">{{$row->nama_toko}}</div>
                            <p><ion-icon name="location-outline"></ion-icon>{{$row->alamat_toko}}</p>
                            <a href="{{url('detail_store/addon/'.base64_encode($row->id))}}" class="btn btn-primary btn-block btn-sm">{{__('bahasa.btn_beli_disini')}}</a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <!-- * waiting tab -->
        </div>


        <div class="section tab-content mt-2 mb-1">

            <!-- waiting tab -->
            <div class="tab-pane fade active show" id="waiting" role="tabpanel">
                <div class="row" id="list_of_store">
                    
                </div>
            </div>
            <!-- * waiting tab -->
        </div>
        

    </div>

    
        <div class="modal fade modalbox" id="ModalInputToko" tabindex="-1" aria-hidden="true" style="display: none;">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{__('bahasa.input_toko')}}</h5>
                        <a href="#" data-bs-dismiss="modal">{{__('bahasa.title_button_tutup')}}</a>
                    </div>
                    <div class="modal-body">
                        <input id="pac-input" class="controls" type="text" placeholder="Search Box" />
                        <div id="map"></div>
                    </div>
                </div>
            </div>
        </div>
    
<script>
    function getStore(longlat){
        console.log('cek '+longlat)
        $.ajax({
            url: "{{route('get_store_longlat')}}",
            type: "POST",   
            dataType:"HTML",
            data: {longlat},
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') 
            },
            success: function(response) {
                $('#list_of_store').html(response)
                loaderHide()
            },
            error: function(err) {
                // console.log(err);
                // $('#content-order').html(err)
            }    
        });
    }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjruIgxJm6OS9ewzXUJ17CR7DMPcozpfo&libraries=places&v=weekly" defer ></script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjruIgxJm6OS9ewzXUJ17CR7DMPcozpfo&libraries=places&callback=getMap&solution_channel=GMP_QB_addressselection_v1_cAB" async defer></script> -->
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjruIgxJm6OS9ewzXUJ17CR7DMPcozpfo&libraries=places&solution_channel=GMP_QB_addressselection_v1_cAB&v=weekly" async defer></script> -->
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjruIgxJm6OS9ewzXUJ17CR7DMPcozpfo&callback=getMap&v=weekly" defer></script> -->


<script>
    function initAutocomplete() {
  const map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: -33.8688, lng: 151.2195 },
    zoom: 13,
    mapTypeId: "roadmap",
  });
  // Create the search box and link it to the UI element.
  const input = document.getElementById("pac-input");
  const searchBox = new google.maps.places.SearchBox(input);

  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
  // Bias the SearchBox results towards current map's viewport.
  map.addListener("bounds_changed", () => {
    searchBox.setBounds(map.getBounds());
  });

  let markers = [];

  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
  searchBox.addListener("places_changed", () => {
    const places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }

    // Clear out the old markers.
    markers.forEach((marker) => {
      marker.setMap(null);
    });
    markers = [];

    // For each place, get the icon, name and location.
    const bounds = new google.maps.LatLngBounds();

    places.forEach((place) => {
      if (!place.geometry || !place.geometry.location) {
        console.log("Returned place contains no geometry");
        return;
      }

      const icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25),
      };

      // Create a marker for each place.
      markers.push(
        new google.maps.Marker({
          map,
          icon,
          title: place.name,
          position: place.geometry.location,
        }),
      );
      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
    map.fitBounds(bounds);
  });
}

</script>


@endsection


@section('js')

@endsection