
<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\HistoryController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\BalanceController;
use App\Http\Controllers\SendController;
use App\Http\Controllers\RideController;
use App\Http\Controllers\FoodController;
use App\Http\Controllers\ShopController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', 'App\Http\Controllers\HomeController@index');

Route::get('/', [HomeController::class, 'index'])->name('home');

Route::get('/login', [LoginController::class, 'index'])->name('login');
Route::get('/logout', [LoginController::class, 'logout'])->name('logout');
Route::post('/proses_login', [LoginController::class, 'prosesLogin'])->name('proses_login');
Route::get('/otp', [LoginController::class, 'otpLogin'])->name('otp');
Route::post('/proses_otp', [LoginController::class, 'prosesOtp'])->name('proses_otp');
Route::get('/device_token', [LoginController::class, 'GetDeviceToken'])->name('device_token');

Route::post('/get_provinsi', [Controller::class, 'GetProvinsi'])->name('get_provinsi');
Route::post('/get_city', [Controller::class, 'GetKota'])->name('get_city');
Route::post('/get_district', [Controller::class, 'GetKecamatan'])->name('get_district');

Route::get('/send', [SendController::class, 'index'])->name('send');
Route::post('/get_ongkir', [SendController::class, 'getOngkir'])->name('get_ongkir');
Route::post('/put_order', [SendController::class, 'putOrder'])->name('put_order');
Route::get('/detail_order/{id}', [SendController::class, 'detailOrder'])->name('detail_order');


Route::get('/ride', [RideController::class, 'index'])->name('ride');
Route::post('/get_ongkos', [RideController::class, 'getOngkos'])->name('get_ongkos');
Route::post('/put_order_ride', [RideController::class, 'putOrder'])->name('put_order_ride');
Route::get('/detail_ride/{id}', [RideController::class, 'detailOrder'])->name('detail_ride');


Route::get('/food', [FoodController::class, 'index'])->name('food');
Route::post('/get_ongkir_food', [FoodController::class, 'getOngkos'])->name('get_ongkir_food');
Route::post('/put_order_food', [FoodController::class, 'putOrder'])->name('put_order_food');
Route::get('/detail_food/{id}', [FoodController::class, 'detailOrder'])->name('detail_food');
Route::post('/get_resto_longlat', [FoodController::class, 'getRestoLongLat'])->name('get_resto_longlat');
Route::get('/get_resto_menu/{id}', [FoodController::class, 'getRestoMenu'])->name('get_resto_menu');
Route::post('/add_cart_food', [FoodController::class, 'addCartFood'])->name('add_cart_food');
Route::post('/view_cart_food', [FoodController::class, 'viewCartFood'])->name('view_cart_food');
Route::get('/cart_food', [FoodController::class, 'indexCart'])->name('cart_food');
Route::post('/view_checkout_food', [FoodController::class, 'getCart'])->name('view_checkout_food');
Route::post('/update_checkout_food', [FoodController::class, 'updateCart'])->name('update_checkout_food');
Route::get('/hapus_cart_food', [FoodController::class, 'hapusCart'])->name('hapus_cart_food');


Route::get('/shop', [ShopController::class, 'index'])->name('shop');
Route::post('/get_store_longlat', [ShopController::class, 'getStoreLongLat'])->name('get_store_longlat');
Route::get('/input_store', [ShopController::class, 'inputStore'])->name('input_store');
Route::post('/simpan_toko', [ShopController::class, 'addStore'])->name('simpan_toko');
Route::get('/detail_store/{type}/{id}', [ShopController::class, 'getProdukStore'])->name('detail_store');
Route::post('/add_cart_shop', [ShopController::class, 'addCartShop'])->name('add_cart_shop');
Route::post('/view_cart_shop', [ShopController::class, 'viewCartShop'])->name('view_cart_shop');
Route::get('/cart_shop', [ShopController::class, 'indexCart'])->name('cart_shop');
Route::post('/view_checkout_shop', [ShopController::class, 'getCart'])->name('view_checkout_shop');
Route::post('/update_checkout_shop', [ShopController::class, 'updateCart'])->name('update_checkout_shop');
Route::get('/hapus_cart_shop', [ShopController::class, 'hapusCart'])->name('hapus_cart_shop');
Route::post('/put_order_shop', [ShopController::class, 'putOrder'])->name('put_order_shop');
Route::get('/detail_shop/{id}', [ShopController::class, 'detailOrder'])->name('detail_shop');

Route::get('/history', [HistoryController::class, 'index'])->name('history');
Route::post('/get_history', [HistoryController::class, 'searchData'])->name('get_history');



Route::get('/balance', [BalanceController::class, 'index'])->name('balance');
Route::post('/proses_deposit', [BalanceController::class, 'ProsesDeposit'])->name('proses_deposit');
Route::get('/konfirmasi_deposit/{id}', [BalanceController::class, 'KonfirmasiDeposit'])->name('konfirmasi_deposit');
Route::post('/upload_bukti_transfer', [BalanceController::class, 'UploadBuktiTransfer'])->name('upload_bukti_transfer');
Route::post('/proses_transfer', [BalanceController::class, 'ProsesTransfer'])->name('proses_transfer');
Route::post('/proses_withdraw', [BalanceController::class, 'ProsesWithdraw'])->name('proses_withdraw');


Route::post('/cek_no_hp', [Controller::class, 'getDataHp'])->name('cek_no_hp');


Route::get('/change/{locale}', function (string $locale) {
    session()->put('locale', $locale);
 
    return redirect('home');

});













Route::get('/registrasi', [LoginController::class, 'registrasi'])->name('registrasi');
Route::post('/registrasi_cek_no_hp', [LoginController::class, 'registrasiCekHP'])->name('registrasi_cek_no_hp');
Route::get('/registrasi_cek_no_hp_confirm', [LoginController::class, 'registrasiCekHPGet'])->name('registrasi_cek_no_hp_confirm');

Route::get('/otp_registrasi', [LoginController::class, 'otpRegistrasi'])->name('otp_registrasi');
Route::post('/proses_otp_registrasi', [LoginController::class, 'prosesOtpRegistrasi'])->name('proses_otp_registrasi');
Route::get('/confirm_registrasi', [LoginController::class, 'confirmRegistrasi'])->name('confirm_registrasi');
Route::post('/registrasi_proses_konfirmasi', [LoginController::class, 'confirmRegistrasiProses'])->name('registrasi_proses_konfirmasi');
Route::post('/registrasi_proses_data_diri', [LoginController::class, 'dataDiriRegistrasiProses'])->name('registrasi_proses_data_diri');

Route::get('/save-token/{device_token}/{latitude}', [LoginController::class, 'saveToken'])->name('save-token');

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::post('/get_saldo', [HomeController::class, 'getSaldo'])->name('get_saldo');
Route::post('/get_order', [HomeController::class, 'getOrder'])->name('get_order');
Route::post('/ambil_order', [HomeController::class, 'ambilOrder'])->name('ambil_order');
Route::post('/ambil_barang', [HomeController::class, 'ambilBarang'])->name('ambil_barang');
Route::post('/serah_barang', [HomeController::class, 'serahBarang'])->name('serah_barang');






Route::get('/test', [Controller::class, 'api_token']);

