<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Session;
use DB;
use App;
class HomeController extends Controller
{
    public function index(Request $req)
    {
        App::setLocale(session()->get('locale'));
        if (!cek_login()){
            return redirect('/login');
        }
        
        $data['title'] = 'Home';
        $data['header'] = 'default';
        $data['menu'] = '';
        $data['tglAwal'] = date('Y-m-01');
        $data['tglAkhir'] = date('Y-m-d');
        
        return view('home',$data);
    }


    public function searchData(Request $req)
    {
        App::setLocale(session()->get('locale'));
        $tglAwal = date('Y-m-d 00:00:01', strtotime($req->tgl_awal));
        $tglAkhir = date('Y-m-d 23:59:59', strtotime($req->tgl_akhir));

        $getData = DB::table('kurir_order')->select(DB::raw('count(id) as total'))->whereBetween('tanggal_order', [$tglAwal, $tglAkhir])->first();
        $getOrder_layanan = DB::table('kurir_order')->select('jenis_layanan', DB::raw('count(id) as total'))->where('status','!=','CANCEL')->whereBetween('tanggal_order', [$tglAwal, $tglAkhir])->groupBy('jenis_layanan')->get();
        $getOrder_cancel = DB::table('kurir_order')->select(DB::raw('count(id) as total'))->where('status','CANCEL')->whereBetween('tanggal_order', [$tglAwal, $tglAkhir])->first();

        $getTotal_komisi = DB::table('rb_pendapatan_kurir')->select(DB::raw('sum(nominal) as total'))->where('rb_pendapatan_kurir.id_konsumen','!=','0')->where('transaksi','debit')->whereNotIn('akun', ['deposit','transfer','withdraw'])->whereBetween('waktu_pendapatan', [$tglAwal, $tglAkhir])->first();
        $getRekap_komisi = DB::table('rb_pendapatan_kurir')->select('keterangan',DB::raw('sum(nominal) as total'))->where('rb_pendapatan_kurir.id_konsumen','!=','0')->where('transaksi','debit')->whereNotIn('akun', ['deposit','transfer','withdraw'])->whereBetween('waktu_pendapatan', [$tglAwal, $tglAkhir])->groupBy('keterangan')->orderBy('keterangan','desc')->get();

        $tglShow = '';

        echo '<div class="row mt-2 mb-2">';

        if ($getData->total > 0) {
            echo '
            
                <div class="col-6 mb-1">
                    <div class="stat-box">
                        <div class="title">Total Order</div>
                        <div class="value text-success">'.number_format($getData->total).'</div>
                    </div>
                </div>
            
            ';
        }
        foreach ($getOrder_layanan as $row){
            echo '
            <div class="col-6 mb-1">
                <div class="stat-box">
                    <div class="title">
                    '.$row->jenis_layanan.'
                    </div>
                    <div class="value">'.number_format($row->total).'</div>
                </div>
            </div>
            ';
        }

        if ($getOrder_cancel->total > 0) {
            echo '
            
                <div class="col-6 mb-1">
                    <div class="stat-box">
                        <div class="title">Total Order Cancel</div>
                        <div class="value text-danger">'.number_format($getOrder_cancel->total).'</div>
                    </div>
                </div>
            
            ';
        }

        echo '</div>';

        echo '<div class="row mt-2 mb-2">';

        if ($getTotal_komisi->total > 0) {
            echo '
            
                <div class="col-6 mb-1">
                    <div class="stat-box">
                        <div class="title">Total Komisi</div>
                        <div class="value text-success">'.__('bahasa.kurs').number_format($getTotal_komisi->total).'</div>
                    </div>
                </div>
            
            ';
        }
    
        foreach ($getRekap_komisi as $row){
            echo '
            <div class="col-6 mb-1">
                <div class="stat-box">
                    <div class="title">';
                    if ($row->keterangan == '') {
                        echo '-';
                    } else {
                        echo $row->keterangan;
                    }
                        
                    echo '</div>
                    <div class="value">'.__('bahasa.kurs').number_format($row->total).'</div>
                </div>
            </div>
            ';
        }
            
        echo '</div>
        ';

        
        
    }
    
}