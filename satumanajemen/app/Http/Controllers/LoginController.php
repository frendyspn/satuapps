<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Http;
use Session;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;

use App;
use DB;

class LoginController extends Controller
{

    public function index()
    {
        App::setLocale(session()->get('locale'));
        if (cek_login()){
            // return redirect('/home');
        }
        
        $data['title'] = 'Masuk';
        // $data['header'] = 'goback';

        return view('login',$data);
    }


    public function prosesLogin(Request $req)
    {
        $validated = $req->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

        $username = $req->username;
        $password = hash("sha512", md5($req->password));

        $getUser = DB::table('users')->where('username', $username)->where('password', $password)->first();

        if ($getUser) {
            Session::put('nama_lengkap', $getUser->nama_lengkap);
            Session::put('username', $getUser->username);
            Session::put('email', $getUser->email);
            Session::put('no_hp', $getUser->no_telp);
            Session::put('level', $getUser->level);
            return redirect('home');
        } else {
            return redirect('/login')->with(['error_msg' => 'Username / Password Tidak Cocok']);
        }
        

    }

    public function logout(Request $req){
        Session::flush();
        $req->session()->regenerateToken();
        return redirect('login');
    }
    
}
