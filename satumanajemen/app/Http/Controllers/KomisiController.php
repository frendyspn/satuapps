<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Session;
use DB;
use App;
class KomisiController extends Controller
{
    public function index(Request $req)
    {
        App::setLocale(session()->get('locale'));
        if (!cek_login()){
            return redirect('/login');
        }
        
        $data['title'] = 'Komisi';
        $data['header'] = 'goback';
        $data['menu'] = '';
        $data['tglAwal'] = date('Y-m-01');
        $data['tglAkhir'] = date('Y-m-d');
        
        return view('komisi',$data);
    }


    public function searchData(Request $req)
    {
        App::setLocale(session()->get('locale'));
        $tglAwal = date('Y-m-d 00:00:01', strtotime($req->tgl_awal));
        $tglAkhir = date('Y-m-d 23:59:59', strtotime($req->tgl_akhir));

        $getData = DB::table('rb_pendapatan_kurir')->select('rb_pendapatan_kurir.*','rb_konsumen.nama_lengkap')->leftJoin('rb_konsumen', 'rb_konsumen.id_konsumen', 'rb_pendapatan_kurir.id_konsumen')->where('rb_pendapatan_kurir.id_konsumen','!=','0')->where('transaksi','debit')->whereNotIn('akun', ['deposit','transfer','withdraw'])->whereBetween('waktu_pendapatan', [$tglAwal, $tglAkhir])->orderBy('waktu_pendapatan','desc')->get();
        $getTotal_komisi = DB::table('rb_pendapatan_kurir')->select(DB::raw('sum(nominal) as total'))->where('rb_pendapatan_kurir.id_konsumen','!=','0')->where('transaksi','debit')->whereNotIn('akun', ['deposit','transfer','withdraw'])->whereBetween('waktu_pendapatan', [$tglAwal, $tglAkhir])->first();
        $getRekap_komisi = DB::table('rb_pendapatan_kurir')->select('keterangan',DB::raw('sum(nominal) as total'))->where('rb_pendapatan_kurir.id_konsumen','!=','0')->where('transaksi','debit')->whereNotIn('akun', ['deposit','transfer','withdraw'])->whereBetween('waktu_pendapatan', [$tglAwal, $tglAkhir])->groupBy('keterangan')->orderBy('keterangan','desc')->get();

        $tglShow = '';

        echo '
        <div class="row mt-2 mb-2">
            <div class="col-6 mb-1">
                <div class="stat-box">
                    <div class="title">Total Komisi</div>
                    <div class="value text-success">'.__('bahasa.kurs').number_format($getTotal_komisi->total).'</div>
                </div>
            </div>';

            foreach ($getRekap_komisi as $row){
                echo '
                <div class="col-6 mb-1">
                    <div class="stat-box">
                        <div class="title">';
                        if ($row->keterangan == '') {
                            echo '-';
                        } else {
                            echo $row->keterangan;
                        }
                        
                        echo '</div>
                        <div class="value">'.__('bahasa.kurs').number_format($row->total).'</div>
                    </div>
                </div>
                ';
            }
            
        echo '</div>
        ';

        if (count($getData) > 0) {
            echo '<div class="transactions">';
            foreach ($getData as $row) {
                if (date('Ymd', strtotime($row->waktu_pendapatan)) != $tglShow) {
                    echo '<div class="section-title">'.date('d M Y', strtotime($row->waktu_pendapatan)).'</div>';
                    $tglShow = date('Ymd', strtotime($row->waktu_pendapatan));
                }
                echo '
                <a href="#" class="item">
                    <div class="detail">
                        <div>
                            <strong>'.$row->keterangan.' Untuk '.$row->nama_lengkap.'</strong>
                            <p>'.date('d M Y', strtotime($row->waktu_pendapatan)).'</p>
                        </div>
                    </div>
                    <div class="right">
                        <div class="price"> '.__('bahasa.kurs').number_format($row->nominal).'</div>
                    </div>
                </a>
                ';
            }
            echo '</div>';
        } else {
            echo '<div class="transactions">
            <a href="#" class="item">
                <div class="detail">
                    <div>
                        <strong class="text-danger">Belum Ada Transaksi</strong>
                    </div>
                </div>
            </a>
            </div>
            ';
        }
        
    }
    
}