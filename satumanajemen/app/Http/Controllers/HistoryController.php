<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Session;
use DB;
use App;
class HistoryController extends Controller
{
    public function index(Request $req)
    {
        App::setLocale(session()->get('locale'));
        if (!cek_login()){
            return redirect('/login');
        }
        
        $data['title'] = 'History';
        $data['header'] = 'goback';
        $data['menu'] = '';
        $data['tglAwal'] = date('Y-m-01');
        $data['tglAkhir'] = date('Y-m-d');
        // $data['pendapatanHariIni'] = DB::table('rb_pendapatan_kurir')->select(DB::raw('sum(nominal) as saldo'))->where('id_konsumen', $this->getDataToken(Session::get('token'))->id_konsumen)->where('transaksi', 'debit')->whereBetween('waktu_pendapatan', [date('Y-m-d 00:00:01'), date('Y-m-d 23:59:59')])->first()->saldo;
        
        return view('history',$data);
    }


    public function searchData(Request $req)
    {
        App::setLocale(session()->get('locale'));
        $tglAwal = date('Y-m-d 00:00:01', strtotime($req->tgl_awal));
        $tglAkhir = date('Y-m-d 23:59:59', strtotime($req->tgl_akhir));
        $layanan = $req->layanan;

        
        $getData = DB::table('kurir_order')->whereBetween('tanggal_order', [$tglAwal, $tglAkhir]);
        if ($layanan) {
            $getData = $getData->where('jenis_layanan', $layanan);
        }
        $getData = $getData->orderBy('tanggal_order','desc')->get();

        echo '
        <div class="section mt-2">
            <div class="card">
                <ul class="listview flush transparent no-line image-listview detailed-list mt-1 mb-1">';

                if (count($getData) <= 0) {
                    echo '<span class="badge bg-danger align-center">'.__('bahasa.notif_belum_ada_order').'</span>';
                }
        foreach ($getData as $val) {
            $responseJemput = Http::attach('token',Session::get('username')) 
            ->withHeaders([ 
                'Authorization'=> api_token(),
            ]) 
            ->post('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$val->titik_jemput.'&key=AIzaSyA3LBjGfAARNWUtl7mVk2APMsi4w4Y9Nx8');

            $dtJemput = json_decode($responseJemput->body());

            $responseAntar = Http::attach('token',Session::get('username')) 
            ->withHeaders([ 
                'Authorization'=> api_token(),
            ]) 
            ->post('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$val->titik_antar.'&key=AIzaSyA3LBjGfAARNWUtl7mVk2APMsi4w4Y9Nx8');

            $dtAntar = json_decode($responseAntar->body());

            if ($val->status == 'CANCEL') {
                $status = '<span class="badge bg-danger">'.$val->status.'</span>';
            } else if ($val->status == 'FINISH') {
                $status = '<span class="badge bg-primary">'.$val->status.'</span>';
            } else {
                $status = '<span class="badge bg-warning">'.$val->status.'</span>';
            }

            if ($val->metode_pembayaran == 'CASH') {
                $pembayaran = '<small class="badge bg-danger">'.__('bahasa.tunai').'</small>';
            } else {
                $pembayaran = '<small class="badge bg-warning">'.__('bahasa.nontunai').'</small>';
            }
            

            echo '
            <li>
                <a href="'.url('detail/'.base64_encode($val->id)).'" target="_blank" class="item">
                    <div class="in">
                        <div>
                            <strong>'.date('d M Y', strtotime($val->tanggal_order)).$status.'</strong>
                            <div class="text-small text-secondary"><span class="fw-bold">'.__('bahasa.dari').': </span>'.$dtJemput->plus_code->compound_code.'</div>
                            <div class="text-small text-secondary"><span class="fw-bold">'.__('bahasa.ke').': </span>'.$dtAntar->plus_code->compound_code.'</div>
                            <div class="text-small text-secondary"><span class="fw-bold">Layanan : </span>'.$val->jenis_layanan.'</div>
                        </div>
                        <div class="text-end">
                            <strong>'.__('bahasa.kurs').number_format($val->tarif).'</strong>
                            <div>'.$pembayaran.'</div>
                            <div>&nbsp;</div>
                            <div>&nbsp;</div>
                            <div>&nbsp;</div>
                        </div>
                    </div>
                </a>
            </li>
            ';
        }
        echo '
                </div>
            </div>
        </div>';

    }


    public function detailData($id)
    {
        App::setLocale(session()->get('locale'));

        if (!cek_login()){
            return redirect('/login');
        }

        $id = base64_decode($id);

        $data['order'] = DB::table('kurir_order as a')->select('a.id', 'a.titik_jemput', 'a.titik_antar', 'a.service', 'a.id_penjualan', 'a.status', 'a.source', 'a.tarif', 'a.id_sopir', 'a.kode_order', 'c.nama_reseller as nama_toko', 'c.alamat_lengkap as alamat_toko', 'd.nama_lengkap as nama_konsumen', 'd.alamat_lengkap as alamat_konsumen', 'a.pemberi_barang', 'a.alamat_jemput', 'a.alamat_antar', 'a.penerima_barang','a.jenis_layanan', DB::raw('(select sum(jumlah*(harga_jual-diskon)) from rb_penjualan_detail where id_penjualan = a.id_penjualan) as total_belanja'))->leftJoin('rb_penjualan as b', 'b.id_penjualan', 'a.id_penjualan')->leftJoin('rb_reseller as c', 'c.id_reseller', 'b.id_penjual')->leftJoin('rb_konsumen as d', 'd.id_konsumen', 'b.id_pembeli')->where('a.id', $id)->first();
        $data['timeline'] = DB::table('kurir_order_log')->where('id_order', $id)->get();
        $data['warna_timeline'] = ['bg-primary','bg-info','bg-danger','bg-warning'];
        $data['kurir'] = DB::table('rb_sopir as a')->select('a.*', 'b.nama_lengkap')->leftJoin('rb_konsumen as b', 'b.id_konsumen', 'a.id_konsumen')->where('a.id_sopir', $data['order']->id_sopir)->first();
        
        $data['title'] = 'Detail '.$data['order']->jenis_layanan.' - '.$data['order']->kode_order;
        $data['header'] = 'goback';
        $data['menu'] = '';

        if ($data['order']->jenis_layanan == 'RIDE') {
            return view('detail.ride',$data);
        } elseif ($data['order']->jenis_layanan == 'SHOP') {
            $data['belanja'] = DB::table('rb_penjualan_shop')->where('id_kurir_order', $id)->get();

            return view('detail.shop',$data);
        } elseif ($data['order']->jenis_layanan == 'SEND') {
            return view('detail.send',$data);
        } elseif ($data['order']->jenis_layanan == 'FOOD') {
            
            return view('detail.food',$data);
        } else {
            return view('detail.ride',$data);
        }
        
    }


}