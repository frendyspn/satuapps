<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Session;
use DB;
use App;
class BannerController extends Controller
{
    public function index(Request $req)
    {
        App::setLocale(session()->get('locale'));
        if (!cek_login()){
            return redirect('/login');
        }
        
        $data['title'] = 'Banner';
        $data['header'] = 'goback';
        $data['menu'] = '';
        $data['dtBanner'] = DB::table('banner_apps')->whereIn('posisi', ['kurir','user'])->where('is_aktif', '1')->where('is_deleted', '0')->get();
        $data['dtBannerNonaktif'] = DB::table('banner_apps')->whereIn('posisi', ['kurir','user'])->where('is_aktif', '0')->where('is_deleted', '0')->get();

        return view('banner',$data);
    }


    public function saveData(Request $req)
    {
        $id_banner = $req->id_banner;
        $judul_banner = $req->judul_banner;
        $keterangan_banner = $req->keterangan_banner;
        $posisi_banner = $req->posisi_banner;
        $status_banner = $req->status_banner;
        $url_banner = $req->url_banner;
        

        if ($req->gambar_banner) {
            $banner=$_FILES['gambar_banner']['name']; 
            $file=explode('/',$_FILES['gambar_banner']['type']);
            $bannerexptype=$file[1];
            $date = time();
            $rand=rand(10000,99999);
            $encname=$date.$rand;
            $gambar_banner=$encname.'.'.$bannerexptype;
            $bannerpath=public_path() . '/uploads/banner/'.$gambar_banner;
            move_uploaded_file($_FILES["gambar_banner"]["tmp_name"],$bannerpath);

            $dtBanner['gambar'] = $gambar_banner;
        }
        

        $dtBanner['judul'] = $judul_banner;
        $dtBanner['keterangan'] = $keterangan_banner;
        $dtBanner['posisi'] = $posisi_banner;
        $dtBanner['tgl_posting'] = date('Y-m-d H:i:s');
        $dtBanner['is_aktif'] = $status_banner;
        $dtBanner['url'] = $url_banner;

        if ($id_banner == '') {
            $put = DB::table('banner_apps')->insert($dtBanner);
        } else {
            $put = DB::table('banner_apps')->where('id_banner', $id_banner)->update($dtBanner);
        }
        
        if ($put) {
            return redirect('banner')->with('sukses', 'Berhasil Menambahkan Banner');
        } else {
            return redirect('banner')->with('error_msg', 'Banner Gagal Disimpan');
        }
        
    }

    public function deleteData($id)
    {
        $dtBanner['is_deleted'] = '1';
        $put = DB::table('banner_apps')->where('id_banner', $id)->update($dtBanner);
        
        if ($put) {
            return redirect('banner')->with('sukses', 'Berhasil Menambahkan Dihapus');
        } else {
            return redirect('banner')->with('error_msg', 'Banner Gagal Dihapus');
        };
    }
}