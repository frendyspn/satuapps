<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Session;
use DB;
use App;
class SettingController extends Controller
{
    public function index(Request $req)
    {
        App::setLocale(session()->get('locale'));
        if (!cek_login()){
            return redirect('/login');
        }
        
        $data['title'] = 'Setting';
        $data['header'] = 'goback';
        $data['menu'] = '';
        $data['ongkir_awal_kurir'] = $this->getConfig('ongkir_awal_kurir');
        $data['ongkir_per_km'] = $this->getConfig('ongkir_per_km');
        $data['max_jarak_km'] = $this->getConfig('max_jarak_km');
        $data['fee_kurir_total'] = $this->getConfig('fee_kurir_total');
        $data['fee_kurir_ref'] = $this->getConfig('fee_kurir_ref');
        $data['fee_kurir_sistem'] = $this->getConfig('fee_kurir_sistem');
        $data['fee_kurir_agen'] = $this->getConfig('fee_kurir_agen');
        $data['fee_kurir_koordinator_kota'] = $this->getConfig('fee_kurir_koordinator_kota');
        $data['fee_kurir_koordinator_kecamatan'] = $this->getConfig('fee_kurir_koordinator_kecamatan');
        $data['fee_kurir_kas_cabang'] = $this->getConfig('fee_kurir_kas_cabang');
        $data['fee_kurir_lainnya'] = $this->getConfig('fee_kurir_lainnya');
        
        return view('setting',$data);
    }

    public function saveData(Request $req)
    {
        DB::table('rb_config')->where('field', 'ongkir_awal_kurir')->update(['value'=>$req->ongkir_awal_kurir]);
        DB::table('rb_config')->where('field', 'ongkir_per_km')->update(['value'=>$req->ongkir_per_km]);
        DB::table('rb_config')->where('field', 'max_jarak_km')->update(['value'=>$req->max_jarak_km]);
        DB::table('rb_config')->where('field', 'fee_kurir_total')->update(['value'=>$req->fee_kurir_total]);
        DB::table('rb_config')->where('field', 'fee_kurir_ref')->update(['value'=>$req->fee_kurir_ref]);
        DB::table('rb_config')->where('field', 'fee_kurir_sistem')->update(['value'=>$req->fee_kurir_sistem]);
        DB::table('rb_config')->where('field', 'fee_kurir_agen')->update(['value'=>$req->fee_kurir_agen]);
        DB::table('rb_config')->where('field', 'fee_kurir_koordinator_kota')->update(['value'=>$req->fee_kurir_koordinator_kota]);
        DB::table('rb_config')->where('field', 'fee_kurir_koordinator_kecamatan')->update(['value'=>$req->fee_kurir_koordinator_kecamatan]);
        DB::table('rb_config')->where('field', 'fee_kurir_kas_cabang')->update(['value'=>$req->fee_kurir_kas_cabang]);
        DB::table('rb_config')->where('field', 'fee_kurir_lainnya')->update(['value'=>$req->fee_kurir_lainnya]);

        return redirect('setting');
    }
}