<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Session;
use DB;
use App;
class FinanceController extends Controller
{
    public function index(Request $req)
    {
        App::setLocale(session()->get('locale'));
        if (!cek_login()){
            return redirect('/login');
        }
        
        $data['title'] = 'Finance';
        $data['header'] = 'goback';
        $data['menu'] = '';
        $data['tglAwal'] = date('Y-m-01');
        $data['tglAkhir'] = date('Y-m-d');
        
        return view('finance.deposit',$data);
    }


    public function getDeposit(Request $req)
    {
        App::setLocale(session()->get('locale'));
        $tglAwal = date('Y-m-d 00:00:01', strtotime($req->tgl_awal));
        $tglAkhir = date('Y-m-d 23:59:59', strtotime($req->tgl_akhir));

        $getData = DB::table('rb_pendapatan_kurir')->select('rb_pendapatan_kurir.*','rb_konsumen.nama_lengkap')->leftJoin('rb_konsumen', 'rb_konsumen.id_konsumen', 'rb_pendapatan_kurir.id_konsumen')->where('status', 'Proses')->whereIn('akun', ['deposit','withdraw'])->whereBetween('waktu_pendapatan', [$tglAwal, $tglAkhir])->orderBy('waktu_pendapatan','desc')->get();

        $tglShow = '';

        if (count($getData) > 0) {
            echo '<div class="transactions">';
            foreach ($getData as $row) {
                if (date('Ymd', strtotime($row->waktu_pendapatan)) != $tglShow) {
                    echo '<div class="section-title">'.date('d M Y', strtotime($row->waktu_pendapatan)).'</div>';
                    $tglShow = date('Ymd', strtotime($row->waktu_pendapatan));
                }
                if ($row->akun == 'deposit') {
                    $ketJS = $row->keterangan;
                } else {
                    $ketJS = '<h5>Harap Transfer Ke<br/>Bank: '.$row->bank_withdraw.'<br/>No Rekening: '.$row->nomor_rekening_withdraw.'<br/>Nama Rekening: '.$row->nama_rekening_withdraw.'<br/>Nominal: '.number_format($row->nominal).'<h5>';
                }
                echo '
                <a href="#" class="item" onclick="viewImage('.$row->id_pendapatan.',\''.$ketJS.'\',\''.$row->akun.'\')">
                    <div class="detail">';
                        if ($row->akun == 'deposit') {
                            echo '<img src="'.$row->keterangan.'" alt="img" class="image-block imaged w48">';
                        }
                        echo '<div>
                            <strong>'.$row->nama_lengkap.' <small class="text-danger">'.$row->akun.'</small></strong>
                            <p>'.date('d M Y', strtotime($row->waktu_pendapatan)).'</p>
                        </div>
                    </div>
                    <div class="right">
                        <div class="price"> '.__('bahasa.kurs').number_format($row->nominal).'</div>
                    </div>
                </a>
                ';
            }
            echo '</div>';
        } else {
            echo '<div class="transactions">
            <a href="#" class="item">
                <div class="detail">
                    <div>
                        <strong class="text-danger">Belum Ada Transaksi</strong>
                    </div>
                </div>
            </a>
            </div>
            ';
        }
    }


    public function konfirmasiDeposit($status, $id)
    {
        if ($status == '1') {
            $dtUpdate['status'] = 'Sukses';
        } else {
            $dtUpdate['status'] = 'Batal';
        }
        
        DB::table('rb_pendapatan_kurir')->where('id_pendapatan', $id)->update($dtUpdate);
        return redirect('finance');
    }



}