<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Session;
use DB;
use App;
class BalanceController extends Controller
{
    public function index(Request $req)
    {
        App::setLocale(session()->get('locale'));

        if (!cek_login()){
            return redirect('/login');
        }

        if ($this->getDataToken(Session::get('token')) == null) {
            return redirect('/home');
        }
        
        $data['title'] = __('bahasa.Saldo');
        $data['header'] = 'goback';
        $data['menu'] = '';
        $data['dtWallet'] = DB::table('rb_pendapatan_kurir')->where('id_konsumen', $this->getDataToken(Session::get('token'))->id_konsumen)->orderBy('waktu_pendapatan', 'desc')->get();

        $debit = DB::table('rb_pendapatan_kurir')->select(DB::raw('sum(nominal) as saldo'))->where('id_konsumen', $this->getDataToken(Session::get('token'))->id_konsumen)->where('transaksi', 'debit')->first();
        $kredit = DB::table('rb_pendapatan_kurir')->select(DB::raw('sum(nominal) as saldo'))->where('id_konsumen', $this->getDataToken(Session::get('token'))->id_konsumen)->where('transaksi', 'kredit')->first();

        $data['saldo'] = $debit->saldo-$kredit->saldo;


        $debitKurir = DB::table('rb_pendapatan_kurir')->select(DB::raw('sum(nominal) as saldo'))->where('id_konsumen', $this->getDataToken(Session::get('token'))->id_konsumen)->where('transaksi', 'debit')->where('akun', 'sopir')->first();
        $kreditKurir = DB::table('rb_pendapatan_kurir')->select(DB::raw('sum(nominal) as saldo'))->where('id_konsumen', $this->getDataToken(Session::get('token'))->id_konsumen)->where('transaksi', 'kredit')->where('akun', 'sopir')->first();

        $data['saldoKurir'] = $debitKurir->saldo-$kreditKurir->saldo;

        $debitKomisi = DB::table('rb_pendapatan_kurir')->select(DB::raw('sum(nominal) as saldo'))->where('id_konsumen', $this->getDataToken(Session::get('token'))->id_konsumen)->where('transaksi', 'debit')->where('akun', '!=', 'sopir')->first();
        $kreditKomisi = DB::table('rb_pendapatan_kurir')->select(DB::raw('sum(nominal) as saldo'))->where('id_konsumen', $this->getDataToken(Session::get('token'))->id_konsumen)->where('transaksi', 'kredit')->where('akun', '!=', 'sopir')->first();

        $data['saldoKomisi'] = $debitKomisi->saldo-$kreditKomisi->saldo;

        return view('balance',$data);
    }

}