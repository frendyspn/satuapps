<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\HistoryController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\BalanceController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\KomisiController;
use App\Http\Controllers\FinanceController;
use App\Http\Controllers\BannerController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', 'App\Http\Controllers\HomeController@index');

Route::get('/', [HomeController::class, 'index'])->name('homey');


Route::get('/login', [LoginController::class, 'index'])->name('login')->middleware('web');
Route::get('/logout', [LoginController::class, 'logout'])->name('logout');
Route::post('/proses_login', [LoginController::class, 'prosesLogin'])->name('proses_login')->middleware('web');





Route::get('/report', [HistoryController::class, 'index'])->name('report');
Route::post('/get_report', [HistoryController::class, 'searchData'])->name('get_report');
Route::get('/detail/{id}', [HistoryController::class, 'detailData'])->name('get_detail');


Route::get('/setting', [SettingController::class, 'index'])->name('setting');
Route::post('/setting_save', [SettingController::class, 'saveData'])->name('setting_save');


Route::get('/komisi', [KomisiController::class, 'index'])->name('komisi');
Route::post('/get_komisi', [KomisiController::class, 'searchData'])->name('get_komisi');


Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::post('/get_home', [HomeController::class, 'searchData'])->name('get_home');







Route::post('/get_provinsi', [Controller::class, 'GetProvinsi'])->name('get_provinsi');
Route::post('/get_city', [Controller::class, 'GetKota'])->name('get_city');
Route::post('/get_district', [Controller::class, 'GetKecamatan'])->name('get_district');




Route::post('/get_saldo', [HomeController::class, 'getSaldo'])->name('get_saldo');
Route::post('/get_order', [HomeController::class, 'getOrder'])->name('get_order');
Route::post('/ambil_order', [HomeController::class, 'ambilOrder'])->name('ambil_order');
Route::post('/ambil_barang', [HomeController::class, 'ambilBarang'])->name('ambil_barang');
Route::post('/serah_barang', [HomeController::class, 'serahBarang'])->name('serah_barang');
Route::post('/update_pesanan_shop', [HomeController::class, 'updatePesananShop'])->name('update_pesanan_shop');




Route::get('/balance', [BalanceController::class, 'index'])->name('balance');


Route::get('/finance', [FinanceController::class, 'index'])->name('finance');
Route::post('/get_deposit', [FinanceController::class, 'getDeposit'])->name('get_deposit');
Route::get('/finance_deposit_konfirm/{status}/{id}', [FinanceController::class, 'konfirmasiDeposit'])->name('finance_deposit_konfirm');


Route::get('/test', [Controller::class, 'api_token']);
Route::get('/change/{locale}', function (string $locale) {
    session()->put('locale', $locale);
 
    return redirect('home');

});

Route::get('/banner', [BannerController::class, 'index'])->name('banner');
Route::post('/save_banner', [BannerController::class, 'saveData'])->name('save_banner');
Route::get('/delete_banner/{id}', [BannerController::class, 'deleteData'])->name('delete_banner');