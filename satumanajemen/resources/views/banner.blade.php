@extends('layout.template')

@section('title', $title)

@section('content')


<div id="appCapsule">
    <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#ModalBasic">Tambah Banner</button>
    <div class="listview-title mt-2">Aktif Banner</div>
    <ul class="listview image-listview media inset mb-2">
        @foreach($dtBanner as $row)
            <li>
                <a href="#" class="item" onclick="editBanner('{{$row->id_banner}}','{{$row->gambar}}','{{$row->posisi}}','{{$row->keterangan}}','{{$row->judul}}','{{$row->is_aktif}}','{{$row->url}}')">
                    <div class="imageWrapper">
                        <img src="{{asset('/uploads/banner/'.$row->gambar)}}" alt="image" class="imaged w64">
                    </div>
                    <div class="in">
                        <div>
                            <small class="text-danger">{{$row->posisi}}</small> - {{$row->judul}}
                            <div class="text-muted">{{$row->keterangan}}</div>
                        </div>
                    </div>
                </a>
            </li>
        @endforeach
            
    </ul>
    <div class="listview-title mt-2">NonAktif Banner</div>
    <ul class="listview image-listview media inset mb-2">
        @foreach($dtBannerNonaktif as $row)
            <li>
                <a href="#" class="item" onclick="editBanner('{{$row->id_banner}}','{{$row->gambar}}','{{$row->posisi}}','{{$row->keterangan}}','{{$row->judul}}','{{$row->is_aktif}}','{{$row->url}}')">
                    <div class="imageWrapper">
                        <img src="{{asset('/uploads/banner/'.$row->gambar)}}" alt="image" class="imaged w64">
                    </div>
                    <div class="in">
                        <div>
                            <small class="text-danger">{{$row->posisi}}</small> - {{$row->judul}}
                            <div class="text-muted">{{$row->keterangan}}</div>
                        </div>
                    </div>
                </a>
            </li>
        @endforeach
            
    </ul>
</div>

        <div class="modal fade modalbox" id="ModalBasic" tabindex="-1" aria-hidden="true" style="display: none;">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah Banner</h5>
                        <a href="#" data-bs-dismiss="modal">Close</a>
                    </div>
                    <div class="modal-body">
                        <div class="card">
                        <form method="POST" action="{{url('save_banner')}}" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" class="form-control" id="id_banner" name="id_banner" placeholder="Judul Banner">
                            <div class="card-body">
                                <div class="form-group basic">
                                    <div class="input-wrapper">
                                        <label class="label" for="judul_banner">Judul</label>
                                        <input type="text" class="form-control" id="judul_banner" name="judul_banner" placeholder="Judul Banner">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>
                            </div>

                            <div class="card-body">
                                <div class="form-group basic">
                                    <div class="input-wrapper">
                                        <label class="label" for="url_banner">URL</label>
                                        <input type="text" class="form-control" id="url_banner" name="url_banner" placeholder="URL Banner">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>
                            </div>

                            <div class="card-body">
                                <div class="form-group basic">
                                    <div class="input-wrapper">
                                        <label class="label" for="keterangan_banner">Keterangan</label>
                                        <input type="text" class="form-control" id="keterangan_banner" name="keterangan_banner" placeholder="Keterangan Banner">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>
                            </div>

                            <div class="card-body">
                                <div class="form-group basic">
                                    <div class="input-wrapper">
                                        <label class="label" for="posisi_banner">Tampil Di</label>
                                        <select class="form-control" name="posisi_banner" id="posisi_banner">
                                            <option value="user">USER</option>
                                            <option value="kurir">KURIR</option>
                                        </select>
                                        <i class="clear-input">
                                            <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>
                            </div>

                            <div class="card-body">
                                <div class="form-group basic">
                                    <div class="input-wrapper">
                                        <label class="label" for="status_banner">Status</label>
                                        <select class="form-control" name="status_banner" id="status_banner">
                                            <option value="1">Aktif</option>
                                            <option value="0">Nonaktif</option>
                                        </select>
                                        <i class="clear-input">
                                            <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>
                            </div>

                            <div class="card-body">
                                <div class="custom-file-upload" id="upload_gambar_banner">
                                    <input type="file" id="gambar_banner" name="gambar_banner" accept=".png, .jpg, .jpeg">
                                    <label for="gambar_banner">
                                        <span>
                                            <strong>
                                                <ion-icon name="arrow-up-circle-outline" role="img" class="md hydrated" aria-label="arrow up circle outline"></ion-icon>
                                                <i>Upload a Photo</i>
                                            </strong>
                                        </span>
                                    </label>
                                </div>
                            </div>


                            <button class="btn btn-primary btn-block">SIMPAN</button>
                            <a href="#" id="hapus_banner" class="btn btn-danger btn-block mt-4">HAPUS</a>

                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<script>
    function editBanner(id_banner,gambar,posisi,keterangan,judul,is_aktif, url) {
        console.log(gambar)
        $('#ModalBasic').modal('show')
        $('#ModalBasic .modal-title').html('Edit Banner')
        $('#hapus_banner').attr('href',`{{url('delete_banner/`+id_banner+`')}}`)

        $('#id_banner').val(id_banner)
        $('#posisi_banner').val(posisi).trigger('change')
        $('#url_banner').val(url)
        $('#keterangan_banner').val(keterangan)
        $('#judul_banner').val(judul)
        $('#status_banner').val(is_aktif).trigger('change')
        $('#gambar_banner').css('background-image', `url(<?=asset('uploads/banner/"+gambar+"')?>)`);
    }
</script>
@endsection