@extends('layout.template')

@section('title', $title)

@section('content')
    <!-- App Capsule -->
    <div id="appCapsule" class="full-height">

        <div class="section mt-2">

            <div class="row">
                <div class="col">
                    <input type="date" class="form-control" value="{{$tglAwal}}" id="tgl_awal" onchange="getHistory()">
                </div>
                <div class="col">
                    <input type="date" class="form-control" value="{{$tglAkhir}}" id="tgl_akhir" onchange="getHistory()">
                </div>
            </div>
            
        </div>

        <div class="section mt-2">
            <div id="history_place"></div>
        </div>
    </div>
    
    <!-- * App Capsule -->


        <div class="modal fade modalbox" id="ModalViewImage" tabindex="-1" role="dialog" aria-modal="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Approve Transfer</h5>
                        <a href="#" data-bs-dismiss="modal">Close</a>
                    </div>
                    <div class="modal-body">
                    
                    </div>
                    <div class="modal-footer" id="modal_button">
                        
                    </div>
                </div>
            </div>
        </div>


    <script>

    $( document ).ready(function() {
        getHistory()
    });


    function getHistory() {
        // loaderShow()
        $.ajax({
            url: "{{route('get_deposit')}}",
            type: "POST",   
            dataType:"HTML",
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') 
            },
            data:{
                tgl_awal : $('#tgl_awal').val(),
                tgl_akhir : $('#tgl_akhir').val(),
            },
            success: function(response) {
                // loaderHide()
                $('#history_place').html(response)
            },
            error: function(err) {
                // loaderHide()
                // console.log(err);
                // $('#content-order').html(err)
            }    
        });
    }


    function viewImage(id, image, type) {
        $('#ModalViewImage').modal('show')
        if(type === 'deposit'){
            $('#ModalViewImage .modal-title').html('Approval Deposit')
            $('#ModalViewImage .modal-body').html(`<img src="`+image+`" alt="" id="image_view_modal" class="imaged img-fluid">`)
        } else {
            $('#ModalViewImage .modal-title').html('Approval Withdraw')
            $('#ModalViewImage .modal-body').html(image)
        }
        
        $('#modal_button').html(`
            <a class="btn btn-primary btn-block" href="{{url('finance_deposit_konfirm/1/`+id+`')}}">Setuju</a>
            <a class="btn btn-danger btn-block" href="{{url('finance_deposit_konfirm/0/`+id+`')}}">Tolak</a>
        `)
    }

    </script>

@endsection
