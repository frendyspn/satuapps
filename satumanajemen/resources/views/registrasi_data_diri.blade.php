@extends('layout.template')

@section('title', $title)

@section('content')
    <!-- App Capsule -->
    <div id="appCapsule">

    <div class="section mt-2 text-center">
            <h1>{{__('bahasa.Daftar')}}</h1>
            <h4>{{__('bahasa.Lengkapi_Form_Dibawah')}}</h4>
        </div>

        <div class="section mb-5 p-2">

            <form method="post" action="{{route('registrasi_proses_data_diri')}}" enctype="multipart/form-data">
                @csrf
                <div class="card">
                    <div class="card-body pb-1">

                        <div class="form-group basic">
                            <div class="input-wrapper">
                                <label class="label" for="nomor_hp">{{__('bahasa.Nomor_HP')}}</label>
                                <input type="text" class="form-control" id="nomor_hp" name="nomor_hp" value="{{$nomor}}" readonly>
                            </div>
                        </div>

                        <div class="form-group basic">
                            <div class="input-wrapper">
                                <label class="label" for="nama">{{__('bahasa.Nama_Lengkap')}}</label>
                                <input type="text" class="form-control" id="nama" name="nama" placeholder="{{__('bahasa.Nama_Lengkap')}}">
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <div class="form-group basic">
                            <div class="input-wrapper">
                                <label class="label" for="email">{{__('bahasa.Email')}}</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="{{__('bahasa.Email')}}">
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <div class="form-group basic">
                            <div class="input-wrapper">
                                <label class="label" for="jenis_kelamin">{{__('bahasa.Jenis_Kelamin')}}</label>
                                <select class="form-control" name="jenis_kelamin" id="jenis_kelamin">
                                    <option value="">-- {{__('bahasa.Pilih_Jenis_Kelamin')}} --</option>
                                    <option value="Laki-laki">{{__('bahasa.Laki_laki')}}</option>
                                    <option value="Perempuan">{{__('bahasa.Perempuan')}}</option>
                                </select>
                                
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <div class="form-group basic">
                            <div class="input-wrapper">
                                <label class="label" for="tanggal_lahir">{{__('bahasa.Tanggal_Lahir')}}</label>
                                <input type="date" class="form-control" id="tanggal_lahir" name="tanggal_lahir" placeholder="{{__('bahasa.Tanggal_Lahir')}}">
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <div class="form-group basic">
                            <div class="input-wrapper">
                                <label class="label" for="tempat_lahir">{{__('bahasa.Tempat_Lahir')}}</label>
                                <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" placeholder="{{__('bahasa.Tempat_Lahir')}}">
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>


                        <div class="form-group basic">
                            <div class="input-wrapper">
                                <label class="label" for="provinsi">{{__('bahasa.Provinsi')}}</label>
                                <select class="form-control custom-select" id="provinsi" name="provinsi" onchange="get_city($(this).val())" required></select>
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <div class="form-group basic">
                            <div class="input-wrapper">
                                <label class="label" for="kota">{{__('bahasa.Kota')}}</label>
                                <select class="form-control custom-select" id="kota" name="kota" onchange="get_district($(this).val())" required></select>
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <div class="form-group basic">
                            <div class="input-wrapper">
                                <label class="label" for="kecamatan">{{__('bahasa.Kecamatan')}}</label>
                                <select class="form-control custom-select" id="kecamatan" name="kecamatan" required></select>
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <div class="form-group basic">
                            <div class="input-wrapper">
                                <label class="label" for="alamat">{{__('bahasa.Alamat')}}</label>
                                <textarea rows="2" class="form-control" id="alamat" name="alamat" placeholder="{{__('bahasa.Alamat')}}" required></textarea>
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        
                    </div>
                </div>


                
                <div class="form-button-group  transparent">
                    <button type="submit" class="btn btn-primary btn-block btn-lg">{{__('bahasa.Lanjutkan')}}</button>
                </div>

            </form>
        </div>

    </div>
    <!-- * App Capsule -->

    <script>

    $( document ).ready(function() {
        
        getProvinsi()
    });

    function getProvinsi() {
            var id_prov = ''
            console.log(id_prov)
            $.ajax({
                url: "<?= route('get_provinsi') ?>",
                method: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(response) {
                    $('#provinsi').append($('<option>', { 
                        value: '',
                        text : '-- {{__(\'bahasa.Pilih_Provinsi\')}} --'
                    }));
                    $.each(response, function (i, item) {
                        $('#provinsi').append($('<option>', { 
                            value: item.province_id,
                            text : item.province_name 
                        }));
                    });
                    if (id_prov !== '') {
                        $('#provinsi').val(id_prov).change()
                    }
                },
                error: function(error) {
                    console.log("error" + error);
                }
            }); 
        }

        function get_city(id_provinsi) {
            var id_kota = ''

            $('#kota').html('')
            $.ajax({
                url: "<?= route('get_city') ?>",
                method: "POST",
                data : {id_provinsi},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(response) {
                    console.log(response)
                    $('#kota').append($('<option>', { 
                        value: '',
                        text : '-- {{__(\'bahasa.Pilih_Kota\')}} --'
                    }));
                    
                    $.each(response, function (i, item) {
                        $('#kota').append($('<option>', { 
                            value: item.city_id,
                            text : item.city_name 
                        }));
                        
                        if ((i+1) === response.length && id_kota !== '') {
                            $('#kota').val(id_kota).change()
                        }
                    });
                },
                error: function(error) {
                    console.log("error" + error);
                }
            }); 
        }

        function get_district(id_kota) {
            var id_kecamatan = ''

            $('#kecamatan').html('')
            $.ajax({
                url: "<?= route('get_district') ?>",
                method: "POST",
                data : {id_kota},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(response) {
                    console.log(response)
                    $('#kecamatan').append($('<option>', { 
                        value: '',
                        text : '-- {{__(\'bahasa.Pilih_Kecamatan\')}}  --'
                    }));
                    $.each(response, function (i, item) {
                        $('#kecamatan').append($('<option>', { 
                            value: item.subdistrict_id,
                            text : item.subdistrict_name 
                        }));

                        if ((i+1) === response.length && id_kecamatan !== '') {
                            $('#kecamatan').val(id_kecamatan).change()
                        }
                    });
                },
                error: function(error) {
                    console.log("error" + error);
                }
            }); 
        }
    
    </script>
@endsection
