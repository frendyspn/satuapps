@extends('layout.template')

@section('title', $title)

@section('content')

<div id="appCapsule" class="full-height">
    
    <form method="post" action="{{url('setting_save')}}">
        @csrf
    <div class="row px-3">
        <div class="section mt-2 mb-2 col-md-6 col-sm-12">
            <div class="section-title">Tarif</div>
            <div class="card">
                <div class="card-body">
                    
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <label class="label" for="text4b">Tarif Awal</label>
                            <input type="number" class="form-control" id="ongkir_awal_kurir" name="ongkir_awal_kurir" placeholder="Tarif Awal" value="{{$ongkir_awal_kurir}}">
                            <i class="clear-input">
                                <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                            </i>
                        </div>
                    </div>
                    
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <label class="label" for="text4b">Tarif / KM</label>
                            <input type="number" class="form-control" id="ongkir_per_km" name="ongkir_per_km" placeholder="Tarif Awal" value="{{$ongkir_per_km}}">
                            <i class="clear-input">
                                <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                            </i>
                        </div>
                    </div>

                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <label class="label" for="text4b">Jarak max (KM) <small class="text-info fw-bold">Isi 0 jika tanpa batas</small></label>
                            <input type="number" class="form-control" id="max_jarak_km" name="max_jarak_km" placeholder="Tarif Awal" value="{{$max_jarak_km}}">
                            <i class="clear-input">
                                <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                            </i>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section-title">Fee</div>
            <div class="card">
                <div class="card-body">
                    
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <label class="label" for="text4b">Total Fee (%)</label>
                            <input type="number" class="form-control" id="fee_kurir_total" name="fee_kurir_total" value="{{$fee_kurir_total}}">
                            <i class="clear-input">
                                <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                            </i>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>


        <div class="section mt-2 mb-2 col-md-6 col-sm-12">
            

            <div class="section-title">Komisi</div>
            <div class="card">
                <div class="card-body">
                    
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <label class="label" for="text4b">Refferal (%)</label>
                            <input type="number" class="form-control" id="fee_kurir_ref" name="fee_kurir_ref" value="{{$fee_kurir_ref}}">
                            <i class="clear-input">
                                <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                            </i>
                        </div>
                    </div>

                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <label class="label" for="text4b">Pusat (%)</label>
                            <input type="number" class="form-control" id="fee_kurir_sistem" name="fee_kurir_sistem" value="{{$fee_kurir_sistem}}">
                            <i class="clear-input">
                                <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                            </i>
                        </div>
                    </div>

                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <label class="label" for="text4b">Agen (%)</label>
                            <input type="number" class="form-control" id="fee_kurir_agen" name="fee_kurir_agen" value="{{$fee_kurir_agen}}">
                            <i class="clear-input">
                                <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                            </i>
                        </div>
                    </div>

                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <label class="label" for="text4b">Koordinator Kota (%)</label>
                            <input type="number" class="form-control" id="fee_kurir_koordinator_kota" name="fee_kurir_koordinator_kota" value="{{$fee_kurir_koordinator_kota}}">
                            <i class="clear-input">
                                <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                            </i>
                        </div>
                    </div>

                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <label class="label" for="text4b">Koordinator Kecamatan (%)</label>
                            <input type="number" class="form-control" id="fee_kurir_koordinator_kecamatan" name="fee_kurir_koordinator_kecamatan" value="{{$fee_kurir_koordinator_kecamatan}}">
                            <i class="clear-input">
                                <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                            </i>
                        </div>
                    </div>

                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <label class="label" for="text4b">Kas Cabang (%)</label>
                            <input type="number" class="form-control" id="fee_kurir_kas_cabang" name="fee_kurir_kas_cabang" value="{{$fee_kurir_kas_cabang}}">
                            <i class="clear-input">
                                <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                            </i>
                        </div>
                    </div>

                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <label class="label" for="text4b">Lainnya (%)</label>
                            <input type="number" class="form-control" id="fee_kurir_lainnya" name="fee_kurir_lainnya" value="{{$fee_kurir_lainnya}}">
                            <i class="clear-input">
                                <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                            </i>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    
        <div class="section mt-2 mb-2">
            <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
    </div>
    </form>
</div>

@endsection