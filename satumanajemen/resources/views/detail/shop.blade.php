@extends('layout.template')

@section('title', $title)

@section('content')

<div id="appCapsule" class="full-height">
    <div class="section inset mt-2">
            <ul class="listview image-listview mb-2">
                <li>
                    <div class="item">
                        <div class="icon-box bg-primary">
                            <ion-icon name="location-outline"></ion-icon>
                        </div>
                        <div class="in">
                            <div>
                                <header>
                                    @if($order->nama_toko == '')
                                    {{$order->pemberi_barang}}
                                    @else
                                    {{$order->nama_toko}}
                                    @endif
                                </header>
                                <footer>
                                {{$order->alamat_jemput}}
                                </footer>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="item">
                        <div class="icon-box bg-info">
                        <a class="text-white" href="https://www.google.com/maps/dir/?api=1&origin='.$position.'&destination='.$getOrder->titik_antar.'"><ion-icon name="location-outline"></ion-icon></a>
                        </div>
                        <div class="in">
                            <div>
                                <header>{{__('bahasa.antar_ke')}}</header>
                                <footer>
                                {{$order->alamat_antar}}
                            </div>
                        </div>
                    </div>
                </li>
                @php $total = 0 @endphp
                @for ($i=0; $i < count($belanja); $i++)
                <li>
                    <div class="item row">
                        <div class="detail col">
                            <div>
                                <strong>{{$belanja[$i]->nama_produk}}</strong>
                                <p>{{__('bahasa.kurs')}}{{number_format($belanja[$i]->harga_jual)}} x {{number_format($belanja[$i]->jumlah)}}</p>
                            </div>
                        </div>
                        <div class="right col-3">
                            </br>
                            <div class="price" style="text-align:right">{{__('bahasa.kurs')}}{{number_format($belanja[$i]->harga_jual * $belanja[$i]->jumlah)}}</div>
                        </div>
                    </div>
                </li>
                @php $total = $total + ($belanja[$i]->harga_jual * $belanja[$i]->jumlah) @endphp
                @endfor

                <li>
                    <div class="item row">
                        <div class="detail col">
                            <div>
                                <strong>{{__('bahasa.sub_total')}}</strong>
                            </div>
                        </div>
                        <div class="right col-3">
                            </br>
                            <div class="price" style="text-align:right">Rp.{{number_format($total)}}</div>
                        </div>
                    </div>
                </li>


                <li>
                    <div class="item row">
                        <div class="detail col">
                            <div>
                                <strong>{{__('bahasa.biaya_kirim')}}</strong>
                            </div>
                        </div>
                        <div class="right col-3">
                            </br>
                            <div class="price" style="text-align:right">Rp.{{number_format($order->tarif)}}</div>
                        </div>
                    </div>
                </li>
                

                <li>
                    <div class="item">
                        <div class="icon-box bg-primary">
                            <ion-icon name="cash-outline"></ion-icon>
                        </div>
                        <div class="in">
                            <h3>
                            {{__('bahasa.total')}} {{__('bahasa.kurs')}}{{number_format($order->tarif+$total)}}
                            </h3>
                        </div>
                    </div>
                </li>
                
            </ul>

            
    </div>

        <div class="section mt-1 mb-2">
            <div class="section-title">{{__('bahasa.status_order')}} <span style="color:blue" onclick="location.reload()">{{__('bahasa.refresh')}}</span></div>
            <div class="card">
                <!-- timeline -->
                <div class="timeline ms-3">
                    
                    @foreach($timeline as $row)
                    @php
                    if($row->log_status == 'NEW'){
                        $status = __('bahasa.mencari_driver');
                    } else if($row->log_status == 'PROCESS'){
                        $status = __('bahasa.mencari_driver');
                    } else if($row->log_status == 'ONTHEWAY'){
                        $status = __('bahasa.pesanan_sedang_diantar');
                    } else if($row->log_status == 'FINISH'){
                        $status = __('bahasa.pesanan_sampai');
                    } else if($row->log_status == 'PENDING'){
                        $status = __('bahasa.pesanan_konfirm_toko');
                    } else {
                        $status = $row->log_status;
                    }

                    @endphp
                    <div class="item">
                        <div class="dot {{$warna_timeline[rand(0,count($warna_timeline)-1)]}}"></div>
                        <div class="content">
                            <h4 class="title">{{$status}}</h4>
                            <div class="text">{{date('d-M-Y H:i:s', strtotime($row->log_time))}}</div>
                            @if($row->log_status == 'PROCESS')
                            <h4 class="title mt-2">{{$kurir->nama_lengkap}}</h4>
                            <div class="text">{{$kurir->merek}} - {{$kurir->plat_nomor}}</div>
                            @endif
                        </div>
                    </div>
                    @endforeach
                </div>
                <!-- * timeline -->
            </div>
        </div>
</div>

@endsection