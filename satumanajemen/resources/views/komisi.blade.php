@extends('layout.template')

@section('title', $title)

@section('content')
    <!-- App Capsule -->
    <div id="appCapsule" class="full-height">

        <div class="section mt-2">

            <div class="row">
                <div class="col">
                    <input type="date" class="form-control" value="{{$tglAwal}}" id="tgl_awal" onchange="getHistory()">
                </div>
                <div class="col">
                    <input type="date" class="form-control" value="{{$tglAkhir}}" id="tgl_akhir" onchange="getHistory()">
                </div>
            </div>
            
        </div>

        <div class="section mt-2">
            <div id="history_place"></div>
        </div>
    </div>
    
    <!-- * App Capsule -->

    <script>

    $( document ).ready(function() {
        getHistory()
    });


    function getHistory() {
        loaderShow()
        $.ajax({
            url: "{{route('get_komisi')}}",
            type: "POST",   
            dataType:"HTML",
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') 
            },
            data:{
                tgl_awal : $('#tgl_awal').val(),
                tgl_akhir : $('#tgl_akhir').val(),
            },
            success: function(response) {
                loaderHide()
                $('#history_place').html(response)
            },
            error: function(err) {
                loaderHide()
                // console.log(err);
                // $('#content-order').html(err)
            }    
        });
    }

    </script>

@endsection
