@extends('layout.template')

@section('title', $title)

@section('content')
    <!-- App Capsule -->
    <div id="appCapsule" class="full-height">
        <div class="section">
            <div class="row mt-2">
                <div class="col-12">
                    <div class="stat-box">
                        <div class="title">{{__('bahasa.Saldo')}}</div>
                        <div class="value text-success">{{__('bahasa.kurs')}} {{number_format($saldo)}}</div>
                    </div>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-6">
                    <div class="stat-box">
                        <div class="title">{{__('bahasa.Pendapatan')}}</div>
                        <div class="value" style="font-size:14px">{{__('bahasa.kurs')}} {{number_format($saldoKurir)}}</div>
                    </div>
                </div>

                <div class="col-6">
                    <div class="stat-box">
                        <div class="title">{{__('bahasa.Komisi')}}</div>
                        <div class="value" style="font-size:14px">{{__('bahasa.kurs')}} {{number_format($saldoKomisi)}}</div>
                    </div>
                </div>

            </div>
        </div>

        <div class="section mt-2">
            <div class="card">
                <ul class="listview flush transparent no-line image-listview detailed-list mt-1 mb-1">
                    <!-- item -->
                    @foreach($dtWallet as $wlt)
                    
                    <li>
                        <a href="#" class="item">
                            @if($wlt->transaksi == 'debit')
                            <div class="icon-box bg-primary">
                                <ion-icon name="arrow-down-outline"></ion-icon>
                            </div>
                            @else
                            <div class="icon-box bg-danger">
                                <ion-icon name="arrow-up-outline"></ion-icon>
                            </div>
                            @endif
                            
                            <div class="in">
                                <div>
                                    <strong>
                                        @if($wlt->transaksi == 'debit')
                                            @if($wlt->akun == 'sopir')
                                                {{__('bahasa.pendapatan_kurir')}}
                                            @else
                                                {{__('bahasa.'.$wlt->keterangan)}}
                                            @endif
                                        @else
                                            <!-- {{$wlt->keterangan}} -->
                                            {{__('bahasa.'.$wlt->keterangan)}}
                                        @endif
                                    </strong>
                                    
                                </div>
                                <div class="text-end">
                                    @if($wlt->transaksi == 'debit')
                                    <strong>{{__('bahasa.kurs')}} {{number_format($wlt->nominal)}}</strong>
                                    @else
                                    <strong class="fw-bold">-{{__('bahasa.kurs')}} {{number_format($wlt->nominal)}}</strong>
                                    @endif
                                    <div class="text-small">
                                    {{date('d M Y', strtotime($wlt->waktu_pendapatan))}}
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    @endforeach
                    <!-- * item -->
                </ul>
            </div>
        </div>
    </div>
    
    <!-- * App Capsule -->


@endsection
