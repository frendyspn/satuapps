@extends('layout.template')

@section('title', $title)

@section('content')
    <!-- App Capsule -->
    <div id="appCapsule" class="full-height">

        <div class="section mt-2">

            <div class="row">
                <div class="col">
                    <input type="date" class="form-control" value="{{$tglAwal}}" id="tgl_awal" onchange="getHistory()">
                </div>
                <div class="col">
                    <input type="date" class="form-control" value="{{$tglAkhir}}" id="tgl_akhir" onchange="getHistory()">
                </div>
                <div class="col">
                    <select class="form-control" name="layanan" id="layanan" onchange="getHistory()">
                    <option value="">Semua Layanan</option>
                    <option value="RIDE">RIDE</option>
                    <option value="KURIR">SEND</option>
                    <option value="FOOD">FOOD</option>
                    <option value="SHOP">SHOP</option>
                    </select>
                </div>
            </div>
            
        </div>
        <div id="history_place"></div>
    </div>
    
    <!-- * App Capsule -->

    <script>

    $( document ).ready(function() {
        getHistory()
    });


    function getHistory() {
        loaderShow()
        $.ajax({
            url: "{{route('get_report')}}",
            type: "POST",   
            dataType:"HTML",
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') 
            },
            data:{
                tgl_awal : $('#tgl_awal').val(),
                tgl_akhir : $('#tgl_akhir').val(),
                layanan : $('#layanan').val()
            },
            success: function(response) {
                console.log('ok')
                loaderHide()
                $('#history_place').html(response)
            },
            error: function(err) {
                console.log('no')
                loaderHide()
                // console.log(err);
                // $('#content-order').html(err)
            }    
        });
    }

    </script>

@endsection
